# Streve
# Copyright (C) 2024 The Streve Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: Copyright (C) 2024 The Streve Authors

import struct
from pathlib import Path

_CRC_TABLE = [
    0x0000,
    0xCC01,
    0xD801,
    0x1400,
    0xF001,
    0x3C00,
    0x2800,
    0xE401,
    0xA001,
    0x6C00,
    0x7800,
    0xB401,
    0x5000,
    0x9C01,
    0x8801,
    0x4400,
]


# Low-level stream which tracks CRC of bytes read
class Stream:

    # TODO support other data sources?
    def __init__(self, filename: Path):
        self._filename = filename
        self._crc = 0
        self._bytes_read = 0

    def __enter__(self):
        self._reader = open(self._filename, "rb")
        return self

    def __exit__(self, exc_type, exc, tb):
        self._reader.close()

    # Stream manipulation

    def __bool__(self):
        return len(self._reader.peek(1)) >= 1

    @property
    def bytes_read(self) -> int:
        return self._bytes_read

    def read_bytes(self, num_bytes: int) -> bytes:
        data = self._reader.read(num_bytes)[0:num_bytes]
        assert len(data) == num_bytes
        self._bytes_read += num_bytes
        for byte in data:
            self._update_crc(byte)
        return data

    def read_byte(self) -> int:
        return self.read_bytes(1)[0]

    def read_struct(self, format: str) -> tuple:
        data = self.read_bytes(struct.calcsize(format))
        return struct.unpack(format, data)

    # CRC

    @property
    def crc(self) -> int:
        return self._crc

    def reset_crc(self):
        self._crc = 0

    def _update_crc(self, value: int):
        # compute checksum of lower four bits of byte
        tmp = _CRC_TABLE[self._crc & 0xF]
        self._crc = (self._crc >> 4) & 0x0FFF
        self._crc = self._crc ^ tmp ^ _CRC_TABLE[value & 0xF]
        # now compute checksum of upper four bits of byte
        tmp = _CRC_TABLE[self._crc & 0xF]
        self._crc = (self._crc >> 4) & 0x0FFF
        self._crc = self._crc ^ tmp ^ _CRC_TABLE[(value >> 4) & 0xF]
