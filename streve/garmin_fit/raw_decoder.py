# Streve
# Copyright (C) 2024 The Streve Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: Copyright (C) 2024 The Streve Authors

from dataclasses import dataclass, field
from enum import Enum
from typing import Iterator, Optional, Self

from streve.garmin_fit.base_type import BaseType
from streve.garmin_fit.stream import Stream

"""
This module provides a RawDecoder class which is used to decode RawMessage's from a stream containing FIT file(s).

This module is profile-agnostic.
"""


@dataclass
class FileHeader:
    header_size: int
    protocol_version: int
    profile_version: int
    data_size: int
    data_type: bytes
    crc: Optional[int] = None

    @staticmethod
    def decode(stream: Stream) -> Self:
        file_header = FileHeader(*stream.read_struct("<BBHI4s"))
        assert file_header.data_type == b".FIT"
        assert file_header.header_size == 12 or file_header.header_size == 14
        if file_header.header_size == 14:
            calculated_crc = stream.crc
            (file_header.crc,) = stream.read_struct("<H")
            assert file_header.crc == 0 or file_header.crc == calculated_crc
        return file_header


class MessageType(Enum):
    DATA = 0
    DEFINITION = 1


@dataclass
class RecordHeader:
    normal_header: bool
    message_type: MessageType
    message_type_specific: bool
    local_message_type: int

    @staticmethod
    def decode(stream: Stream) -> Self:
        data = stream.read_byte()
        normal_header = not (bool(data & (1 << 7)))
        message_type = MessageType((data >> 6) & 1)
        message_type_specific = bool(data & (1 << 5))
        local_message_type = data & 0b111
        return RecordHeader(normal_header, message_type, message_type_specific, local_message_type)


@dataclass
class Field:
    field_def_number: int
    size: int
    base_type: BaseType

    @staticmethod
    def decode(stream: Stream) -> Self:
        field_def_number, size = stream.read_struct("BB")
        base_type = BaseType.decode(stream)
        return Field(field_def_number, size, base_type)

    @property
    def num_elements(self) -> int:
        assert self.size % self.base_type.size == 0
        return self.size // self.base_type.size

    def decode_values(self, stream: Stream, architecture):
        return self.base_type.decode_values(stream, architecture.format_code, self.num_elements)


@dataclass
class DeveloperField:
    field_number: int
    size: int
    developer_data_index: int

    @staticmethod
    def decode(stream: Stream) -> Self:
        return DeveloperField(*stream.read_struct("BBB"))


class ArchitectureType(Enum):
    LITTLE_ENDIAN = 0
    BIG_ENDIAN = 1

    @property
    def format_code(self) -> str:
        return "<" if self == ArchitectureType.LITTLE_ENDIAN else ">"


@dataclass
class DefinitionMessage:
    architecture: ArchitectureType
    global_message_number: int
    fields: list[Field]
    developer_fields: list[DeveloperField]

    @staticmethod
    def decode(stream: Stream, developer_data: bool) -> Self:
        reserved = stream.read_byte()
        assert reserved == 0
        architecture = ArchitectureType(stream.read_byte())
        (global_message_number, num_fields) = stream.read_struct(architecture.format_code + "HB")
        fields = [Field.decode(stream) for _ in range(num_fields)]
        if developer_data:
            num_developer_fields = stream.read_byte()
            developer_fields = [DeveloperField.decode(stream) for _ in range(num_developer_fields)]
        else:
            developer_fields = []
        return DefinitionMessage(architecture, global_message_number, fields, developer_fields)


type RawValue = int | float | str | None

type RawValues = list[RawValue] | None


@dataclass
class RawField:
    number: int
    values: RawValues


@dataclass
class RawMessage:
    number: int
    fields: list[RawField]
    fields_by_number: dict[int, RawField] = field(init=False)

    def __post_init__(self):
        self.fields_by_number = {field.number: field for field in self.fields}
        assert len(self.fields_by_number) == len(self.fields)


class RawDecoder:

    def __init__(self, stream):
        self._stream = stream

    # Note: Multiple files may be decoded from the same stream
    def decode_file(self) -> Iterator[RawMessage]:
        self._reset()
        file_header = FileHeader.decode(self._stream)
        data_end = self._stream.bytes_read + file_header.data_size
        while self._stream.bytes_read < data_end:
            yield from self._decode_record()
        assert self._stream.bytes_read == data_end
        calculated_crc = self._stream.crc
        (crc,) = self._stream.read_struct("<H")
        assert crc == calculated_crc

    def _reset(self):
        self._local_message_defs = {}
        self._messages = []
        self._stream.reset_crc()

    def _decode_record(self):
        record_header = RecordHeader.decode(self._stream)
        if not record_header.normal_header:
            raise NotImplementedError("compressed timestamp header not supported")
        match record_header.message_type:
            case MessageType.DEFINITION:
                self._decode_definition_message(record_header)
            case MessageType.DATA:
                yield from self._decode_data_message(record_header)

    def _decode_definition_message(self, record_header: RecordHeader):
        message = DefinitionMessage.decode(self._stream, record_header.message_type_specific)
        self._local_message_defs[record_header.local_message_type] = message

    def _decode_data_message(self, record_header: RecordHeader):
        message_def = self._local_message_defs[record_header.local_message_type]
        if message_def.developer_fields:
            raise NotImplementedError("developer fields not supported")  # TODO
        fields = [
            RawField(field_def.field_def_number, field_def.decode_values(self._stream, message_def.architecture))
            for field_def in message_def.fields
        ]
        yield RawMessage(message_def.global_message_number, fields)
