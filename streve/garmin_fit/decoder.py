# Streve
# Copyright (C) 2024 The Streve Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: Copyright (C) 2024 The Streve Authors

import argparse
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Iterator, Optional

import streve.garmin_fit.profile.profile as profile
from streve.garmin_fit.base_type import BASE_TYPES_BY_NAME
from streve.garmin_fit.profile.parser import parse_profile
from streve.garmin_fit.profile.schema import (
    MessageComponentDefinition,
    MessageFieldDefinition,
    MessageRefFieldDefinition,
    MessageSubfieldDefinition,
    ProfileDefinition,
    TypeName,
)
from streve.garmin_fit.raw_decoder import RawDecoder, RawMessage
from streve.garmin_fit.stream import Stream

"""
This module provides a Decoder class which is used to decode Message's from a stream containing FIT file(s).

The Decoder extends RawDecoder, transforming RawMessage's to Message's using the supplied FIT profile.
"""


@dataclass
class Message:
    type: profile.MesgNum
    fields: dict[str, Any]
    # Depending on the features of the field (as specified in the profile), a value may either be:
    #  1. None (a non-array invalid value, or invalid byte array)
    #  2. A single string, enum (from profile.py), int, or float (a non-array valid value)
    #  3. A list of enums (from profile.py), ints, or floats (an array value)
    #    Note: Any of the elements may be None, indicating an invalid value for that element.
    #  4. A dictionary with string keys and values as given by this list (subfields / components)
    #    Note: subfields / components can be nested, so a value in a dictionary may be another dictionary.


class MessageBuilder:

    def __init__(self, type: profile.MesgNum, message_def: MessageFieldDefinition):
        self._type = type
        self._message_def = message_def
        self._accumulators = {}
        for field_def in self._message_def.fields:
            for component in field_def.components:
                if component.accumulate:
                    self._accumulators[component.name] = 0

    def build(self, raw_message: RawMessage) -> Message:
        fields = {}
        for field in raw_message.fields:
            field_def = self._message_def.fields_by_number[field.number]
            fields[field_def.name] = self._build_field(
                raw_message, field.values, field_def, field_def.scale, field_def.offset
            )
        return Message(self._type, fields)

    def _build_field(
        self, raw_message, raw_values, field_def: MessageFieldDefinition, scale: Optional[int], offset: Optional[int]
    ):
        if raw_values is None:
            return None
        assert not (field_def.subfields and field_def.components)
        if field_def.subfields:
            subfield = self._try_build_subfield(raw_message, raw_values, field_def.subfields)
            if subfield is not None:
                return subfield
            return self._build_values(raw_values, field_def.type, field_def.array, scale, offset)
        elif field_def.components:
            return self._build_components(raw_message, raw_values, field_def.components)
        else:
            return self._build_values(raw_values, field_def.type, field_def.array, scale, offset)

    def _build_subfield(self, raw_message, raw_values, subfield_def: MessageSubfieldDefinition):
        if subfield_def.components:
            return self._build_components(raw_message, raw_values, subfield_def.components)
        else:
            return self._build_values(raw_values, subfield_def.type, False, subfield_def.scale, subfield_def.offset)

    def _ref_field_matches(self, raw_message, raw_values, ref_field_def: MessageRefFieldDefinition) -> bool:
        field_def = self._message_def.fields_by_name[ref_field_def.name]
        field = raw_message.fields_by_number.get(field_def.number, None)
        if field is None:
            return False
        assert isinstance(field.values, list) and len(field.values) == 1
        actual_value = field.values[0]
        if actual_value is None:
            return False
        assert isinstance(actual_value, int)
        enum_type = getattr(profile, field_def.type)
        actual_value_enum = enum_type(actual_value)
        target_value_enum = getattr(enum_type, ref_field_def.value)
        return actual_value_enum == target_value_enum

    def _subfield_matches(self, raw_message, raw_values, subfield_def: MessageSubfieldDefinition) -> bool:
        return any(
            self._ref_field_matches(raw_message, raw_values, ref_field_def) for ref_field_def in subfield_def.ref_fields
        )

    def _try_build_subfield(
        self, raw_message, raw_values, subfield_defs: list[MessageSubfieldDefinition]
    ) -> Optional[dict]:
        for subfield_def in subfield_defs:
            if not self._subfield_matches(raw_message, raw_values, subfield_def):
                continue
            return {subfield_def.name: self._build_subfield(raw_message, raw_values, subfield_def)}
        return None

    def _accumulate(self, component_def: MessageComponentDefinition, value: int):
        assert component_def.accumulate and isinstance(value, int)
        # accumulators track values (of n bits) that increase over time.
        # if we see a value less than the last value, we infer the overflow amount (1 << n)
        accumulation = self._accumulators[component_def.name]
        overflow = 1 << component_def.bits
        low_mask = overflow - 1
        high_mask = ~low_mask
        last_value = accumulation & low_mask
        accumulation = (accumulation & high_mask) | value
        if value < last_value:
            accumulation += overflow
        self._accumulators[component_def.name] = accumulation
        return accumulation

    def _build_components(
        self, raw_message, raw_values, component_defs: list[MessageComponentDefinition]
    ) -> Optional[dict]:
        assert isinstance(raw_values, list)
        bits = 0
        for byte in reversed(raw_values):
            bits = (bits << 8) | byte
        values = {}
        for component_def in component_defs:
            value = bits & ((1 << component_def.bits) - 1)
            # TODO - can value be interpreted as invalid?
            if component_def.accumulate:
                value = self._accumulate(component_def, value)
            field_def = self._message_def.fields_by_name[component_def.name]
            value = self._build_field(raw_message, [value], field_def, component_def.scale, component_def.offset)
            values[component_def.name] = value
            bits = bits >> component_def.bits
        return values

    def _build_value(self, raw_value, type: TypeName, scale: Optional[int], offset: Optional[int]):
        if raw_value is None:
            return None
        elif type in BASE_TYPES_BY_NAME:
            if isinstance(raw_value, int):
                if scale is not None and scale != 1:
                    raw_value = raw_value / scale
                if offset is not None and offset != 0:
                    raw_value = raw_value - offset
            return raw_value
        else:
            # the type is an enum defined in profile.py with the same name
            cls = getattr(profile, type)
            return cls(raw_value)

    def _build_values(self, raw_values, type: TypeName, array: bool, scale: Optional[int], offset: Optional[int]):
        if raw_values is None:
            return None
        elif array:
            assert isinstance(raw_values, list)
            return [self._build_value(raw_value, type, scale, offset) for raw_value in raw_values]
        else:
            assert isinstance(raw_values, list) and len(raw_values) == 1
            return self._build_value(raw_values[0], type, scale, offset)


class Decoder(RawDecoder):

    def __init__(self, stream: Stream, profile: ProfileDefinition):
        super().__init__(stream)
        self._profile = profile
        self._message_builders = {}

    def decode_file(self) -> Iterator[Message]:
        for raw_message in super().decode_file():
            yield self._build_message(raw_message)

    def _build_message(self, raw_message: RawMessage) -> Message:
        if raw_message.number not in self._message_builders:
            type = profile.MesgNum(raw_message.number)
            message_def = self._profile.messages_by_number[raw_message.number]
            self._message_builders[raw_message.number] = MessageBuilder(type, message_def)
        return self._message_builders[raw_message.number].build(raw_message)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument
    parser.add_argument("sdk_path", help="FIT SDK archive path", type=Path)
    parser.add_argument("fit_file", help="FIT file to decode", type=Path)
    args = parser.parse_args()
    profile = parse_profile(args.sdk_path)  # TODO read from a checked-in file to avoid direct dependency on SDK?
    with Stream(args.fit_file) as stream:
        for message in Decoder(stream, profile).decode_file():
            print(message)


if __name__ == "__main__":
    main()
