# Streve
# Copyright (C) 2024 The Streve Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: Copyright (C) 2024 The Streve Authors

import struct
from enum import Enum

from streve.garmin_fit.stream import Stream


# Reference: https://developer.garmin.com/fit/protocol/
class BaseType(Enum):
    ENUM = (0, 0x00, "enum", "B", 0xFF, 1)
    SINT8 = (1, 0x01, "sint8", "b", 0x7F, 1)  # 2's complement format
    UINT8 = (2, 0x02, "uint8", "B", 0xFF, 1)
    SINT16 = (3, 0x83, "sint16", "h", 0x7FFF, 2)  # 2's complement format
    UINT16 = (4, 0x84, "uint16", "H", 0xFFFF, 2)
    SINT32 = (5, 0x85, "sint32", "i", 0x7FFFFFFF, 4)  # 2's complement format
    UINT32 = (6, 0x86, "uint32", "I", 0xFFFFFFFF, 4)
    STRING = (7, 0x07, "string", "s", 0x00, 1)  # Null terminated string encoded in UTF-8 format
    FLOAT32 = (8, 0x88, "float32", "f", 0xFFFFFFFF, 4)
    FLOAT64 = (9, 0x89, "float64", "d", 0xFFFFFFFFFFFFFFFF, 8)
    UINT8Z = (10, 0x0A, "uint8z", "B", 0x00, 1)
    UINT16Z = (11, 0x8B, "uint16z", "H", 0x0000, 2)
    UINT32Z = (12, 0x8C, "uint32z", "I", 0x00000000, 4)
    BYTE = (13, 0x0D, "byte", "B", 0xFF, 1)  # Array of bytes. Field is invalid if all bytes are invalid.
    SINT64 = (14, 0x8E, "sint64", "q", 0x7FFFFFFFFFFFFFFF, 8)  # 2's complement format
    UINT64 = (15, 0x8F, "uint64", "Q", 0xFFFFFFFFFFFFFFFF, 8)
    UINT64Z = (16, 0x90, "uint64z", "Q", 0x0000000000000000, 8)

    def __init__(self, number: int, field: int, type_name: str, format_code: str, invalid_value: int, size: int):
        self.number = number
        self.field = field
        self.type_name = type_name
        self.format_code = format_code
        self.invalid_value = invalid_value
        self.size = size

    @staticmethod
    def decode(stream: Stream):
        data = stream.read_byte()
        return next(base_type for base_type in BaseType if base_type.field == data)

    def decode_values(self, stream: Stream, endian_code: str, num_elements: int):
        match self:
            case BaseType.BYTE:
                values = list(stream.read_struct(endian_code + str(num_elements) + self.format_code))
                if all(value == self.invalid_value for value in values):  # Field is invalid if all bytes are invalid
                    return None
                return values
            case BaseType.STRING:
                (value,) = stream.read_struct(endian_code + str(num_elements) + self.format_code)
                value = value[: value.index(0x00)].decode(encoding="utf-8", errors="replace")
                return [value] if value else None
            case _:
                values = [self._decode_value(stream, endian_code) for _ in range(num_elements)]
                return values

    def _decode_value(self, stream: Stream, endian_code: str):
        assert self != BaseType.STRING and self != BaseType.BYTE
        invalid_bytes = struct.pack(endian_code + self.format_code, self.invalid_value)
        bytes = stream.read_bytes(self.size)
        if bytes == invalid_bytes:
            return None
        else:
            (value,) = struct.unpack(endian_code + self.format_code, bytes)
            return value

    def __str__(self):
        return self.type_name

    def __repr__(self):
        return self.type_name


BASE_TYPES_BY_NAME = {base_type.type_name: base_type for base_type in BaseType}
