# Streve
# Copyright (C) 2024 The Streve Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: Copyright (C) 2024 The Streve Authors

import argparse
import logging
import sys
from pathlib import Path
from typing import Optional

from streve.garmin_fit.base_type import BaseType
from streve.garmin_fit.profile.parser import parse_profile
from streve.garmin_fit.profile.schema import (
    ProfileDefinition,
    TypeDefinition,
    TypeValueDefinition,
)

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.StreamHandler(stream=sys.stderr)],
)

HEADER = """# *** BEGIN GENERATED PROFILE *** #

from dataclasses import dataclass
from enum import Enum


"""


def append_comment(text: str, comment: Optional[str]) -> str:
    if comment is None:
        return text
    else:
        return text + "  # " + comment


def generate_enum_type(type: TypeDefinition) -> str:
    def generate_value(value: TypeValueDefinition):
        return append_comment(f"    {value.name} = {value.value}", value.comment) + "\n"

    assert type.values
    values = "".join(generate_value(value) for value in type.values)
    return append_comment(f"class {type.name}(Enum):", type.comment) + "\n" + values


def generate_int_type(type: TypeDefinition) -> str:
    def generate_value(value: TypeValueDefinition):
        return append_comment(f"{type.name}.{value.name} = {value.value}", value.comment) + "\n"

    values = "".join(generate_value(value) for value in type.values)
    if values:
        values = "\n\n" + values
    return "@dataclass\n" + append_comment(f"class {type.name}:", type.comment) + "\n" + "    value: int\n" + values


def generate_type(type: TypeDefinition) -> str:
    if type.base_type == BaseType.ENUM:
        return generate_enum_type(type)
    else:
        return generate_int_type(type)


def generate_profile(profile: ProfileDefinition) -> str:
    return HEADER + "\n\n".join(generate_type(type) for type in profile.types)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("sdk_path", help="Fit SDK archive path", type=Path)
    args = parser.parse_args()
    print(generate_profile(parse_profile(args.sdk_path)).rstrip())


if __name__ == "__main__":
    main()
