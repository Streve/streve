# Streve
# Copyright (C) 2024 The Streve Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: Copyright (C) 2024 The Streve Authors

import keyword
import logging
from dataclasses import dataclass, field
from typing import Optional

from streve.garmin_fit.base_type import BASE_TYPES_BY_NAME, BaseType


class TypeName(str):

    def __new__(cls, name: str):
        obj = str.__new__(cls, TypeName._convert(name))
        obj.raw = name
        return obj

    @staticmethod
    def _convert(name: str) -> str:
        if name in BASE_TYPES_BY_NAME:
            return name
        else:
            return _sanitize_name("".join(word.title() for word in name.split("_")))


class ValueName(str):

    def __new__(cls, name: str):
        obj = str.__new__(cls, ValueName._convert(name))
        obj.raw = name
        return obj

    @staticmethod
    def _convert(name: str) -> str:
        return _sanitize_name(name.upper())


class FieldName(str):

    def __new__(cls, name: str):
        obj = str.__new__(cls, FieldName._convert(name))
        obj.raw = name
        return obj

    @staticmethod
    def _convert(name: str) -> str:
        return _sanitize_name(name)


class MessageName(str):

    def __new__(cls, name: str):
        obj = str.__new__(cls, MessageName._convert(name))
        obj.raw = name
        return obj

    @staticmethod
    def _convert(name: str) -> str:
        return _sanitize_name("".join(word.title() for word in name.split("_")))


# names (of messages, types, enum values, fields) are sanitized to ensure they are a valid python identifier
def _sanitize_name(name: str) -> str:
    if name[0].isalpha():
        sanitized_name = name
    else:
        sanitized_name = "_" + name
        logging.info("sanitizing name: %s -> %s", name, sanitized_name)
    assert sanitized_name.isidentifier() and not keyword.iskeyword(sanitized_name)
    return sanitized_name


@dataclass
class TypeValueDefinition:
    name: ValueName
    value: int
    comment: Optional[str]


@dataclass
class TypeDefinition:
    name: TypeName
    base_type: BaseType
    values: list[TypeValueDefinition]
    comment: Optional[str]
    values_by_name: dict[ValueName, TypeValueDefinition] = field(init=False)

    def __post_init__(self):
        self.values_by_name = {value.name: value for value in self.values}


@dataclass
class MessageComponentDefinition:
    name: FieldName
    scale: Optional[int]
    offset: Optional[int]
    units: Optional[str]
    bits: int
    accumulate: bool


@dataclass
class MessageRefFieldDefinition:
    name: FieldName
    value: ValueName


@dataclass
class MessageSubfieldDefinition:
    name: FieldName
    type: TypeName
    scale: Optional[int]
    offset: Optional[int]
    units: Optional[str]
    ref_fields: list[MessageRefFieldDefinition]
    components: list[MessageComponentDefinition]
    comment: Optional[str]


@dataclass
class MessageFieldDefinition:
    name: FieldName
    number: int
    type: TypeName
    array: bool
    scale: Optional[int]
    offset: Optional[int]
    units: Optional[str]
    components: list[MessageComponentDefinition]
    subfields: list[MessageSubfieldDefinition]
    comment: Optional[str]


@dataclass
class MessageDefinition:
    name: MessageName
    number: int
    group_name: Optional[str]
    fields: list[MessageFieldDefinition]
    comment: str
    fields_by_name: dict[FieldName, MessageFieldDefinition] = field(init=False)
    fields_by_number: dict[int, MessageFieldDefinition] = field(init=False)

    def __post_init__(self):
        self.fields_by_name = {field.name: field for field in self.fields}
        self.fields_by_number = {field.number: field for field in self.fields}


@dataclass
class ProfileDefinition:
    types: list[TypeDefinition]
    messages: list[MessageDefinition]
    messages_by_number: dict[int, MessageDefinition] = field(init=False)

    def __post_init__(self):
        self.messages_by_number = {message.number: message for message in self.messages}
