# *** BEGIN GENERATED PROFILE *** #

from dataclasses import dataclass
from enum import Enum


class File(Enum):
    DEVICE = 1  # Read only, single file. Must be in root directory.
    SETTINGS = 2  # Read/write, single file. Directory=Settings
    SPORT = 3  # Read/write, multiple files, file number = sport type. Directory=Sports
    ACTIVITY = 4  # Read/erase, multiple files. Directory=Activities
    WORKOUT = 5  # Read/write/erase, multiple files. Directory=Workouts
    COURSE = 6  # Read/write/erase, multiple files. Directory=Courses
    SCHEDULES = 7  # Read/write, single file. Directory=Schedules
    WEIGHT = 9  # Read only, single file. Circular buffer. All message definitions at start of file. Directory=Weight
    TOTALS = 10  # Read only, single file. Directory=Totals
    GOALS = 11  # Read/write, single file. Directory=Goals
    BLOOD_PRESSURE = 14  # Read only. Directory=Blood Pressure
    MONITORING_A = 15  # Read only. Directory=Monitoring. File number=sub type.
    ACTIVITY_SUMMARY = 20  # Read/erase, multiple files. Directory=Activities
    MONITORING_DAILY = 28
    MONITORING_B = 32  # Read only. Directory=Monitoring. File number=identifier
    SEGMENT = 34  # Read/write/erase. Multiple Files. Directory=Segments
    SEGMENT_LIST = 35  # Read/write/erase. Single File. Directory=Segments
    EXD_CONFIGURATION = 40  # Read/write/erase. Single File. Directory=Settings
    MFG_RANGE_MIN = 247  # 0xF7 - 0xFE reserved for manufacturer specific file types
    MFG_RANGE_MAX = 254  # 0xF7 - 0xFE reserved for manufacturer specific file types


@dataclass
class MesgNum:
    value: int


MesgNum.FILE_ID = 0
MesgNum.CAPABILITIES = 1
MesgNum.DEVICE_SETTINGS = 2
MesgNum.USER_PROFILE = 3
MesgNum.HRM_PROFILE = 4
MesgNum.SDM_PROFILE = 5
MesgNum.BIKE_PROFILE = 6
MesgNum.ZONES_TARGET = 7
MesgNum.HR_ZONE = 8
MesgNum.POWER_ZONE = 9
MesgNum.MET_ZONE = 10
MesgNum.SPORT = 12
MesgNum.GOAL = 15
MesgNum.SESSION = 18
MesgNum.LAP = 19
MesgNum.RECORD = 20
MesgNum.EVENT = 21
MesgNum.DEVICE_INFO = 23
MesgNum.WORKOUT = 26
MesgNum.WORKOUT_STEP = 27
MesgNum.SCHEDULE = 28
MesgNum.WEIGHT_SCALE = 30
MesgNum.COURSE = 31
MesgNum.COURSE_POINT = 32
MesgNum.TOTALS = 33
MesgNum.ACTIVITY = 34
MesgNum.SOFTWARE = 35
MesgNum.FILE_CAPABILITIES = 37
MesgNum.MESG_CAPABILITIES = 38
MesgNum.FIELD_CAPABILITIES = 39
MesgNum.FILE_CREATOR = 49
MesgNum.BLOOD_PRESSURE = 51
MesgNum.SPEED_ZONE = 53
MesgNum.MONITORING = 55
MesgNum.TRAINING_FILE = 72
MesgNum.HRV = 78
MesgNum.ANT_RX = 80
MesgNum.ANT_TX = 81
MesgNum.ANT_CHANNEL_ID = 82
MesgNum.LENGTH = 101
MesgNum.MONITORING_INFO = 103
MesgNum.PAD = 105
MesgNum.SLAVE_DEVICE = 106
MesgNum.CONNECTIVITY = 127
MesgNum.WEATHER_CONDITIONS = 128
MesgNum.WEATHER_ALERT = 129
MesgNum.CADENCE_ZONE = 131
MesgNum.HR = 132
MesgNum.SEGMENT_LAP = 142
MesgNum.MEMO_GLOB = 145
MesgNum.SEGMENT_ID = 148
MesgNum.SEGMENT_LEADERBOARD_ENTRY = 149
MesgNum.SEGMENT_POINT = 150
MesgNum.SEGMENT_FILE = 151
MesgNum.WORKOUT_SESSION = 158
MesgNum.WATCHFACE_SETTINGS = 159
MesgNum.GPS_METADATA = 160
MesgNum.CAMERA_EVENT = 161
MesgNum.TIMESTAMP_CORRELATION = 162
MesgNum.GYROSCOPE_DATA = 164
MesgNum.ACCELEROMETER_DATA = 165
MesgNum.THREE_D_SENSOR_CALIBRATION = 167
MesgNum.VIDEO_FRAME = 169
MesgNum.OBDII_DATA = 174
MesgNum.NMEA_SENTENCE = 177
MesgNum.AVIATION_ATTITUDE = 178
MesgNum.VIDEO = 184
MesgNum.VIDEO_TITLE = 185
MesgNum.VIDEO_DESCRIPTION = 186
MesgNum.VIDEO_CLIP = 187
MesgNum.OHR_SETTINGS = 188
MesgNum.EXD_SCREEN_CONFIGURATION = 200
MesgNum.EXD_DATA_FIELD_CONFIGURATION = 201
MesgNum.EXD_DATA_CONCEPT_CONFIGURATION = 202
MesgNum.FIELD_DESCRIPTION = 206
MesgNum.DEVELOPER_DATA_ID = 207
MesgNum.MAGNETOMETER_DATA = 208
MesgNum.BAROMETER_DATA = 209
MesgNum.ONE_D_SENSOR_CALIBRATION = 210
MesgNum.MONITORING_HR_DATA = 211
MesgNum.TIME_IN_ZONE = 216
MesgNum.SET = 225
MesgNum.STRESS_LEVEL = 227
MesgNum.MAX_MET_DATA = 229
MesgNum.DIVE_SETTINGS = 258
MesgNum.DIVE_GAS = 259
MesgNum.DIVE_ALARM = 262
MesgNum.EXERCISE_TITLE = 264
MesgNum.DIVE_SUMMARY = 268
MesgNum.SPO2_DATA = 269
MesgNum.SLEEP_LEVEL = 275
MesgNum.JUMP = 285
MesgNum.AAD_ACCEL_FEATURES = 289
MesgNum.BEAT_INTERVALS = 290
MesgNum.RESPIRATION_RATE = 297
MesgNum.HSA_ACCELEROMETER_DATA = 302
MesgNum.HSA_STEP_DATA = 304
MesgNum.HSA_SPO2_DATA = 305
MesgNum.HSA_STRESS_DATA = 306
MesgNum.HSA_RESPIRATION_DATA = 307
MesgNum.HSA_HEART_RATE_DATA = 308
MesgNum.SPLIT = 312
MesgNum.SPLIT_SUMMARY = 313
MesgNum.HSA_BODY_BATTERY_DATA = 314
MesgNum.HSA_EVENT = 315
MesgNum.CLIMB_PRO = 317
MesgNum.TANK_UPDATE = 319
MesgNum.TANK_SUMMARY = 323
MesgNum.SLEEP_ASSESSMENT = 346
MesgNum.HRV_STATUS_SUMMARY = 370
MesgNum.HRV_VALUE = 371
MesgNum.RAW_BBI = 372
MesgNum.DEVICE_AUX_BATTERY_INFO = 375
MesgNum.HSA_GYROSCOPE_DATA = 376
MesgNum.CHRONO_SHOT_SESSION = 387
MesgNum.CHRONO_SHOT_DATA = 388
MesgNum.HSA_CONFIGURATION_DATA = 389
MesgNum.DIVE_APNEA_ALARM = 393
MesgNum.SKIN_TEMP_OVERNIGHT = 398
MesgNum.HSA_WRIST_TEMPERATURE_DATA = 409  # Message number for the HSA wrist temperature data message
MesgNum.MFG_RANGE_MIN = 65280  # 0xFF00 - 0xFFFE reserved for manufacturer specific messages
MesgNum.MFG_RANGE_MAX = 65534  # 0xFF00 - 0xFFFE reserved for manufacturer specific messages


@dataclass
class Checksum:
    value: int


Checksum.CLEAR = 0  # Allows clear of checksum for flash memory where can only write 1 to 0 without erasing sector.
Checksum.OK = 1  # Set to mark checksum as valid if computes to invalid values 0 or 0xFF. Checksum can also be set to ok to save encoding computation time.


@dataclass
class FileFlags:
    value: int


FileFlags.READ = 2
FileFlags.WRITE = 4
FileFlags.ERASE = 8


class MesgCount(Enum):
    NUM_PER_FILE = 0
    MAX_PER_FILE = 1
    MAX_PER_FILE_TYPE = 2


@dataclass
class DateTime:  # seconds since UTC 00:00 Dec 31 1989
    value: int


DateTime.MIN = 268435456  # if date_time is < 0x10000000 then it is system time (seconds from device power on)


@dataclass
class LocalDateTime:  # seconds since 00:00 Dec 31 1989 in local time zone
    value: int


LocalDateTime.MIN = 268435456  # if date_time is < 0x10000000 then it is system time (seconds from device power on)


@dataclass
class MessageIndex:
    value: int


MessageIndex.SELECTED = 32768  # message is selected if set
MessageIndex.RESERVED = 28672  # reserved (default 0)
MessageIndex.MASK = 4095  # index


@dataclass
class DeviceIndex:
    value: int


DeviceIndex.CREATOR = 0  # Creator of the file is always device index 0.


class Gender(Enum):
    FEMALE = 0
    MALE = 1


class Language(Enum):
    ENGLISH = 0
    FRENCH = 1
    ITALIAN = 2
    GERMAN = 3
    SPANISH = 4
    CROATIAN = 5
    CZECH = 6
    DANISH = 7
    DUTCH = 8
    FINNISH = 9
    GREEK = 10
    HUNGARIAN = 11
    NORWEGIAN = 12
    POLISH = 13
    PORTUGUESE = 14
    SLOVAKIAN = 15
    SLOVENIAN = 16
    SWEDISH = 17
    RUSSIAN = 18
    TURKISH = 19
    LATVIAN = 20
    UKRAINIAN = 21
    ARABIC = 22
    FARSI = 23
    BULGARIAN = 24
    ROMANIAN = 25
    CHINESE = 26
    JAPANESE = 27
    KOREAN = 28
    TAIWANESE = 29
    THAI = 30
    HEBREW = 31
    BRAZILIAN_PORTUGUESE = 32
    INDONESIAN = 33
    MALAYSIAN = 34
    VIETNAMESE = 35
    BURMESE = 36
    MONGOLIAN = 37
    CUSTOM = 254


@dataclass
class LanguageBits0:  # Bit field corresponding to language enum type (1 << language).
    value: int


LanguageBits0.ENGLISH = 1
LanguageBits0.FRENCH = 2
LanguageBits0.ITALIAN = 4
LanguageBits0.GERMAN = 8
LanguageBits0.SPANISH = 16
LanguageBits0.CROATIAN = 32
LanguageBits0.CZECH = 64
LanguageBits0.DANISH = 128


@dataclass
class LanguageBits1:
    value: int


LanguageBits1.DUTCH = 1
LanguageBits1.FINNISH = 2
LanguageBits1.GREEK = 4
LanguageBits1.HUNGARIAN = 8
LanguageBits1.NORWEGIAN = 16
LanguageBits1.POLISH = 32
LanguageBits1.PORTUGUESE = 64
LanguageBits1.SLOVAKIAN = 128


@dataclass
class LanguageBits2:
    value: int


LanguageBits2.SLOVENIAN = 1
LanguageBits2.SWEDISH = 2
LanguageBits2.RUSSIAN = 4
LanguageBits2.TURKISH = 8
LanguageBits2.LATVIAN = 16
LanguageBits2.UKRAINIAN = 32
LanguageBits2.ARABIC = 64
LanguageBits2.FARSI = 128


@dataclass
class LanguageBits3:
    value: int


LanguageBits3.BULGARIAN = 1
LanguageBits3.ROMANIAN = 2
LanguageBits3.CHINESE = 4
LanguageBits3.JAPANESE = 8
LanguageBits3.KOREAN = 16
LanguageBits3.TAIWANESE = 32
LanguageBits3.THAI = 64
LanguageBits3.HEBREW = 128


@dataclass
class LanguageBits4:
    value: int


LanguageBits4.BRAZILIAN_PORTUGUESE = 1
LanguageBits4.INDONESIAN = 2
LanguageBits4.MALAYSIAN = 4
LanguageBits4.VIETNAMESE = 8
LanguageBits4.BURMESE = 16
LanguageBits4.MONGOLIAN = 32


class TimeZone(Enum):
    ALMATY = 0
    BANGKOK = 1
    BOMBAY = 2
    BRASILIA = 3
    CAIRO = 4
    CAPE_VERDE_IS = 5
    DARWIN = 6
    ENIWETOK = 7
    FIJI = 8
    HONG_KONG = 9
    ISLAMABAD = 10
    KABUL = 11
    MAGADAN = 12
    MID_ATLANTIC = 13
    MOSCOW = 14
    MUSCAT = 15
    NEWFOUNDLAND = 16
    SAMOA = 17
    SYDNEY = 18
    TEHRAN = 19
    TOKYO = 20
    US_ALASKA = 21
    US_ATLANTIC = 22
    US_CENTRAL = 23
    US_EASTERN = 24
    US_HAWAII = 25
    US_MOUNTAIN = 26
    US_PACIFIC = 27
    OTHER = 28
    AUCKLAND = 29
    KATHMANDU = 30
    EUROPE_WESTERN_WET = 31
    EUROPE_CENTRAL_CET = 32
    EUROPE_EASTERN_EET = 33
    JAKARTA = 34
    PERTH = 35
    ADELAIDE = 36
    BRISBANE = 37
    TASMANIA = 38
    ICELAND = 39
    AMSTERDAM = 40
    ATHENS = 41
    BARCELONA = 42
    BERLIN = 43
    BRUSSELS = 44
    BUDAPEST = 45
    COPENHAGEN = 46
    DUBLIN = 47
    HELSINKI = 48
    LISBON = 49
    LONDON = 50
    MADRID = 51
    MUNICH = 52
    OSLO = 53
    PARIS = 54
    PRAGUE = 55
    REYKJAVIK = 56
    ROME = 57
    STOCKHOLM = 58
    VIENNA = 59
    WARSAW = 60
    ZURICH = 61
    QUEBEC = 62
    ONTARIO = 63
    MANITOBA = 64
    SASKATCHEWAN = 65
    ALBERTA = 66
    BRITISH_COLUMBIA = 67
    BOISE = 68
    BOSTON = 69
    CHICAGO = 70
    DALLAS = 71
    DENVER = 72
    KANSAS_CITY = 73
    LAS_VEGAS = 74
    LOS_ANGELES = 75
    MIAMI = 76
    MINNEAPOLIS = 77
    NEW_YORK = 78
    NEW_ORLEANS = 79
    PHOENIX = 80
    SANTA_FE = 81
    SEATTLE = 82
    WASHINGTON_DC = 83
    US_ARIZONA = 84
    CHITA = 85
    EKATERINBURG = 86
    IRKUTSK = 87
    KALININGRAD = 88
    KRASNOYARSK = 89
    NOVOSIBIRSK = 90
    PETROPAVLOVSK_KAMCHATSKIY = 91
    SAMARA = 92
    VLADIVOSTOK = 93
    MEXICO_CENTRAL = 94
    MEXICO_MOUNTAIN = 95
    MEXICO_PACIFIC = 96
    CAPE_TOWN = 97
    WINKHOEK = 98
    LAGOS = 99
    RIYAHD = 100
    VENEZUELA = 101
    AUSTRALIA_LH = 102
    SANTIAGO = 103
    MANUAL = 253
    AUTOMATIC = 254


class DisplayMeasure(Enum):
    METRIC = 0
    STATUTE = 1
    NAUTICAL = 2


class DisplayHeart(Enum):
    BPM = 0
    MAX = 1
    RESERVE = 2


class DisplayPower(Enum):
    WATTS = 0
    PERCENT_FTP = 1


class DisplayPosition(Enum):
    DEGREE = 0  # dd.dddddd
    DEGREE_MINUTE = 1  # dddmm.mmm
    DEGREE_MINUTE_SECOND = 2  # dddmmss
    AUSTRIAN_GRID = 3  # Austrian Grid (BMN)
    BRITISH_GRID = 4  # British National Grid
    DUTCH_GRID = 5  # Dutch grid system
    HUNGARIAN_GRID = 6  # Hungarian grid system
    FINNISH_GRID = 7  # Finnish grid system Zone3 KKJ27
    GERMAN_GRID = 8  # Gausss Krueger (German)
    ICELANDIC_GRID = 9  # Icelandic Grid
    INDONESIAN_EQUATORIAL = 10  # Indonesian Equatorial LCO
    INDONESIAN_IRIAN = 11  # Indonesian Irian LCO
    INDONESIAN_SOUTHERN = 12  # Indonesian Southern LCO
    INDIA_ZONE_0 = 13  # India zone 0
    INDIA_ZONE_IA = 14  # India zone IA
    INDIA_ZONE_IB = 15  # India zone IB
    INDIA_ZONE_IIA = 16  # India zone IIA
    INDIA_ZONE_IIB = 17  # India zone IIB
    INDIA_ZONE_IIIA = 18  # India zone IIIA
    INDIA_ZONE_IIIB = 19  # India zone IIIB
    INDIA_ZONE_IVA = 20  # India zone IVA
    INDIA_ZONE_IVB = 21  # India zone IVB
    IRISH_TRANSVERSE = 22  # Irish Transverse Mercator
    IRISH_GRID = 23  # Irish Grid
    LORAN = 24  # Loran TD
    MAIDENHEAD_GRID = 25  # Maidenhead grid system
    MGRS_GRID = 26  # MGRS grid system
    NEW_ZEALAND_GRID = 27  # New Zealand grid system
    NEW_ZEALAND_TRANSVERSE = 28  # New Zealand Transverse Mercator
    QATAR_GRID = 29  # Qatar National Grid
    MODIFIED_SWEDISH_GRID = 30  # Modified RT-90 (Sweden)
    SWEDISH_GRID = 31  # RT-90 (Sweden)
    SOUTH_AFRICAN_GRID = 32  # South African Grid
    SWISS_GRID = 33  # Swiss CH-1903 grid
    TAIWAN_GRID = 34  # Taiwan Grid
    UNITED_STATES_GRID = 35  # United States National Grid
    UTM_UPS_GRID = 36  # UTM/UPS grid system
    WEST_MALAYAN = 37  # West Malayan RSO
    BORNEO_RSO = 38  # Borneo RSO
    ESTONIAN_GRID = 39  # Estonian grid system
    LATVIAN_GRID = 40  # Latvian Transverse Mercator
    SWEDISH_REF_99_GRID = 41  # Reference Grid 99 TM (Swedish)


class Switch(Enum):
    OFF = 0
    ON = 1
    AUTO = 2


class Sport(Enum):
    GENERIC = 0
    RUNNING = 1
    CYCLING = 2
    TRANSITION = 3  # Mulitsport transition
    FITNESS_EQUIPMENT = 4
    SWIMMING = 5
    BASKETBALL = 6
    SOCCER = 7
    TENNIS = 8
    AMERICAN_FOOTBALL = 9
    TRAINING = 10
    WALKING = 11
    CROSS_COUNTRY_SKIING = 12
    ALPINE_SKIING = 13
    SNOWBOARDING = 14
    ROWING = 15
    MOUNTAINEERING = 16
    HIKING = 17
    MULTISPORT = 18
    PADDLING = 19
    FLYING = 20
    E_BIKING = 21
    MOTORCYCLING = 22
    BOATING = 23
    DRIVING = 24
    GOLF = 25
    HANG_GLIDING = 26
    HORSEBACK_RIDING = 27
    HUNTING = 28
    FISHING = 29
    INLINE_SKATING = 30
    ROCK_CLIMBING = 31
    SAILING = 32
    ICE_SKATING = 33
    SKY_DIVING = 34
    SNOWSHOEING = 35
    SNOWMOBILING = 36
    STAND_UP_PADDLEBOARDING = 37
    SURFING = 38
    WAKEBOARDING = 39
    WATER_SKIING = 40
    KAYAKING = 41
    RAFTING = 42
    WINDSURFING = 43
    KITESURFING = 44
    TACTICAL = 45
    JUMPMASTER = 46
    BOXING = 47
    FLOOR_CLIMBING = 48
    BASEBALL = 49
    DIVING = 53
    HIIT = 62
    RACKET = 64
    WHEELCHAIR_PUSH_WALK = 65
    WHEELCHAIR_PUSH_RUN = 66
    MEDITATION = 67
    DISC_GOLF = 69
    CRICKET = 71
    RUGBY = 72
    HOCKEY = 73
    LACROSSE = 74
    VOLLEYBALL = 75
    WATER_TUBING = 76
    WAKESURFING = 77
    MIXED_MARTIAL_ARTS = 80
    SNORKELING = 82
    DANCE = 83
    JUMP_ROPE = 84
    ALL = 254  # All is for goals only to include all sports.


@dataclass
class SportBits0:  # Bit field corresponding to sport enum type (1 << sport).
    value: int


SportBits0.GENERIC = 1
SportBits0.RUNNING = 2
SportBits0.CYCLING = 4
SportBits0.TRANSITION = 8  # Mulitsport transition
SportBits0.FITNESS_EQUIPMENT = 16
SportBits0.SWIMMING = 32
SportBits0.BASKETBALL = 64
SportBits0.SOCCER = 128


@dataclass
class SportBits1:  # Bit field corresponding to sport enum type (1 << (sport-8)).
    value: int


SportBits1.TENNIS = 1
SportBits1.AMERICAN_FOOTBALL = 2
SportBits1.TRAINING = 4
SportBits1.WALKING = 8
SportBits1.CROSS_COUNTRY_SKIING = 16
SportBits1.ALPINE_SKIING = 32
SportBits1.SNOWBOARDING = 64
SportBits1.ROWING = 128


@dataclass
class SportBits2:  # Bit field corresponding to sport enum type (1 << (sport-16)).
    value: int


SportBits2.MOUNTAINEERING = 1
SportBits2.HIKING = 2
SportBits2.MULTISPORT = 4
SportBits2.PADDLING = 8
SportBits2.FLYING = 16
SportBits2.E_BIKING = 32
SportBits2.MOTORCYCLING = 64
SportBits2.BOATING = 128


@dataclass
class SportBits3:  # Bit field corresponding to sport enum type (1 << (sport-24)).
    value: int


SportBits3.DRIVING = 1
SportBits3.GOLF = 2
SportBits3.HANG_GLIDING = 4
SportBits3.HORSEBACK_RIDING = 8
SportBits3.HUNTING = 16
SportBits3.FISHING = 32
SportBits3.INLINE_SKATING = 64
SportBits3.ROCK_CLIMBING = 128


@dataclass
class SportBits4:  # Bit field corresponding to sport enum type (1 << (sport-32)).
    value: int


SportBits4.SAILING = 1
SportBits4.ICE_SKATING = 2
SportBits4.SKY_DIVING = 4
SportBits4.SNOWSHOEING = 8
SportBits4.SNOWMOBILING = 16
SportBits4.STAND_UP_PADDLEBOARDING = 32
SportBits4.SURFING = 64
SportBits4.WAKEBOARDING = 128


@dataclass
class SportBits5:  # Bit field corresponding to sport enum type (1 << (sport-40)).
    value: int


SportBits5.WATER_SKIING = 1
SportBits5.KAYAKING = 2
SportBits5.RAFTING = 4
SportBits5.WINDSURFING = 8
SportBits5.KITESURFING = 16
SportBits5.TACTICAL = 32
SportBits5.JUMPMASTER = 64
SportBits5.BOXING = 128


@dataclass
class SportBits6:  # Bit field corresponding to sport enum type (1 << (sport-48)).
    value: int


SportBits6.FLOOR_CLIMBING = 1


class SubSport(Enum):
    GENERIC = 0
    TREADMILL = 1  # Run/Fitness Equipment
    STREET = 2  # Run
    TRAIL = 3  # Run
    TRACK = 4  # Run
    SPIN = 5  # Cycling
    INDOOR_CYCLING = 6  # Cycling/Fitness Equipment
    ROAD = 7  # Cycling
    MOUNTAIN = 8  # Cycling
    DOWNHILL = 9  # Cycling
    RECUMBENT = 10  # Cycling
    CYCLOCROSS = 11  # Cycling
    HAND_CYCLING = 12  # Cycling
    TRACK_CYCLING = 13  # Cycling
    INDOOR_ROWING = 14  # Fitness Equipment
    ELLIPTICAL = 15  # Fitness Equipment
    STAIR_CLIMBING = 16  # Fitness Equipment
    LAP_SWIMMING = 17  # Swimming
    OPEN_WATER = 18  # Swimming
    FLEXIBILITY_TRAINING = 19  # Training
    STRENGTH_TRAINING = 20  # Training
    WARM_UP = 21  # Tennis
    MATCH = 22  # Tennis
    EXERCISE = 23  # Tennis
    CHALLENGE = 24
    INDOOR_SKIING = 25  # Fitness Equipment
    CARDIO_TRAINING = 26  # Training
    INDOOR_WALKING = 27  # Walking/Fitness Equipment
    E_BIKE_FITNESS = 28  # E-Biking
    BMX = 29  # Cycling
    CASUAL_WALKING = 30  # Walking
    SPEED_WALKING = 31  # Walking
    BIKE_TO_RUN_TRANSITION = 32  # Transition
    RUN_TO_BIKE_TRANSITION = 33  # Transition
    SWIM_TO_BIKE_TRANSITION = 34  # Transition
    ATV = 35  # Motorcycling
    MOTOCROSS = 36  # Motorcycling
    BACKCOUNTRY = 37  # Alpine Skiing/Snowboarding
    RESORT = 38  # Alpine Skiing/Snowboarding
    RC_DRONE = 39  # Flying
    WINGSUIT = 40  # Flying
    WHITEWATER = 41  # Kayaking/Rafting
    SKATE_SKIING = 42  # Cross Country Skiing
    YOGA = 43  # Training
    PILATES = 44  # Fitness Equipment
    INDOOR_RUNNING = 45  # Run
    GRAVEL_CYCLING = 46  # Cycling
    E_BIKE_MOUNTAIN = 47  # Cycling
    COMMUTING = 48  # Cycling
    MIXED_SURFACE = 49  # Cycling
    NAVIGATE = 50
    TRACK_ME = 51
    MAP = 52
    SINGLE_GAS_DIVING = 53  # Diving
    MULTI_GAS_DIVING = 54  # Diving
    GAUGE_DIVING = 55  # Diving
    APNEA_DIVING = 56  # Diving
    APNEA_HUNTING = 57  # Diving
    VIRTUAL_ACTIVITY = 58
    OBSTACLE = 59  # Used for events where participants run, crawl through mud, climb over walls, etc.
    BREATHING = 62
    SAIL_RACE = 65  # Sailing
    ULTRA = 67  # Ultramarathon
    INDOOR_CLIMBING = 68  # Climbing
    BOULDERING = 69  # Climbing
    HIIT = 70  # High Intensity Interval Training
    AMRAP = 73  # HIIT
    EMOM = 74  # HIIT
    TABATA = 75  # HIIT
    PICKLEBALL = 84  # Racket
    PADEL = 85  # Racket
    INDOOR_WHEELCHAIR_WALK = 86
    INDOOR_WHEELCHAIR_RUN = 87
    INDOOR_HAND_CYCLING = 88
    SQUASH = 94
    BADMINTON = 95
    RACQUETBALL = 96
    TABLE_TENNIS = 97
    FLY_CANOPY = 110  # Flying
    FLY_PARAGLIDE = 111  # Flying
    FLY_PARAMOTOR = 112  # Flying
    FLY_PRESSURIZED = 113  # Flying
    FLY_NAVIGATE = 114  # Flying
    FLY_TIMER = 115  # Flying
    FLY_ALTIMETER = 116  # Flying
    FLY_WX = 117  # Flying
    FLY_VFR = 118  # Flying
    FLY_IFR = 119  # Flying
    ALL = 254


class SportEvent(Enum):
    UNCATEGORIZED = 0
    GEOCACHING = 1
    FITNESS = 2
    RECREATION = 3
    RACE = 4
    SPECIAL_EVENT = 5
    TRAINING = 6
    TRANSPORTATION = 7
    TOURING = 8


class Activity(Enum):
    MANUAL = 0
    AUTO_MULTI_SPORT = 1


class Intensity(Enum):
    ACTIVE = 0
    REST = 1
    WARMUP = 2
    COOLDOWN = 3
    RECOVERY = 4
    INTERVAL = 5
    OTHER = 6


class SessionTrigger(Enum):
    ACTIVITY_END = 0
    MANUAL = 1  # User changed sport.
    AUTO_MULTI_SPORT = 2  # Auto multi-sport feature is enabled and user pressed lap button to advance session.
    FITNESS_EQUIPMENT = 3  # Auto sport change caused by user linking to fitness equipment.


class AutolapTrigger(Enum):
    TIME = 0
    DISTANCE = 1
    POSITION_START = 2
    POSITION_LAP = 3
    POSITION_WAYPOINT = 4
    POSITION_MARKED = 5
    OFF = 6


class LapTrigger(Enum):
    MANUAL = 0
    TIME = 1
    DISTANCE = 2
    POSITION_START = 3
    POSITION_LAP = 4
    POSITION_WAYPOINT = 5
    POSITION_MARKED = 6
    SESSION_END = 7
    FITNESS_EQUIPMENT = 8


class TimeMode(Enum):
    HOUR12 = 0
    HOUR24 = 1  # Does not use a leading zero and has a colon
    MILITARY = 2  # Uses a leading zero and does not have a colon
    HOUR_12_WITH_SECONDS = 3
    HOUR_24_WITH_SECONDS = 4
    UTC = 5


class BacklightMode(Enum):
    OFF = 0
    MANUAL = 1
    KEY_AND_MESSAGES = 2
    AUTO_BRIGHTNESS = 3
    SMART_NOTIFICATIONS = 4
    KEY_AND_MESSAGES_NIGHT = 5
    KEY_AND_MESSAGES_AND_SMART_NOTIFICATIONS = 6


class DateMode(Enum):
    DAY_MONTH = 0
    MONTH_DAY = 1


@dataclass
class BacklightTimeout:  # Timeout in seconds.
    value: int


BacklightTimeout.INFINITE = 0  # Backlight stays on forever.


class Event(Enum):
    TIMER = 0  # Group 0. Start / stop_all
    WORKOUT = 3  # start / stop
    WORKOUT_STEP = 4  # Start at beginning of workout. Stop at end of each step.
    POWER_DOWN = 5  # stop_all group 0
    POWER_UP = 6  # stop_all group 0
    OFF_COURSE = 7  # start / stop group 0
    SESSION = 8  # Stop at end of each session.
    LAP = 9  # Stop at end of each lap.
    COURSE_POINT = 10  # marker
    BATTERY = 11  # marker
    VIRTUAL_PARTNER_PACE = 12  # Group 1. Start at beginning of activity if VP enabled, when VP pace is changed during activity or VP enabled mid activity. stop_disable when VP disabled.
    HR_HIGH_ALERT = 13  # Group 0. Start / stop when in alert condition.
    HR_LOW_ALERT = 14  # Group 0. Start / stop when in alert condition.
    SPEED_HIGH_ALERT = 15  # Group 0. Start / stop when in alert condition.
    SPEED_LOW_ALERT = 16  # Group 0. Start / stop when in alert condition.
    CAD_HIGH_ALERT = 17  # Group 0. Start / stop when in alert condition.
    CAD_LOW_ALERT = 18  # Group 0. Start / stop when in alert condition.
    POWER_HIGH_ALERT = 19  # Group 0. Start / stop when in alert condition.
    POWER_LOW_ALERT = 20  # Group 0. Start / stop when in alert condition.
    RECOVERY_HR = 21  # marker
    BATTERY_LOW = 22  # marker
    TIME_DURATION_ALERT = 23  # Group 1. Start if enabled mid activity (not required at start of activity). Stop when duration is reached. stop_disable if disabled.
    DISTANCE_DURATION_ALERT = 24  # Group 1. Start if enabled mid activity (not required at start of activity). Stop when duration is reached. stop_disable if disabled.
    CALORIE_DURATION_ALERT = 25  # Group 1. Start if enabled mid activity (not required at start of activity). Stop when duration is reached. stop_disable if disabled.
    ACTIVITY = 26  # Group 1.. Stop at end of activity.
    FITNESS_EQUIPMENT = 27  # marker
    LENGTH = 28  # Stop at end of each length.
    USER_MARKER = 32  # marker
    SPORT_POINT = 33  # marker
    CALIBRATION = 36  # start/stop/marker
    FRONT_GEAR_CHANGE = 42  # marker
    REAR_GEAR_CHANGE = 43  # marker
    RIDER_POSITION_CHANGE = 44  # marker
    ELEV_HIGH_ALERT = 45  # Group 0. Start / stop when in alert condition.
    ELEV_LOW_ALERT = 46  # Group 0. Start / stop when in alert condition.
    COMM_TIMEOUT = 47  # marker
    AUTO_ACTIVITY_DETECT = 54  # marker
    DIVE_ALERT = 56  # marker
    DIVE_GAS_SWITCHED = 57  # marker
    TANK_PRESSURE_RESERVE = 71  # marker
    TANK_PRESSURE_CRITICAL = 72  # marker
    TANK_LOST = 73  # marker
    RADAR_THREAT_ALERT = 75  # start/stop/marker
    TANK_BATTERY_LOW = 76  # marker
    TANK_POD_CONNECTED = 81  # marker - tank pod has connected
    TANK_POD_DISCONNECTED = 82  # marker - tank pod has lost connection


class EventType(Enum):
    START = 0
    STOP = 1
    CONSECUTIVE_DEPRECIATED = 2
    MARKER = 3
    STOP_ALL = 4
    BEGIN_DEPRECIATED = 5
    END_DEPRECIATED = 6
    END_ALL_DEPRECIATED = 7
    STOP_DISABLE = 8
    STOP_DISABLE_ALL = 9


class TimerTrigger(Enum):  # timer event data
    MANUAL = 0
    AUTO = 1
    FITNESS_EQUIPMENT = 2


class FitnessEquipmentState(Enum):  # fitness equipment event data
    READY = 0
    IN_USE = 1
    PAUSED = 2
    UNKNOWN = 3  # lost connection to fitness equipment


class Tone(Enum):
    OFF = 0
    TONE = 1
    VIBRATE = 2
    TONE_AND_VIBRATE = 3


class Autoscroll(Enum):
    NONE = 0
    SLOW = 1
    MEDIUM = 2
    FAST = 3


class ActivityClass(Enum):
    LEVEL = 127  # 0 to 100
    LEVEL_MAX = 100
    ATHLETE = 128


class HrZoneCalc(Enum):
    CUSTOM = 0
    PERCENT_MAX_HR = 1
    PERCENT_HRR = 2
    PERCENT_LTHR = 3


class PwrZoneCalc(Enum):
    CUSTOM = 0
    PERCENT_FTP = 1


class WktStepDuration(Enum):
    TIME = 0
    DISTANCE = 1
    HR_LESS_THAN = 2
    HR_GREATER_THAN = 3
    CALORIES = 4
    OPEN = 5
    REPEAT_UNTIL_STEPS_CMPLT = 6
    REPEAT_UNTIL_TIME = 7
    REPEAT_UNTIL_DISTANCE = 8
    REPEAT_UNTIL_CALORIES = 9
    REPEAT_UNTIL_HR_LESS_THAN = 10
    REPEAT_UNTIL_HR_GREATER_THAN = 11
    REPEAT_UNTIL_POWER_LESS_THAN = 12
    REPEAT_UNTIL_POWER_GREATER_THAN = 13
    POWER_LESS_THAN = 14
    POWER_GREATER_THAN = 15
    TRAINING_PEAKS_TSS = 16
    REPEAT_UNTIL_POWER_LAST_LAP_LESS_THAN = 17
    REPEAT_UNTIL_MAX_POWER_LAST_LAP_LESS_THAN = 18
    POWER_3S_LESS_THAN = 19
    POWER_10S_LESS_THAN = 20
    POWER_30S_LESS_THAN = 21
    POWER_3S_GREATER_THAN = 22
    POWER_10S_GREATER_THAN = 23
    POWER_30S_GREATER_THAN = 24
    POWER_LAP_LESS_THAN = 25
    POWER_LAP_GREATER_THAN = 26
    REPEAT_UNTIL_TRAINING_PEAKS_TSS = 27
    REPETITION_TIME = 28
    REPS = 29
    TIME_ONLY = 31


class WktStepTarget(Enum):
    SPEED = 0
    HEART_RATE = 1
    OPEN = 2
    CADENCE = 3
    POWER = 4
    GRADE = 5
    RESISTANCE = 6
    POWER_3S = 7
    POWER_10S = 8
    POWER_30S = 9
    POWER_LAP = 10
    SWIM_STROKE = 11
    SPEED_LAP = 12
    HEART_RATE_LAP = 13


class Goal(Enum):
    TIME = 0
    DISTANCE = 1
    CALORIES = 2
    FREQUENCY = 3
    STEPS = 4
    ASCENT = 5
    ACTIVE_MINUTES = 6


class GoalRecurrence(Enum):
    OFF = 0
    DAILY = 1
    WEEKLY = 2
    MONTHLY = 3
    YEARLY = 4
    CUSTOM = 5


class GoalSource(Enum):
    AUTO = 0  # Device generated
    COMMUNITY = 1  # Social network sourced goal
    USER = 2  # Manually generated


class Schedule(Enum):
    WORKOUT = 0
    COURSE = 1


class CoursePoint(Enum):
    GENERIC = 0
    SUMMIT = 1
    VALLEY = 2
    WATER = 3
    FOOD = 4
    DANGER = 5
    LEFT = 6
    RIGHT = 7
    STRAIGHT = 8
    FIRST_AID = 9
    FOURTH_CATEGORY = 10
    THIRD_CATEGORY = 11
    SECOND_CATEGORY = 12
    FIRST_CATEGORY = 13
    HORS_CATEGORY = 14
    SPRINT = 15
    LEFT_FORK = 16
    RIGHT_FORK = 17
    MIDDLE_FORK = 18
    SLIGHT_LEFT = 19
    SHARP_LEFT = 20
    SLIGHT_RIGHT = 21
    SHARP_RIGHT = 22
    U_TURN = 23
    SEGMENT_START = 24
    SEGMENT_END = 25
    CAMPSITE = 27
    AID_STATION = 28
    REST_AREA = 29
    GENERAL_DISTANCE = 30  # Used with UpAhead
    SERVICE = 31
    ENERGY_GEL = 32
    SPORTS_DRINK = 33
    MILE_MARKER = 34
    CHECKPOINT = 35
    SHELTER = 36
    MEETING_SPOT = 37
    OVERLOOK = 38
    TOILET = 39
    SHOWER = 40
    GEAR = 41
    SHARP_CURVE = 42
    STEEP_INCLINE = 43
    TUNNEL = 44
    BRIDGE = 45
    OBSTACLE = 46
    CROSSING = 47
    STORE = 48
    TRANSITION = 49
    NAVAID = 50
    TRANSPORT = 51
    ALERT = 52
    INFO = 53


@dataclass
class Manufacturer:
    value: int


Manufacturer.GARMIN = 1
Manufacturer.GARMIN_FR405_ANTFS = 2  # Do not use. Used by FR405 for ANTFS man id.
Manufacturer.ZEPHYR = 3
Manufacturer.DAYTON = 4
Manufacturer.IDT = 5
Manufacturer.SRM = 6
Manufacturer.QUARQ = 7
Manufacturer.IBIKE = 8
Manufacturer.SARIS = 9
Manufacturer.SPARK_HK = 10
Manufacturer.TANITA = 11
Manufacturer.ECHOWELL = 12
Manufacturer.DYNASTREAM_OEM = 13
Manufacturer.NAUTILUS = 14
Manufacturer.DYNASTREAM = 15
Manufacturer.TIMEX = 16
Manufacturer.METRIGEAR = 17
Manufacturer.XELIC = 18
Manufacturer.BEURER = 19
Manufacturer.CARDIOSPORT = 20
Manufacturer.A_AND_D = 21
Manufacturer.HMM = 22
Manufacturer.SUUNTO = 23
Manufacturer.THITA_ELEKTRONIK = 24
Manufacturer.GPULSE = 25
Manufacturer.CLEAN_MOBILE = 26
Manufacturer.PEDAL_BRAIN = 27
Manufacturer.PEAKSWARE = 28
Manufacturer.SAXONAR = 29
Manufacturer.LEMOND_FITNESS = 30
Manufacturer.DEXCOM = 31
Manufacturer.WAHOO_FITNESS = 32
Manufacturer.OCTANE_FITNESS = 33
Manufacturer.ARCHINOETICS = 34
Manufacturer.THE_HURT_BOX = 35
Manufacturer.CITIZEN_SYSTEMS = 36
Manufacturer.MAGELLAN = 37
Manufacturer.OSYNCE = 38
Manufacturer.HOLUX = 39
Manufacturer.CONCEPT2 = 40
Manufacturer.SHIMANO = 41
Manufacturer.ONE_GIANT_LEAP = 42
Manufacturer.ACE_SENSOR = 43
Manufacturer.BRIM_BROTHERS = 44
Manufacturer.XPLOVA = 45
Manufacturer.PERCEPTION_DIGITAL = 46
Manufacturer.BF1SYSTEMS = 47
Manufacturer.PIONEER = 48
Manufacturer.SPANTEC = 49
Manufacturer.METALOGICS = 50
Manufacturer._4IIIIS = 51
Manufacturer.SEIKO_EPSON = 52
Manufacturer.SEIKO_EPSON_OEM = 53
Manufacturer.IFOR_POWELL = 54
Manufacturer.MAXWELL_GUIDER = 55
Manufacturer.STAR_TRAC = 56
Manufacturer.BREAKAWAY = 57
Manufacturer.ALATECH_TECHNOLOGY_LTD = 58
Manufacturer.MIO_TECHNOLOGY_EUROPE = 59
Manufacturer.ROTOR = 60
Manufacturer.GEONAUTE = 61
Manufacturer.ID_BIKE = 62
Manufacturer.SPECIALIZED = 63
Manufacturer.WTEK = 64
Manufacturer.PHYSICAL_ENTERPRISES = 65
Manufacturer.NORTH_POLE_ENGINEERING = 66
Manufacturer.BKOOL = 67
Manufacturer.CATEYE = 68
Manufacturer.STAGES_CYCLING = 69
Manufacturer.SIGMASPORT = 70
Manufacturer.TOMTOM = 71
Manufacturer.PERIPEDAL = 72
Manufacturer.WATTBIKE = 73
Manufacturer.MOXY = 76
Manufacturer.CICLOSPORT = 77
Manufacturer.POWERBAHN = 78
Manufacturer.ACORN_PROJECTS_APS = 79
Manufacturer.LIFEBEAM = 80
Manufacturer.BONTRAGER = 81
Manufacturer.WELLGO = 82
Manufacturer.SCOSCHE = 83
Manufacturer.MAGURA = 84
Manufacturer.WOODWAY = 85
Manufacturer.ELITE = 86
Manufacturer.NIELSEN_KELLERMAN = 87
Manufacturer.DK_CITY = 88
Manufacturer.TACX = 89
Manufacturer.DIRECTION_TECHNOLOGY = 90
Manufacturer.MAGTONIC = 91
Manufacturer._1PARTCARBON = 92
Manufacturer.INSIDE_RIDE_TECHNOLOGIES = 93
Manufacturer.SOUND_OF_MOTION = 94
Manufacturer.STRYD = 95
Manufacturer.ICG = 96  # Indoorcycling Group
Manufacturer.MIPULSE = 97
Manufacturer.BSX_ATHLETICS = 98
Manufacturer.LOOK = 99
Manufacturer.CAMPAGNOLO_SRL = 100
Manufacturer.BODY_BIKE_SMART = 101
Manufacturer.PRAXISWORKS = 102
Manufacturer.LIMITS_TECHNOLOGY = 103  # Limits Technology Ltd.
Manufacturer.TOPACTION_TECHNOLOGY = 104  # TopAction Technology Inc.
Manufacturer.COSINUSS = 105
Manufacturer.FITCARE = 106
Manufacturer.MAGENE = 107
Manufacturer.GIANT_MANUFACTURING_CO = 108
Manufacturer.TIGRASPORT = 109  # Tigrasport
Manufacturer.SALUTRON = 110
Manufacturer.TECHNOGYM = 111
Manufacturer.BRYTON_SENSORS = 112
Manufacturer.LATITUDE_LIMITED = 113
Manufacturer.SOARING_TECHNOLOGY = 114
Manufacturer.IGPSPORT = 115
Manufacturer.THINKRIDER = 116
Manufacturer.GOPHER_SPORT = 117
Manufacturer.WATERROWER = 118
Manufacturer.ORANGETHEORY = 119
Manufacturer.INPEAK = 120
Manufacturer.KINETIC = 121
Manufacturer.JOHNSON_HEALTH_TECH = 122
Manufacturer.POLAR_ELECTRO = 123
Manufacturer.SEESENSE = 124
Manufacturer.NCI_TECHNOLOGY = 125
Manufacturer.IQSQUARE = 126
Manufacturer.LEOMO = 127
Manufacturer.IFIT_COM = 128
Manufacturer.COROS_BYTE = 129
Manufacturer.VERSA_DESIGN = 130
Manufacturer.CHILEAF = 131
Manufacturer.CYCPLUS = 132
Manufacturer.GRAVAA_BYTE = 133
Manufacturer.SIGEYI = 134
Manufacturer.COOSPO = 135
Manufacturer.GEOID = 136
Manufacturer.BOSCH = 137
Manufacturer.KYTO = 138
Manufacturer.KINETIC_SPORTS = 139
Manufacturer.DECATHLON_BYTE = 140
Manufacturer.TQ_SYSTEMS = 141
Manufacturer.TAG_HEUER = 142
Manufacturer.KEISER_FITNESS = 143
Manufacturer.ZWIFT_BYTE = 144
Manufacturer.PORSCHE_EP = 145
Manufacturer.BLACKBIRD = 146
Manufacturer.MEILAN_BYTE = 147
Manufacturer.EZON = 148
Manufacturer.LAISI = 149
Manufacturer.MYZONE = 150
Manufacturer.DEVELOPMENT = 255
Manufacturer.HEALTHANDLIFE = 257
Manufacturer.LEZYNE = 258
Manufacturer.SCRIBE_LABS = 259
Manufacturer.ZWIFT = 260
Manufacturer.WATTEAM = 261
Manufacturer.RECON = 262
Manufacturer.FAVERO_ELECTRONICS = 263
Manufacturer.DYNOVELO = 264
Manufacturer.STRAVA = 265
Manufacturer.PRECOR = 266  # Amer Sports
Manufacturer.BRYTON = 267
Manufacturer.SRAM = 268
Manufacturer.NAVMAN = 269  # MiTAC Global Corporation (Mio Technology)
Manufacturer.COBI = 270  # COBI GmbH
Manufacturer.SPIVI = 271
Manufacturer.MIO_MAGELLAN = 272
Manufacturer.EVESPORTS = 273
Manufacturer.SENSITIVUS_GAUGE = 274
Manufacturer.PODOON = 275
Manufacturer.LIFE_TIME_FITNESS = 276
Manufacturer.FALCO_E_MOTORS = 277  # Falco eMotors Inc.
Manufacturer.MINOURA = 278
Manufacturer.CYCLIQ = 279
Manufacturer.LUXOTTICA = 280
Manufacturer.TRAINER_ROAD = 281
Manufacturer.THE_SUFFERFEST = 282
Manufacturer.FULLSPEEDAHEAD = 283
Manufacturer.VIRTUALTRAINING = 284
Manufacturer.FEEDBACKSPORTS = 285
Manufacturer.OMATA = 286
Manufacturer.VDO = 287
Manufacturer.MAGNETICDAYS = 288
Manufacturer.HAMMERHEAD = 289
Manufacturer.KINETIC_BY_KURT = 290
Manufacturer.SHAPELOG = 291
Manufacturer.DABUZIDUO = 292
Manufacturer.JETBLACK = 293
Manufacturer.COROS = 294
Manufacturer.VIRTUGO = 295
Manufacturer.VELOSENSE = 296
Manufacturer.CYCLIGENTINC = 297
Manufacturer.TRAILFORKS = 298
Manufacturer.MAHLE_EBIKEMOTION = 299
Manufacturer.NURVV = 300
Manufacturer.MICROPROGRAM = 301
Manufacturer.ZONE5CLOUD = 302
Manufacturer.GREENTEG = 303
Manufacturer.YAMAHA_MOTORS = 304
Manufacturer.WHOOP = 305
Manufacturer.GRAVAA = 306
Manufacturer.ONELAP = 307
Manufacturer.MONARK_EXERCISE = 308
Manufacturer.FORM = 309
Manufacturer.DECATHLON = 310
Manufacturer.SYNCROS = 311
Manufacturer.HEATUP = 312
Manufacturer.CANNONDALE = 313
Manufacturer.TRUE_FITNESS = 314
Manufacturer.RGT_CYCLING = 315
Manufacturer.VASA = 316
Manufacturer.RACE_REPUBLIC = 317
Manufacturer.FAZUA = 318
Manufacturer.OREKA_TRAINING = 319
Manufacturer.LSEC = 320  # Lishun Electric & Communication
Manufacturer.LULULEMON_STUDIO = 321
Manufacturer.SHANYUE = 322
Manufacturer.SPINNING_MDA = 323
Manufacturer.HILLDATING = 324
Manufacturer.AERO_SENSOR = 325
Manufacturer.NIKE = 326
Manufacturer.MAGICSHINE = 327
Manufacturer.ICTRAINER = 328
Manufacturer.ABSOLUTE_CYCLING = 329
Manufacturer.ACTIGRAPHCORP = 5759


@dataclass
class GarminProduct:
    value: int


GarminProduct.HRM1 = 1
GarminProduct.AXH01 = 2  # AXH01 HRM chipset
GarminProduct.AXB01 = 3
GarminProduct.AXB02 = 4
GarminProduct.HRM2SS = 5
GarminProduct.DSI_ALF02 = 6
GarminProduct.HRM3SS = 7
GarminProduct.HRM_RUN_SINGLE_BYTE_PRODUCT_ID = 8  # hrm_run model for HRM ANT+ messaging
GarminProduct.BSM = 9  # BSM model for ANT+ messaging
GarminProduct.BCM = 10  # BCM model for ANT+ messaging
GarminProduct.AXS01 = 11  # AXS01 HRM Bike Chipset model for ANT+ messaging
GarminProduct.HRM_TRI_SINGLE_BYTE_PRODUCT_ID = 12  # hrm_tri model for HRM ANT+ messaging
GarminProduct.HRM4_RUN_SINGLE_BYTE_PRODUCT_ID = 13  # hrm4 run model for HRM ANT+ messaging
GarminProduct.FR225_SINGLE_BYTE_PRODUCT_ID = 14  # fr225 model for HRM ANT+ messaging
GarminProduct.GEN3_BSM_SINGLE_BYTE_PRODUCT_ID = 15  # gen3_bsm model for Bike Speed ANT+ messaging
GarminProduct.GEN3_BCM_SINGLE_BYTE_PRODUCT_ID = 16  # gen3_bcm model for Bike Cadence ANT+ messaging
GarminProduct.HRM_FIT_SINGLE_BYTE_PRODUCT_ID = 22
GarminProduct.OHR = 255  # Garmin Wearable Optical Heart Rate Sensor for ANT+ HR Profile Broadcasting
GarminProduct.FR301_CHINA = 473
GarminProduct.FR301_JAPAN = 474
GarminProduct.FR301_KOREA = 475
GarminProduct.FR301_TAIWAN = 494
GarminProduct.FR405 = 717  # Forerunner 405
GarminProduct.FR50 = 782  # Forerunner 50
GarminProduct.FR405_JAPAN = 987
GarminProduct.FR60 = 988  # Forerunner 60
GarminProduct.DSI_ALF01 = 1011
GarminProduct.FR310XT = 1018  # Forerunner 310
GarminProduct.EDGE500 = 1036
GarminProduct.FR110 = 1124  # Forerunner 110
GarminProduct.EDGE800 = 1169
GarminProduct.EDGE500_TAIWAN = 1199
GarminProduct.EDGE500_JAPAN = 1213
GarminProduct.CHIRP = 1253
GarminProduct.FR110_JAPAN = 1274
GarminProduct.EDGE200 = 1325
GarminProduct.FR910XT = 1328
GarminProduct.EDGE800_TAIWAN = 1333
GarminProduct.EDGE800_JAPAN = 1334
GarminProduct.ALF04 = 1341
GarminProduct.FR610 = 1345
GarminProduct.FR210_JAPAN = 1360
GarminProduct.VECTOR_SS = 1380
GarminProduct.VECTOR_CP = 1381
GarminProduct.EDGE800_CHINA = 1386
GarminProduct.EDGE500_CHINA = 1387
GarminProduct.APPROACH_G10 = 1405
GarminProduct.FR610_JAPAN = 1410
GarminProduct.EDGE500_KOREA = 1422
GarminProduct.FR70 = 1436
GarminProduct.FR310XT_4T = 1446
GarminProduct.AMX = 1461
GarminProduct.FR10 = 1482
GarminProduct.EDGE800_KOREA = 1497
GarminProduct.SWIM = 1499
GarminProduct.FR910XT_CHINA = 1537
GarminProduct.FENIX = 1551
GarminProduct.EDGE200_TAIWAN = 1555
GarminProduct.EDGE510 = 1561
GarminProduct.EDGE810 = 1567
GarminProduct.TEMPE = 1570
GarminProduct.FR910XT_JAPAN = 1600
GarminProduct.FR620 = 1623
GarminProduct.FR220 = 1632
GarminProduct.FR910XT_KOREA = 1664
GarminProduct.FR10_JAPAN = 1688
GarminProduct.EDGE810_JAPAN = 1721
GarminProduct.VIRB_ELITE = 1735
GarminProduct.EDGE_TOURING = 1736  # Also Edge Touring Plus
GarminProduct.EDGE510_JAPAN = 1742
GarminProduct.HRM_TRI = 1743  # Also HRM-Swim
GarminProduct.HRM_RUN = 1752
GarminProduct.FR920XT = 1765
GarminProduct.EDGE510_ASIA = 1821
GarminProduct.EDGE810_CHINA = 1822
GarminProduct.EDGE810_TAIWAN = 1823
GarminProduct.EDGE1000 = 1836
GarminProduct.VIVO_FIT = 1837
GarminProduct.VIRB_REMOTE = 1853
GarminProduct.VIVO_KI = 1885
GarminProduct.FR15 = 1903
GarminProduct.VIVO_ACTIVE = 1907
GarminProduct.EDGE510_KOREA = 1918
GarminProduct.FR620_JAPAN = 1928
GarminProduct.FR620_CHINA = 1929
GarminProduct.FR220_JAPAN = 1930
GarminProduct.FR220_CHINA = 1931
GarminProduct.APPROACH_S6 = 1936
GarminProduct.VIVO_SMART = 1956
GarminProduct.FENIX2 = 1967
GarminProduct.EPIX = 1988
GarminProduct.FENIX3 = 2050
GarminProduct.EDGE1000_TAIWAN = 2052
GarminProduct.EDGE1000_JAPAN = 2053
GarminProduct.FR15_JAPAN = 2061
GarminProduct.EDGE520 = 2067
GarminProduct.EDGE1000_CHINA = 2070
GarminProduct.FR620_RUSSIA = 2072
GarminProduct.FR220_RUSSIA = 2073
GarminProduct.VECTOR_S = 2079
GarminProduct.EDGE1000_KOREA = 2100
GarminProduct.FR920XT_TAIWAN = 2130
GarminProduct.FR920XT_CHINA = 2131
GarminProduct.FR920XT_JAPAN = 2132
GarminProduct.VIRBX = 2134
GarminProduct.VIVO_SMART_APAC = 2135
GarminProduct.ETREX_TOUCH = 2140
GarminProduct.EDGE25 = 2147
GarminProduct.FR25 = 2148
GarminProduct.VIVO_FIT2 = 2150
GarminProduct.FR225 = 2153
GarminProduct.FR630 = 2156
GarminProduct.FR230 = 2157
GarminProduct.FR735XT = 2158
GarminProduct.VIVO_ACTIVE_APAC = 2160
GarminProduct.VECTOR_2 = 2161
GarminProduct.VECTOR_2S = 2162
GarminProduct.VIRBXE = 2172
GarminProduct.FR620_TAIWAN = 2173
GarminProduct.FR220_TAIWAN = 2174
GarminProduct.TRUSWING = 2175
GarminProduct.D2AIRVENU = 2187
GarminProduct.FENIX3_CHINA = 2188
GarminProduct.FENIX3_TWN = 2189
GarminProduct.VARIA_HEADLIGHT = 2192
GarminProduct.VARIA_TAILLIGHT_OLD = 2193
GarminProduct.EDGE_EXPLORE_1000 = 2204
GarminProduct.FR225_ASIA = 2219
GarminProduct.VARIA_RADAR_TAILLIGHT = 2225
GarminProduct.VARIA_RADAR_DISPLAY = 2226
GarminProduct.EDGE20 = 2238
GarminProduct.EDGE520_ASIA = 2260
GarminProduct.EDGE520_JAPAN = 2261
GarminProduct.D2_BRAVO = 2262
GarminProduct.APPROACH_S20 = 2266
GarminProduct.VIVO_SMART2 = 2271
GarminProduct.EDGE1000_THAI = 2274
GarminProduct.VARIA_REMOTE = 2276
GarminProduct.EDGE25_ASIA = 2288
GarminProduct.EDGE25_JPN = 2289
GarminProduct.EDGE20_ASIA = 2290
GarminProduct.APPROACH_X40 = 2292
GarminProduct.FENIX3_JAPAN = 2293
GarminProduct.VIVO_SMART_EMEA = 2294
GarminProduct.FR630_ASIA = 2310
GarminProduct.FR630_JPN = 2311
GarminProduct.FR230_JPN = 2313
GarminProduct.HRM4_RUN = 2327
GarminProduct.EPIX_JAPAN = 2332
GarminProduct.VIVO_ACTIVE_HR = 2337
GarminProduct.VIVO_SMART_GPS_HR = 2347
GarminProduct.VIVO_SMART_HR = 2348
GarminProduct.VIVO_SMART_HR_ASIA = 2361
GarminProduct.VIVO_SMART_GPS_HR_ASIA = 2362
GarminProduct.VIVO_MOVE = 2368
GarminProduct.VARIA_TAILLIGHT = 2379
GarminProduct.FR235_ASIA = 2396
GarminProduct.FR235_JAPAN = 2397
GarminProduct.VARIA_VISION = 2398
GarminProduct.VIVO_FIT3 = 2406
GarminProduct.FENIX3_KOREA = 2407
GarminProduct.FENIX3_SEA = 2408
GarminProduct.FENIX3_HR = 2413
GarminProduct.VIRB_ULTRA_30 = 2417
GarminProduct.INDEX_SMART_SCALE = 2429
GarminProduct.FR235 = 2431
GarminProduct.FENIX3_CHRONOS = 2432
GarminProduct.OREGON7XX = 2441
GarminProduct.RINO7XX = 2444
GarminProduct.EPIX_KOREA = 2457
GarminProduct.FENIX3_HR_CHN = 2473
GarminProduct.FENIX3_HR_TWN = 2474
GarminProduct.FENIX3_HR_JPN = 2475
GarminProduct.FENIX3_HR_SEA = 2476
GarminProduct.FENIX3_HR_KOR = 2477
GarminProduct.NAUTIX = 2496
GarminProduct.VIVO_ACTIVE_HR_APAC = 2497
GarminProduct.FR35 = 2503
GarminProduct.OREGON7XX_WW = 2512
GarminProduct.EDGE_820 = 2530
GarminProduct.EDGE_EXPLORE_820 = 2531
GarminProduct.FR735XT_APAC = 2533
GarminProduct.FR735XT_JAPAN = 2534
GarminProduct.FENIX5S = 2544
GarminProduct.D2_BRAVO_TITANIUM = 2547
GarminProduct.VARIA_UT800 = 2567  # Varia UT 800 SW
GarminProduct.RUNNING_DYNAMICS_POD = 2593
GarminProduct.EDGE_820_CHINA = 2599
GarminProduct.EDGE_820_JAPAN = 2600
GarminProduct.FENIX5X = 2604
GarminProduct.VIVO_FIT_JR = 2606
GarminProduct.VIVO_SMART3 = 2622
GarminProduct.VIVO_SPORT = 2623
GarminProduct.EDGE_820_TAIWAN = 2628
GarminProduct.EDGE_820_KOREA = 2629
GarminProduct.EDGE_820_SEA = 2630
GarminProduct.FR35_HEBREW = 2650
GarminProduct.APPROACH_S60 = 2656
GarminProduct.FR35_APAC = 2667
GarminProduct.FR35_JAPAN = 2668
GarminProduct.FENIX3_CHRONOS_ASIA = 2675
GarminProduct.VIRB_360 = 2687
GarminProduct.FR935 = 2691
GarminProduct.FENIX5 = 2697
GarminProduct.VIVOACTIVE3 = 2700
GarminProduct.FR235_CHINA_NFC = 2733
GarminProduct.FORETREX_601_701 = 2769
GarminProduct.VIVO_MOVE_HR = 2772
GarminProduct.EDGE_1030 = 2713
GarminProduct.FR35_SEA = 2727
GarminProduct.VECTOR_3 = 2787
GarminProduct.FENIX5_ASIA = 2796
GarminProduct.FENIX5S_ASIA = 2797
GarminProduct.FENIX5X_ASIA = 2798
GarminProduct.APPROACH_Z80 = 2806
GarminProduct.FR35_KOREA = 2814
GarminProduct.D2CHARLIE = 2819
GarminProduct.VIVO_SMART3_APAC = 2831
GarminProduct.VIVO_SPORT_APAC = 2832
GarminProduct.FR935_ASIA = 2833
GarminProduct.DESCENT = 2859
GarminProduct.VIVO_FIT4 = 2878
GarminProduct.FR645 = 2886
GarminProduct.FR645M = 2888
GarminProduct.FR30 = 2891
GarminProduct.FENIX5S_PLUS = 2900
GarminProduct.EDGE_130 = 2909
GarminProduct.EDGE_1030_ASIA = 2924
GarminProduct.VIVOSMART_4 = 2927
GarminProduct.VIVO_MOVE_HR_ASIA = 2945
GarminProduct.APPROACH_X10 = 2962
GarminProduct.FR30_ASIA = 2977
GarminProduct.VIVOACTIVE3M_W = 2988
GarminProduct.FR645_ASIA = 3003
GarminProduct.FR645M_ASIA = 3004
GarminProduct.EDGE_EXPLORE = 3011
GarminProduct.GPSMAP66 = 3028
GarminProduct.APPROACH_S10 = 3049
GarminProduct.VIVOACTIVE3M_L = 3066
GarminProduct.APPROACH_G80 = 3085
GarminProduct.EDGE_130_ASIA = 3092
GarminProduct.EDGE_1030_BONTRAGER = 3095
GarminProduct.FENIX5_PLUS = 3110
GarminProduct.FENIX5X_PLUS = 3111
GarminProduct.EDGE_520_PLUS = 3112
GarminProduct.FR945 = 3113
GarminProduct.EDGE_530 = 3121
GarminProduct.EDGE_830 = 3122
GarminProduct.INSTINCT_ESPORTS = 3126
GarminProduct.FENIX5S_PLUS_APAC = 3134
GarminProduct.FENIX5X_PLUS_APAC = 3135
GarminProduct.EDGE_520_PLUS_APAC = 3142
GarminProduct.DESCENT_T1 = 3143
GarminProduct.FR235L_ASIA = 3144
GarminProduct.FR245_ASIA = 3145
GarminProduct.VIVO_ACTIVE3M_APAC = 3163
GarminProduct.GEN3_BSM = 3192  # gen3 bike speed sensor
GarminProduct.GEN3_BCM = 3193  # gen3 bike cadence sensor
GarminProduct.VIVO_SMART4_ASIA = 3218
GarminProduct.VIVOACTIVE4_SMALL = 3224
GarminProduct.VIVOACTIVE4_LARGE = 3225
GarminProduct.VENU = 3226
GarminProduct.MARQ_DRIVER = 3246
GarminProduct.MARQ_AVIATOR = 3247
GarminProduct.MARQ_CAPTAIN = 3248
GarminProduct.MARQ_COMMANDER = 3249
GarminProduct.MARQ_EXPEDITION = 3250
GarminProduct.MARQ_ATHLETE = 3251
GarminProduct.DESCENT_MK2 = 3258
GarminProduct.GPSMAP66I = 3284
GarminProduct.FENIX6S_SPORT = 3287
GarminProduct.FENIX6S = 3288
GarminProduct.FENIX6_SPORT = 3289
GarminProduct.FENIX6 = 3290
GarminProduct.FENIX6X = 3291
GarminProduct.HRM_DUAL = 3299  # HRM-Dual
GarminProduct.HRM_PRO = 3300  # HRM-Pro
GarminProduct.VIVO_MOVE3_PREMIUM = 3308
GarminProduct.APPROACH_S40 = 3314
GarminProduct.FR245M_ASIA = 3321
GarminProduct.EDGE_530_APAC = 3349
GarminProduct.EDGE_830_APAC = 3350
GarminProduct.VIVO_MOVE3 = 3378
GarminProduct.VIVO_ACTIVE4_SMALL_ASIA = 3387
GarminProduct.VIVO_ACTIVE4_LARGE_ASIA = 3388
GarminProduct.VIVO_ACTIVE4_OLED_ASIA = 3389
GarminProduct.SWIM2 = 3405
GarminProduct.MARQ_DRIVER_ASIA = 3420
GarminProduct.MARQ_AVIATOR_ASIA = 3421
GarminProduct.VIVO_MOVE3_ASIA = 3422
GarminProduct.FR945_ASIA = 3441
GarminProduct.VIVO_ACTIVE3T_CHN = 3446
GarminProduct.MARQ_CAPTAIN_ASIA = 3448
GarminProduct.MARQ_COMMANDER_ASIA = 3449
GarminProduct.MARQ_EXPEDITION_ASIA = 3450
GarminProduct.MARQ_ATHLETE_ASIA = 3451
GarminProduct.INSTINCT_SOLAR = 3466
GarminProduct.FR45_ASIA = 3469
GarminProduct.VIVOACTIVE3_DAIMLER = 3473
GarminProduct.LEGACY_REY = 3498
GarminProduct.LEGACY_DARTH_VADER = 3499
GarminProduct.LEGACY_CAPTAIN_MARVEL = 3500
GarminProduct.LEGACY_FIRST_AVENGER = 3501
GarminProduct.FENIX6S_SPORT_ASIA = 3512
GarminProduct.FENIX6S_ASIA = 3513
GarminProduct.FENIX6_SPORT_ASIA = 3514
GarminProduct.FENIX6_ASIA = 3515
GarminProduct.FENIX6X_ASIA = 3516
GarminProduct.LEGACY_CAPTAIN_MARVEL_ASIA = 3535
GarminProduct.LEGACY_FIRST_AVENGER_ASIA = 3536
GarminProduct.LEGACY_REY_ASIA = 3537
GarminProduct.LEGACY_DARTH_VADER_ASIA = 3538
GarminProduct.DESCENT_MK2S = 3542
GarminProduct.EDGE_130_PLUS = 3558
GarminProduct.EDGE_1030_PLUS = 3570
GarminProduct.RALLY_200 = 3578  # Rally 100/200 Power Meter Series
GarminProduct.FR745 = 3589
GarminProduct.VENUSQ = 3600
GarminProduct.LILY = 3615
GarminProduct.MARQ_ADVENTURER = 3624
GarminProduct.ENDURO = 3638
GarminProduct.SWIM2_APAC = 3639
GarminProduct.MARQ_ADVENTURER_ASIA = 3648
GarminProduct.FR945_LTE = 3652
GarminProduct.DESCENT_MK2_ASIA = 3702  # Mk2 and Mk2i
GarminProduct.VENU2 = 3703
GarminProduct.VENU2S = 3704
GarminProduct.VENU_DAIMLER_ASIA = 3737
GarminProduct.MARQ_GOLFER = 3739
GarminProduct.VENU_DAIMLER = 3740
GarminProduct.FR745_ASIA = 3794
GarminProduct.VARIA_RCT715 = 3808
GarminProduct.LILY_ASIA = 3809
GarminProduct.EDGE_1030_PLUS_ASIA = 3812
GarminProduct.EDGE_130_PLUS_ASIA = 3813
GarminProduct.APPROACH_S12 = 3823
GarminProduct.ENDURO_ASIA = 3872
GarminProduct.VENUSQ_ASIA = 3837
GarminProduct.EDGE_1040 = 3843
GarminProduct.MARQ_GOLFER_ASIA = 3850
GarminProduct.VENU2_PLUS = 3851
GarminProduct.GNSS = 3865
GarminProduct.FR55 = 3869
GarminProduct.INSTINCT_2 = 3888
GarminProduct.FENIX7S = 3905
GarminProduct.FENIX7 = 3906
GarminProduct.FENIX7X = 3907
GarminProduct.FENIX7S_APAC = 3908
GarminProduct.FENIX7_APAC = 3909
GarminProduct.FENIX7X_APAC = 3910
GarminProduct.APPROACH_G12 = 14631
GarminProduct.DESCENT_MK2S_ASIA = 3930
GarminProduct.APPROACH_S42 = 3934
GarminProduct.EPIX_GEN2 = 3943
GarminProduct.EPIX_GEN2_APAC = 3944
GarminProduct.VENU2S_ASIA = 3949
GarminProduct.VENU2_ASIA = 3950
GarminProduct.FR945_LTE_ASIA = 3978
GarminProduct.VIVO_MOVE_SPORT = 3982
GarminProduct.VIVOMOVE_TREND = 3983
GarminProduct.APPROACH_S12_ASIA = 3986
GarminProduct.FR255_MUSIC = 3990
GarminProduct.FR255_SMALL_MUSIC = 3991
GarminProduct.FR255 = 3992
GarminProduct.FR255_SMALL = 3993
GarminProduct.APPROACH_G12_ASIA = 16385
GarminProduct.APPROACH_S42_ASIA = 4002
GarminProduct.DESCENT_G1 = 4005
GarminProduct.VENU2_PLUS_ASIA = 4017
GarminProduct.FR955 = 4024
GarminProduct.FR55_ASIA = 4033
GarminProduct.EDGE_540 = 4061
GarminProduct.EDGE_840 = 4062
GarminProduct.VIVOSMART_5 = 4063
GarminProduct.INSTINCT_2_ASIA = 4071
GarminProduct.MARQ_GEN2 = 4105  # Adventurer, Athlete, Captain, Golfer
GarminProduct.VENUSQ2 = 4115
GarminProduct.VENUSQ2MUSIC = 4116
GarminProduct.MARQ_GEN2_AVIATOR = 4124
GarminProduct.D2_AIR_X10 = 4125
GarminProduct.HRM_PRO_PLUS = 4130
GarminProduct.DESCENT_G1_ASIA = 4132
GarminProduct.TACTIX7 = 4135
GarminProduct.INSTINCT_CROSSOVER = 4155
GarminProduct.EDGE_EXPLORE2 = 4169
GarminProduct.DESCENT_MK3 = 4222
GarminProduct.DESCENT_MK3I = 4223
GarminProduct.APPROACH_S70 = 4233
GarminProduct.FR265_LARGE = 4257
GarminProduct.FR265_SMALL = 4258
GarminProduct.VENU3 = 4260
GarminProduct.VENU3S = 4261
GarminProduct.TACX_NEO_SMART = 4265  # Neo Smart, Tacx
GarminProduct.TACX_NEO2_SMART = 4266  # Neo 2 Smart, Tacx
GarminProduct.TACX_NEO2_T_SMART = 4267  # Neo 2T Smart, Tacx
GarminProduct.TACX_NEO_SMART_BIKE = 4268  # Neo Smart Bike, Tacx
GarminProduct.TACX_SATORI_SMART = 4269  # Satori Smart, Tacx
GarminProduct.TACX_FLOW_SMART = 4270  # Flow Smart, Tacx
GarminProduct.TACX_VORTEX_SMART = 4271  # Vortex Smart, Tacx
GarminProduct.TACX_BUSHIDO_SMART = 4272  # Bushido Smart, Tacx
GarminProduct.TACX_GENIUS_SMART = 4273  # Genius Smart, Tacx
GarminProduct.TACX_FLUX_FLUX_S_SMART = 4274  # Flux/Flux S Smart, Tacx
GarminProduct.TACX_FLUX2_SMART = 4275  # Flux 2 Smart, Tacx
GarminProduct.TACX_MAGNUM = 4276  # Magnum, Tacx
GarminProduct.EDGE_1040_ASIA = 4305
GarminProduct.EPIX_GEN2_PRO_42 = 4312
GarminProduct.EPIX_GEN2_PRO_47 = 4313
GarminProduct.EPIX_GEN2_PRO_51 = 4314
GarminProduct.FR965 = 4315
GarminProduct.ENDURO2 = 4341
GarminProduct.FENIX7S_PRO_SOLAR = 4374
GarminProduct.FENIX7_PRO_SOLAR = 4375
GarminProduct.FENIX7X_PRO_SOLAR = 4376
GarminProduct.LILY2 = 4380
GarminProduct.INSTINCT_2X = 4394
GarminProduct.VIVOACTIVE5 = 4426
GarminProduct.FR165 = 4432
GarminProduct.FR165_MUSIC = 4433
GarminProduct.DESCENT_T2 = 4442
GarminProduct.HRM_FIT = 4446
GarminProduct.MARQ_GEN2_COMMANDER = 4472
GarminProduct.D2_MACH1_PRO = 4556
GarminProduct.SDM4 = 10007  # SDM4 footpod
GarminProduct.EDGE_REMOTE = 10014
GarminProduct.TACX_TRAINING_APP_WIN = 20533
GarminProduct.TACX_TRAINING_APP_MAC = 20534
GarminProduct.TACX_TRAINING_APP_MAC_CATALYST = 20565
GarminProduct.TRAINING_CENTER = 20119
GarminProduct.TACX_TRAINING_APP_ANDROID = 30045
GarminProduct.TACX_TRAINING_APP_IOS = 30046
GarminProduct.TACX_TRAINING_APP_LEGACY = 30047
GarminProduct.CONNECTIQ_SIMULATOR = 65531
GarminProduct.ANDROID_ANTPLUS_PLUGIN = 65532
GarminProduct.CONNECT = 65534  # Garmin Connect website


@dataclass
class AntplusDeviceType:
    value: int


AntplusDeviceType.ANTFS = 1
AntplusDeviceType.BIKE_POWER = 11
AntplusDeviceType.ENVIRONMENT_SENSOR_LEGACY = 12
AntplusDeviceType.MULTI_SPORT_SPEED_DISTANCE = 15
AntplusDeviceType.CONTROL = 16
AntplusDeviceType.FITNESS_EQUIPMENT = 17
AntplusDeviceType.BLOOD_PRESSURE = 18
AntplusDeviceType.GEOCACHE_NODE = 19
AntplusDeviceType.LIGHT_ELECTRIC_VEHICLE = 20
AntplusDeviceType.ENV_SENSOR = 25
AntplusDeviceType.RACQUET = 26
AntplusDeviceType.CONTROL_HUB = 27
AntplusDeviceType.MUSCLE_OXYGEN = 31
AntplusDeviceType.SHIFTING = 34
AntplusDeviceType.BIKE_LIGHT_MAIN = 35
AntplusDeviceType.BIKE_LIGHT_SHARED = 36
AntplusDeviceType.EXD = 38
AntplusDeviceType.BIKE_RADAR = 40
AntplusDeviceType.BIKE_AERO = 46
AntplusDeviceType.WEIGHT_SCALE = 119
AntplusDeviceType.HEART_RATE = 120
AntplusDeviceType.BIKE_SPEED_CADENCE = 121
AntplusDeviceType.BIKE_CADENCE = 122
AntplusDeviceType.BIKE_SPEED = 123
AntplusDeviceType.STRIDE_SPEED_DISTANCE = 124


class AntNetwork(Enum):
    PUBLIC = 0
    ANTPLUS = 1
    ANTFS = 2
    PRIVATE = 3


@dataclass
class WorkoutCapabilities:
    value: int


WorkoutCapabilities.INTERVAL = 1
WorkoutCapabilities.CUSTOM = 2
WorkoutCapabilities.FITNESS_EQUIPMENT = 4
WorkoutCapabilities.FIRSTBEAT = 8
WorkoutCapabilities.NEW_LEAF = 16
WorkoutCapabilities.TCX = 32  # For backwards compatibility. Watch should add missing id fields then clear flag.
WorkoutCapabilities.SPEED = 128  # Speed source required for workout step.
WorkoutCapabilities.HEART_RATE = 256  # Heart rate source required for workout step.
WorkoutCapabilities.DISTANCE = 512  # Distance source required for workout step.
WorkoutCapabilities.CADENCE = 1024  # Cadence source required for workout step.
WorkoutCapabilities.POWER = 2048  # Power source required for workout step.
WorkoutCapabilities.GRADE = 4096  # Grade source required for workout step.
WorkoutCapabilities.RESISTANCE = 8192  # Resistance source required for workout step.
WorkoutCapabilities.PROTECTED = 16384


@dataclass
class BatteryStatus:
    value: int


BatteryStatus.NEW = 1
BatteryStatus.GOOD = 2
BatteryStatus.OK = 3
BatteryStatus.LOW = 4
BatteryStatus.CRITICAL = 5
BatteryStatus.CHARGING = 6
BatteryStatus.UNKNOWN = 7


class HrType(Enum):
    NORMAL = 0
    IRREGULAR = 1


@dataclass
class CourseCapabilities:
    value: int


CourseCapabilities.PROCESSED = 1
CourseCapabilities.VALID = 2
CourseCapabilities.TIME = 4
CourseCapabilities.DISTANCE = 8
CourseCapabilities.POSITION = 16
CourseCapabilities.HEART_RATE = 32
CourseCapabilities.POWER = 64
CourseCapabilities.CADENCE = 128
CourseCapabilities.TRAINING = 256
CourseCapabilities.NAVIGATION = 512
CourseCapabilities.BIKEWAY = 1024
CourseCapabilities.AVIATION = 4096  # Denote course files to be used as flight plans


@dataclass
class Weight:
    value: int


Weight.CALCULATING = 65534


@dataclass
class WorkoutHr:  # 0 - 100 indicates% of max hr; >100 indicates bpm (255 max) plus 100
    value: int


WorkoutHr.BPM_OFFSET = 100


@dataclass
class WorkoutPower:  # 0 - 1000 indicates % of functional threshold power; >1000 indicates watts plus 1000.
    value: int


WorkoutPower.WATTS_OFFSET = 1000


class BpStatus(Enum):
    NO_ERROR = 0
    ERROR_INCOMPLETE_DATA = 1
    ERROR_NO_MEASUREMENT = 2
    ERROR_DATA_OUT_OF_RANGE = 3
    ERROR_IRREGULAR_HEART_RATE = 4


@dataclass
class UserLocalId:
    value: int


UserLocalId.LOCAL_MIN = 0
UserLocalId.LOCAL_MAX = 15
UserLocalId.STATIONARY_MIN = 16
UserLocalId.STATIONARY_MAX = 255
UserLocalId.PORTABLE_MIN = 256
UserLocalId.PORTABLE_MAX = 65534


class SwimStroke(Enum):
    FREESTYLE = 0
    BACKSTROKE = 1
    BREASTSTROKE = 2
    BUTTERFLY = 3
    DRILL = 4
    MIXED = 5
    IM = 6  # IM is a mixed interval containing the same number of lengths for each of: Butterfly, Backstroke, Breaststroke, Freestyle, swam in that order.


class ActivityType(Enum):
    GENERIC = 0
    RUNNING = 1
    CYCLING = 2
    TRANSITION = 3  # Mulitsport transition
    FITNESS_EQUIPMENT = 4
    SWIMMING = 5
    WALKING = 6
    SEDENTARY = 8
    ALL = 254  # All is for goals only to include all sports.


class ActivitySubtype(Enum):
    GENERIC = 0
    TREADMILL = 1  # Run
    STREET = 2  # Run
    TRAIL = 3  # Run
    TRACK = 4  # Run
    SPIN = 5  # Cycling
    INDOOR_CYCLING = 6  # Cycling
    ROAD = 7  # Cycling
    MOUNTAIN = 8  # Cycling
    DOWNHILL = 9  # Cycling
    RECUMBENT = 10  # Cycling
    CYCLOCROSS = 11  # Cycling
    HAND_CYCLING = 12  # Cycling
    TRACK_CYCLING = 13  # Cycling
    INDOOR_ROWING = 14  # Fitness Equipment
    ELLIPTICAL = 15  # Fitness Equipment
    STAIR_CLIMBING = 16  # Fitness Equipment
    LAP_SWIMMING = 17  # Swimming
    OPEN_WATER = 18  # Swimming
    ALL = 254


class ActivityLevel(Enum):
    LOW = 0
    MEDIUM = 1
    HIGH = 2


class Side(Enum):
    RIGHT = 0
    LEFT = 1


@dataclass
class LeftRightBalance:
    value: int


LeftRightBalance.MASK = 127  # % contribution
LeftRightBalance.RIGHT = 128  # data corresponds to right if set, otherwise unknown


@dataclass
class LeftRightBalance100:
    value: int


LeftRightBalance100.MASK = 16383  # % contribution scaled by 100
LeftRightBalance100.RIGHT = 32768  # data corresponds to right if set, otherwise unknown


class LengthType(Enum):
    IDLE = 0  # Rest period. Length with no strokes
    ACTIVE = 1  # Length with strokes.


class DayOfWeek(Enum):
    SUNDAY = 0
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6


@dataclass
class ConnectivityCapabilities:
    value: int


ConnectivityCapabilities.BLUETOOTH = 1
ConnectivityCapabilities.BLUETOOTH_LE = 2
ConnectivityCapabilities.ANT = 4
ConnectivityCapabilities.ACTIVITY_UPLOAD = 8
ConnectivityCapabilities.COURSE_DOWNLOAD = 16
ConnectivityCapabilities.WORKOUT_DOWNLOAD = 32
ConnectivityCapabilities.LIVE_TRACK = 64
ConnectivityCapabilities.WEATHER_CONDITIONS = 128
ConnectivityCapabilities.WEATHER_ALERTS = 256
ConnectivityCapabilities.GPS_EPHEMERIS_DOWNLOAD = 512
ConnectivityCapabilities.EXPLICIT_ARCHIVE = 1024
ConnectivityCapabilities.SETUP_INCOMPLETE = 2048
ConnectivityCapabilities.CONTINUE_SYNC_AFTER_SOFTWARE_UPDATE = 4096
ConnectivityCapabilities.CONNECT_IQ_APP_DOWNLOAD = 8192
ConnectivityCapabilities.GOLF_COURSE_DOWNLOAD = 16384
ConnectivityCapabilities.DEVICE_INITIATES_SYNC = 32768  # Indicates device is in control of initiating all syncs
ConnectivityCapabilities.CONNECT_IQ_WATCH_APP_DOWNLOAD = 65536
ConnectivityCapabilities.CONNECT_IQ_WIDGET_DOWNLOAD = 131072
ConnectivityCapabilities.CONNECT_IQ_WATCH_FACE_DOWNLOAD = 262144
ConnectivityCapabilities.CONNECT_IQ_DATA_FIELD_DOWNLOAD = 524288
ConnectivityCapabilities.CONNECT_IQ_APP_MANAGMENT = 1048576  # Device supports delete and reorder of apps via GCM
ConnectivityCapabilities.SWING_SENSOR = 2097152
ConnectivityCapabilities.SWING_SENSOR_REMOTE = 4194304
ConnectivityCapabilities.INCIDENT_DETECTION = 8388608  # Device supports incident detection
ConnectivityCapabilities.AUDIO_PROMPTS = 16777216
ConnectivityCapabilities.WIFI_VERIFICATION = 33554432  # Device supports reporting wifi verification via GCM
ConnectivityCapabilities.TRUE_UP = 67108864  # Device supports True Up
ConnectivityCapabilities.FIND_MY_WATCH = 134217728  # Device supports Find My Watch
ConnectivityCapabilities.REMOTE_MANUAL_SYNC = 268435456
ConnectivityCapabilities.LIVE_TRACK_AUTO_START = 536870912  # Device supports LiveTrack auto start
ConnectivityCapabilities.LIVE_TRACK_MESSAGING = 1073741824  # Device supports LiveTrack Messaging
ConnectivityCapabilities.INSTANT_INPUT = 2147483648  # Device supports instant input feature


class WeatherReport(Enum):
    CURRENT = 0
    FORECAST = 1  # Deprecated use hourly_forecast instead
    HOURLY_FORECAST = 1
    DAILY_FORECAST = 2


class WeatherStatus(Enum):
    CLEAR = 0
    PARTLY_CLOUDY = 1
    MOSTLY_CLOUDY = 2
    RAIN = 3
    SNOW = 4
    WINDY = 5
    THUNDERSTORMS = 6
    WINTRY_MIX = 7
    FOG = 8
    HAZY = 11
    HAIL = 12
    SCATTERED_SHOWERS = 13
    SCATTERED_THUNDERSTORMS = 14
    UNKNOWN_PRECIPITATION = 15
    LIGHT_RAIN = 16
    HEAVY_RAIN = 17
    LIGHT_SNOW = 18
    HEAVY_SNOW = 19
    LIGHT_RAIN_SNOW = 20
    HEAVY_RAIN_SNOW = 21
    CLOUDY = 22


class WeatherSeverity(Enum):
    UNKNOWN = 0
    WARNING = 1
    WATCH = 2
    ADVISORY = 3
    STATEMENT = 4


class WeatherSevereType(Enum):
    UNSPECIFIED = 0
    TORNADO = 1
    TSUNAMI = 2
    HURRICANE = 3
    EXTREME_WIND = 4
    TYPHOON = 5
    INLAND_HURRICANE = 6
    HURRICANE_FORCE_WIND = 7
    WATERSPOUT = 8
    SEVERE_THUNDERSTORM = 9
    WRECKHOUSE_WINDS = 10
    LES_SUETES_WIND = 11
    AVALANCHE = 12
    FLASH_FLOOD = 13
    TROPICAL_STORM = 14
    INLAND_TROPICAL_STORM = 15
    BLIZZARD = 16
    ICE_STORM = 17
    FREEZING_RAIN = 18
    DEBRIS_FLOW = 19
    FLASH_FREEZE = 20
    DUST_STORM = 21
    HIGH_WIND = 22
    WINTER_STORM = 23
    HEAVY_FREEZING_SPRAY = 24
    EXTREME_COLD = 25
    WIND_CHILL = 26
    COLD_WAVE = 27
    HEAVY_SNOW_ALERT = 28
    LAKE_EFFECT_BLOWING_SNOW = 29
    SNOW_SQUALL = 30
    LAKE_EFFECT_SNOW = 31
    WINTER_WEATHER = 32
    SLEET = 33
    SNOWFALL = 34
    SNOW_AND_BLOWING_SNOW = 35
    BLOWING_SNOW = 36
    SNOW_ALERT = 37
    ARCTIC_OUTFLOW = 38
    FREEZING_DRIZZLE = 39
    STORM = 40
    STORM_SURGE = 41
    RAINFALL = 42
    AREAL_FLOOD = 43
    COASTAL_FLOOD = 44
    LAKESHORE_FLOOD = 45
    EXCESSIVE_HEAT = 46
    HEAT = 47
    WEATHER = 48
    HIGH_HEAT_AND_HUMIDITY = 49
    HUMIDEX_AND_HEALTH = 50
    HUMIDEX = 51
    GALE = 52
    FREEZING_SPRAY = 53
    SPECIAL_MARINE = 54
    SQUALL = 55
    STRONG_WIND = 56
    LAKE_WIND = 57
    MARINE_WEATHER = 58
    WIND = 59
    SMALL_CRAFT_HAZARDOUS_SEAS = 60
    HAZARDOUS_SEAS = 61
    SMALL_CRAFT = 62
    SMALL_CRAFT_WINDS = 63
    SMALL_CRAFT_ROUGH_BAR = 64
    HIGH_WATER_LEVEL = 65
    ASHFALL = 66
    FREEZING_FOG = 67
    DENSE_FOG = 68
    DENSE_SMOKE = 69
    BLOWING_DUST = 70
    HARD_FREEZE = 71
    FREEZE = 72
    FROST = 73
    FIRE_WEATHER = 74
    FLOOD = 75
    RIP_TIDE = 76
    HIGH_SURF = 77
    SMOG = 78
    AIR_QUALITY = 79
    BRISK_WIND = 80
    AIR_STAGNATION = 81
    LOW_WATER = 82
    HYDROLOGICAL = 83
    SPECIAL_WEATHER = 84


@dataclass
class TimeIntoDay:  # number of seconds into the day since 00:00:00 UTC
    value: int


@dataclass
class LocaltimeIntoDay:  # number of seconds into the day since local 00:00:00
    value: int


class StrokeType(Enum):
    NO_EVENT = 0
    OTHER = 1  # stroke was detected but cannot be identified
    SERVE = 2
    FOREHAND = 3
    BACKHAND = 4
    SMASH = 5


class BodyLocation(Enum):
    LEFT_LEG = 0
    LEFT_CALF = 1
    LEFT_SHIN = 2
    LEFT_HAMSTRING = 3
    LEFT_QUAD = 4
    LEFT_GLUTE = 5
    RIGHT_LEG = 6
    RIGHT_CALF = 7
    RIGHT_SHIN = 8
    RIGHT_HAMSTRING = 9
    RIGHT_QUAD = 10
    RIGHT_GLUTE = 11
    TORSO_BACK = 12
    LEFT_LOWER_BACK = 13
    LEFT_UPPER_BACK = 14
    RIGHT_LOWER_BACK = 15
    RIGHT_UPPER_BACK = 16
    TORSO_FRONT = 17
    LEFT_ABDOMEN = 18
    LEFT_CHEST = 19
    RIGHT_ABDOMEN = 20
    RIGHT_CHEST = 21
    LEFT_ARM = 22
    LEFT_SHOULDER = 23
    LEFT_BICEP = 24
    LEFT_TRICEP = 25
    LEFT_BRACHIORADIALIS = 26  # Left anterior forearm
    LEFT_FOREARM_EXTENSORS = 27  # Left posterior forearm
    RIGHT_ARM = 28
    RIGHT_SHOULDER = 29
    RIGHT_BICEP = 30
    RIGHT_TRICEP = 31
    RIGHT_BRACHIORADIALIS = 32  # Right anterior forearm
    RIGHT_FOREARM_EXTENSORS = 33  # Right posterior forearm
    NECK = 34
    THROAT = 35
    WAIST_MID_BACK = 36
    WAIST_FRONT = 37
    WAIST_LEFT = 38
    WAIST_RIGHT = 39


class SegmentLapStatus(Enum):
    END = 0
    FAIL = 1


class SegmentLeaderboardType(Enum):
    OVERALL = 0
    PERSONAL_BEST = 1
    CONNECTIONS = 2
    GROUP = 3
    CHALLENGER = 4
    KOM = 5
    QOM = 6
    PR = 7
    GOAL = 8
    CARROT = 9
    CLUB_LEADER = 10
    RIVAL = 11
    LAST = 12
    RECENT_BEST = 13
    COURSE_RECORD = 14


class SegmentDeleteStatus(Enum):
    DO_NOT_DELETE = 0
    DELETE_ONE = 1
    DELETE_ALL = 2


class SegmentSelectionType(Enum):
    STARRED = 0
    SUGGESTED = 1


class SourceType(Enum):
    ANT = 0  # External device connected with ANT
    ANTPLUS = 1  # External device connected with ANT+
    BLUETOOTH = 2  # External device connected with BT
    BLUETOOTH_LOW_ENERGY = 3  # External device connected with BLE
    WIFI = 4  # External device connected with Wifi
    LOCAL = 5  # Onboard device


@dataclass
class LocalDeviceType:
    value: int


LocalDeviceType.GPS = 0  # Onboard gps receiver
LocalDeviceType.GLONASS = 1  # Onboard glonass receiver
LocalDeviceType.GPS_GLONASS = 2  # Onboard gps glonass receiver
LocalDeviceType.ACCELEROMETER = 3  # Onboard sensor
LocalDeviceType.BAROMETER = 4  # Onboard sensor
LocalDeviceType.TEMPERATURE = 5  # Onboard sensor
LocalDeviceType.WHR = 10  # Onboard wrist HR sensor
LocalDeviceType.SENSOR_HUB = 12  # Onboard software package


@dataclass
class BleDeviceType:
    value: int


BleDeviceType.CONNECTED_GPS = 0  # GPS that is provided over a proprietary bluetooth service
BleDeviceType.HEART_RATE = 1
BleDeviceType.BIKE_POWER = 2
BleDeviceType.BIKE_SPEED_CADENCE = 3
BleDeviceType.BIKE_SPEED = 4
BleDeviceType.BIKE_CADENCE = 5
BleDeviceType.FOOTPOD = 6
BleDeviceType.BIKE_TRAINER = 7  # Indoor-Bike FTMS protocol


@dataclass
class AntChannelId:
    value: int


AntChannelId.ANT_EXTENDED_DEVICE_NUMBER_UPPER_NIBBLE = 4026531840
AntChannelId.ANT_TRANSMISSION_TYPE_LOWER_NIBBLE = 251658240
AntChannelId.ANT_DEVICE_TYPE = 16711680
AntChannelId.ANT_DEVICE_NUMBER = 65535


class DisplayOrientation(Enum):
    AUTO = 0  # automatic if the device supports it
    PORTRAIT = 1
    LANDSCAPE = 2
    PORTRAIT_FLIPPED = 3  # portrait mode but rotated 180 degrees
    LANDSCAPE_FLIPPED = 4  # landscape mode but rotated 180 degrees


class WorkoutEquipment(Enum):
    NONE = 0
    SWIM_FINS = 1
    SWIM_KICKBOARD = 2
    SWIM_PADDLES = 3
    SWIM_PULL_BUOY = 4
    SWIM_SNORKEL = 5


class WatchfaceMode(Enum):
    DIGITAL = 0
    ANALOG = 1
    CONNECT_IQ = 2
    DISABLED = 3


class DigitalWatchfaceLayout(Enum):
    TRADITIONAL = 0
    MODERN = 1
    BOLD = 2


class AnalogWatchfaceLayout(Enum):
    MINIMAL = 0
    TRADITIONAL = 1
    MODERN = 2


class RiderPositionType(Enum):
    SEATED = 0
    STANDING = 1
    TRANSITION_TO_SEATED = 2
    TRANSITION_TO_STANDING = 3


class PowerPhaseType(Enum):
    POWER_PHASE_START_ANGLE = 0
    POWER_PHASE_END_ANGLE = 1
    POWER_PHASE_ARC_LENGTH = 2
    POWER_PHASE_CENTER = 3


class CameraEventType(Enum):
    VIDEO_START = 0  # Start of video recording
    VIDEO_SPLIT = 1  # Mark of video file split (end of one file, beginning of the other)
    VIDEO_END = 2  # End of video recording
    PHOTO_TAKEN = 3  # Still photo taken
    VIDEO_SECOND_STREAM_START = 4
    VIDEO_SECOND_STREAM_SPLIT = 5
    VIDEO_SECOND_STREAM_END = 6
    VIDEO_SPLIT_START = 7  # Mark of video file split start
    VIDEO_SECOND_STREAM_SPLIT_START = 8
    VIDEO_PAUSE = 11  # Mark when a video recording has been paused
    VIDEO_SECOND_STREAM_PAUSE = 12
    VIDEO_RESUME = 13  # Mark when a video recording has been resumed
    VIDEO_SECOND_STREAM_RESUME = 14


class SensorType(Enum):
    ACCELEROMETER = 0
    GYROSCOPE = 1
    COMPASS = 2  # Magnetometer
    BAROMETER = 3


class BikeLightNetworkConfigType(Enum):
    AUTO = 0
    INDIVIDUAL = 4
    HIGH_VISIBILITY = 5
    TRAIL = 6


@dataclass
class CommTimeoutType:
    value: int


CommTimeoutType.WILDCARD_PAIRING_TIMEOUT = 0  # Timeout pairing to any device
CommTimeoutType.PAIRING_TIMEOUT = 1  # Timeout pairing to previously paired device
CommTimeoutType.CONNECTION_LOST = 2  # Temporary loss of communications
CommTimeoutType.CONNECTION_TIMEOUT = 3  # Connection closed due to extended bad communications


class CameraOrientationType(Enum):
    CAMERA_ORIENTATION_0 = 0
    CAMERA_ORIENTATION_90 = 1
    CAMERA_ORIENTATION_180 = 2
    CAMERA_ORIENTATION_270 = 3


class AttitudeStage(Enum):
    FAILED = 0
    ALIGNING = 1
    DEGRADED = 2
    VALID = 3


@dataclass
class AttitudeValidity:
    value: int


AttitudeValidity.TRACK_ANGLE_HEADING_VALID = 1
AttitudeValidity.PITCH_VALID = 2
AttitudeValidity.ROLL_VALID = 4
AttitudeValidity.LATERAL_BODY_ACCEL_VALID = 8
AttitudeValidity.NORMAL_BODY_ACCEL_VALID = 16
AttitudeValidity.TURN_RATE_VALID = 32
AttitudeValidity.HW_FAIL = 64
AttitudeValidity.MAG_INVALID = 128
AttitudeValidity.NO_GPS = 256
AttitudeValidity.GPS_INVALID = 512
AttitudeValidity.SOLUTION_COASTING = 1024
AttitudeValidity.TRUE_TRACK_ANGLE = 2048
AttitudeValidity.MAGNETIC_HEADING = 4096


class AutoSyncFrequency(Enum):
    NEVER = 0
    OCCASIONALLY = 1
    FREQUENT = 2
    ONCE_A_DAY = 3
    REMOTE = 4


class ExdLayout(Enum):
    FULL_SCREEN = 0
    HALF_VERTICAL = 1
    HALF_HORIZONTAL = 2
    HALF_VERTICAL_RIGHT_SPLIT = 3
    HALF_HORIZONTAL_BOTTOM_SPLIT = 4
    FULL_QUARTER_SPLIT = 5
    HALF_VERTICAL_LEFT_SPLIT = 6
    HALF_HORIZONTAL_TOP_SPLIT = 7
    DYNAMIC = 8  # The EXD may display the configured concepts in any layout it sees fit.


class ExdDisplayType(Enum):
    NUMERICAL = 0
    SIMPLE = 1
    GRAPH = 2
    BAR = 3
    CIRCLE_GRAPH = 4
    VIRTUAL_PARTNER = 5
    BALANCE = 6
    STRING_LIST = 7
    STRING = 8
    SIMPLE_DYNAMIC_ICON = 9
    GAUGE = 10


class ExdDataUnits(Enum):
    NO_UNITS = 0
    LAPS = 1
    MILES_PER_HOUR = 2
    KILOMETERS_PER_HOUR = 3
    FEET_PER_HOUR = 4
    METERS_PER_HOUR = 5
    DEGREES_CELSIUS = 6
    DEGREES_FARENHEIT = 7
    ZONE = 8
    GEAR = 9
    RPM = 10
    BPM = 11
    DEGREES = 12
    MILLIMETERS = 13
    METERS = 14
    KILOMETERS = 15
    FEET = 16
    YARDS = 17
    KILOFEET = 18
    MILES = 19
    TIME = 20
    ENUM_TURN_TYPE = 21
    PERCENT = 22
    WATTS = 23
    WATTS_PER_KILOGRAM = 24
    ENUM_BATTERY_STATUS = 25
    ENUM_BIKE_LIGHT_BEAM_ANGLE_MODE = 26
    ENUM_BIKE_LIGHT_BATTERY_STATUS = 27
    ENUM_BIKE_LIGHT_NETWORK_CONFIG_TYPE = 28
    LIGHTS = 29
    SECONDS = 30
    MINUTES = 31
    HOURS = 32
    CALORIES = 33
    KILOJOULES = 34
    MILLISECONDS = 35
    SECOND_PER_MILE = 36
    SECOND_PER_KILOMETER = 37
    CENTIMETER = 38
    ENUM_COURSE_POINT = 39
    BRADIANS = 40
    ENUM_SPORT = 41
    INCHES_HG = 42
    MM_HG = 43
    MBARS = 44
    HECTO_PASCALS = 45
    FEET_PER_MIN = 46
    METERS_PER_MIN = 47
    METERS_PER_SEC = 48
    EIGHT_CARDINAL = 49


class ExdQualifiers(Enum):
    NO_QUALIFIER = 0
    INSTANTANEOUS = 1
    AVERAGE = 2
    LAP = 3
    MAXIMUM = 4
    MAXIMUM_AVERAGE = 5
    MAXIMUM_LAP = 6
    LAST_LAP = 7
    AVERAGE_LAP = 8
    TO_DESTINATION = 9
    TO_GO = 10
    TO_NEXT = 11
    NEXT_COURSE_POINT = 12
    TOTAL = 13
    THREE_SECOND_AVERAGE = 14
    TEN_SECOND_AVERAGE = 15
    THIRTY_SECOND_AVERAGE = 16
    PERCENT_MAXIMUM = 17
    PERCENT_MAXIMUM_AVERAGE = 18
    LAP_PERCENT_MAXIMUM = 19
    ELAPSED = 20
    SUNRISE = 21
    SUNSET = 22
    COMPARED_TO_VIRTUAL_PARTNER = 23
    MAXIMUM_24H = 24
    MINIMUM_24H = 25
    MINIMUM = 26
    FIRST = 27
    SECOND = 28
    THIRD = 29
    SHIFTER = 30
    LAST_SPORT = 31
    MOVING = 32
    STOPPED = 33
    ESTIMATED_TOTAL = 34
    ZONE_9 = 242
    ZONE_8 = 243
    ZONE_7 = 244
    ZONE_6 = 245
    ZONE_5 = 246
    ZONE_4 = 247
    ZONE_3 = 248
    ZONE_2 = 249
    ZONE_1 = 250


class ExdDescriptors(Enum):
    BIKE_LIGHT_BATTERY_STATUS = 0
    BEAM_ANGLE_STATUS = 1
    BATERY_LEVEL = 2
    LIGHT_NETWORK_MODE = 3
    NUMBER_LIGHTS_CONNECTED = 4
    CADENCE = 5
    DISTANCE = 6
    ESTIMATED_TIME_OF_ARRIVAL = 7
    HEADING = 8
    TIME = 9
    BATTERY_LEVEL = 10
    TRAINER_RESISTANCE = 11
    TRAINER_TARGET_POWER = 12
    TIME_SEATED = 13
    TIME_STANDING = 14
    ELEVATION = 15
    GRADE = 16
    ASCENT = 17
    DESCENT = 18
    VERTICAL_SPEED = 19
    DI2_BATTERY_LEVEL = 20
    FRONT_GEAR = 21
    REAR_GEAR = 22
    GEAR_RATIO = 23
    HEART_RATE = 24
    HEART_RATE_ZONE = 25
    TIME_IN_HEART_RATE_ZONE = 26
    HEART_RATE_RESERVE = 27
    CALORIES = 28
    GPS_ACCURACY = 29
    GPS_SIGNAL_STRENGTH = 30
    TEMPERATURE = 31
    TIME_OF_DAY = 32
    BALANCE = 33
    PEDAL_SMOOTHNESS = 34
    POWER = 35
    FUNCTIONAL_THRESHOLD_POWER = 36
    INTENSITY_FACTOR = 37
    WORK = 38
    POWER_RATIO = 39
    NORMALIZED_POWER = 40
    TRAINING_STRESS_SCORE = 41
    TIME_ON_ZONE = 42
    SPEED = 43
    LAPS = 44
    REPS = 45
    WORKOUT_STEP = 46
    COURSE_DISTANCE = 47
    NAVIGATION_DISTANCE = 48
    COURSE_ESTIMATED_TIME_OF_ARRIVAL = 49
    NAVIGATION_ESTIMATED_TIME_OF_ARRIVAL = 50
    COURSE_TIME = 51
    NAVIGATION_TIME = 52
    COURSE_HEADING = 53
    NAVIGATION_HEADING = 54
    POWER_ZONE = 55
    TORQUE_EFFECTIVENESS = 56
    TIMER_TIME = 57
    POWER_WEIGHT_RATIO = 58
    LEFT_PLATFORM_CENTER_OFFSET = 59
    RIGHT_PLATFORM_CENTER_OFFSET = 60
    LEFT_POWER_PHASE_START_ANGLE = 61
    RIGHT_POWER_PHASE_START_ANGLE = 62
    LEFT_POWER_PHASE_FINISH_ANGLE = 63
    RIGHT_POWER_PHASE_FINISH_ANGLE = 64
    GEARS = 65  # Combined gear information
    PACE = 66
    TRAINING_EFFECT = 67
    VERTICAL_OSCILLATION = 68
    VERTICAL_RATIO = 69
    GROUND_CONTACT_TIME = 70
    LEFT_GROUND_CONTACT_TIME_BALANCE = 71
    RIGHT_GROUND_CONTACT_TIME_BALANCE = 72
    STRIDE_LENGTH = 73
    RUNNING_CADENCE = 74
    PERFORMANCE_CONDITION = 75
    COURSE_TYPE = 76
    TIME_IN_POWER_ZONE = 77
    NAVIGATION_TURN = 78
    COURSE_LOCATION = 79
    NAVIGATION_LOCATION = 80
    COMPASS = 81
    GEAR_COMBO = 82
    MUSCLE_OXYGEN = 83
    ICON = 84
    COMPASS_HEADING = 85
    GPS_HEADING = 86
    GPS_ELEVATION = 87
    ANAEROBIC_TRAINING_EFFECT = 88
    COURSE = 89
    OFF_COURSE = 90
    GLIDE_RATIO = 91
    VERTICAL_DISTANCE = 92
    VMG = 93
    AMBIENT_PRESSURE = 94
    PRESSURE = 95
    VAM = 96


@dataclass
class AutoActivityDetect:
    value: int


AutoActivityDetect.NONE = 0
AutoActivityDetect.RUNNING = 1
AutoActivityDetect.CYCLING = 2
AutoActivityDetect.SWIMMING = 4
AutoActivityDetect.WALKING = 8
AutoActivityDetect.ELLIPTICAL = 32
AutoActivityDetect.SEDENTARY = 1024


@dataclass
class SupportedExdScreenLayouts:
    value: int


SupportedExdScreenLayouts.FULL_SCREEN = 1
SupportedExdScreenLayouts.HALF_VERTICAL = 2
SupportedExdScreenLayouts.HALF_HORIZONTAL = 4
SupportedExdScreenLayouts.HALF_VERTICAL_RIGHT_SPLIT = 8
SupportedExdScreenLayouts.HALF_HORIZONTAL_BOTTOM_SPLIT = 16
SupportedExdScreenLayouts.FULL_QUARTER_SPLIT = 32
SupportedExdScreenLayouts.HALF_VERTICAL_LEFT_SPLIT = 64
SupportedExdScreenLayouts.HALF_HORIZONTAL_TOP_SPLIT = 128


@dataclass
class FitBaseType:
    value: int


FitBaseType.ENUM = 0
FitBaseType.SINT8 = 1
FitBaseType.UINT8 = 2
FitBaseType.SINT16 = 131
FitBaseType.UINT16 = 132
FitBaseType.SINT32 = 133
FitBaseType.UINT32 = 134
FitBaseType.STRING = 7
FitBaseType.FLOAT32 = 136
FitBaseType.FLOAT64 = 137
FitBaseType.UINT8Z = 10
FitBaseType.UINT16Z = 139
FitBaseType.UINT32Z = 140
FitBaseType.BYTE = 13
FitBaseType.SINT64 = 142
FitBaseType.UINT64 = 143
FitBaseType.UINT64Z = 144


class TurnType(Enum):
    ARRIVING_IDX = 0
    ARRIVING_LEFT_IDX = 1
    ARRIVING_RIGHT_IDX = 2
    ARRIVING_VIA_IDX = 3
    ARRIVING_VIA_LEFT_IDX = 4
    ARRIVING_VIA_RIGHT_IDX = 5
    BEAR_KEEP_LEFT_IDX = 6
    BEAR_KEEP_RIGHT_IDX = 7
    CONTINUE_IDX = 8
    EXIT_LEFT_IDX = 9
    EXIT_RIGHT_IDX = 10
    FERRY_IDX = 11
    ROUNDABOUT_45_IDX = 12
    ROUNDABOUT_90_IDX = 13
    ROUNDABOUT_135_IDX = 14
    ROUNDABOUT_180_IDX = 15
    ROUNDABOUT_225_IDX = 16
    ROUNDABOUT_270_IDX = 17
    ROUNDABOUT_315_IDX = 18
    ROUNDABOUT_360_IDX = 19
    ROUNDABOUT_NEG_45_IDX = 20
    ROUNDABOUT_NEG_90_IDX = 21
    ROUNDABOUT_NEG_135_IDX = 22
    ROUNDABOUT_NEG_180_IDX = 23
    ROUNDABOUT_NEG_225_IDX = 24
    ROUNDABOUT_NEG_270_IDX = 25
    ROUNDABOUT_NEG_315_IDX = 26
    ROUNDABOUT_NEG_360_IDX = 27
    ROUNDABOUT_GENERIC_IDX = 28
    ROUNDABOUT_NEG_GENERIC_IDX = 29
    SHARP_TURN_LEFT_IDX = 30
    SHARP_TURN_RIGHT_IDX = 31
    TURN_LEFT_IDX = 32
    TURN_RIGHT_IDX = 33
    UTURN_LEFT_IDX = 34
    UTURN_RIGHT_IDX = 35
    ICON_INV_IDX = 36
    ICON_IDX_CNT = 37


@dataclass
class BikeLightBeamAngleMode:
    value: int


BikeLightBeamAngleMode.MANUAL = 0
BikeLightBeamAngleMode.AUTO = 1


@dataclass
class FitBaseUnit:
    value: int


FitBaseUnit.OTHER = 0
FitBaseUnit.KILOGRAM = 1
FitBaseUnit.POUND = 2


@dataclass
class SetType:
    value: int


SetType.REST = 0
SetType.ACTIVE = 1


class MaxMetCategory(Enum):
    GENERIC = 0
    CYCLING = 1


@dataclass
class ExerciseCategory:
    value: int


ExerciseCategory.BENCH_PRESS = 0
ExerciseCategory.CALF_RAISE = 1
ExerciseCategory.CARDIO = 2
ExerciseCategory.CARRY = 3
ExerciseCategory.CHOP = 4
ExerciseCategory.CORE = 5
ExerciseCategory.CRUNCH = 6
ExerciseCategory.CURL = 7
ExerciseCategory.DEADLIFT = 8
ExerciseCategory.FLYE = 9
ExerciseCategory.HIP_RAISE = 10
ExerciseCategory.HIP_STABILITY = 11
ExerciseCategory.HIP_SWING = 12
ExerciseCategory.HYPEREXTENSION = 13
ExerciseCategory.LATERAL_RAISE = 14
ExerciseCategory.LEG_CURL = 15
ExerciseCategory.LEG_RAISE = 16
ExerciseCategory.LUNGE = 17
ExerciseCategory.OLYMPIC_LIFT = 18
ExerciseCategory.PLANK = 19
ExerciseCategory.PLYO = 20
ExerciseCategory.PULL_UP = 21
ExerciseCategory.PUSH_UP = 22
ExerciseCategory.ROW = 23
ExerciseCategory.SHOULDER_PRESS = 24
ExerciseCategory.SHOULDER_STABILITY = 25
ExerciseCategory.SHRUG = 26
ExerciseCategory.SIT_UP = 27
ExerciseCategory.SQUAT = 28
ExerciseCategory.TOTAL_BODY = 29
ExerciseCategory.TRICEPS_EXTENSION = 30
ExerciseCategory.WARM_UP = 31
ExerciseCategory.RUN = 32
ExerciseCategory.UNKNOWN = 65534


@dataclass
class BenchPressExerciseName:
    value: int


BenchPressExerciseName.ALTERNATING_DUMBBELL_CHEST_PRESS_ON_SWISS_BALL = 0
BenchPressExerciseName.BARBELL_BENCH_PRESS = 1
BenchPressExerciseName.BARBELL_BOARD_BENCH_PRESS = 2
BenchPressExerciseName.BARBELL_FLOOR_PRESS = 3
BenchPressExerciseName.CLOSE_GRIP_BARBELL_BENCH_PRESS = 4
BenchPressExerciseName.DECLINE_DUMBBELL_BENCH_PRESS = 5
BenchPressExerciseName.DUMBBELL_BENCH_PRESS = 6
BenchPressExerciseName.DUMBBELL_FLOOR_PRESS = 7
BenchPressExerciseName.INCLINE_BARBELL_BENCH_PRESS = 8
BenchPressExerciseName.INCLINE_DUMBBELL_BENCH_PRESS = 9
BenchPressExerciseName.INCLINE_SMITH_MACHINE_BENCH_PRESS = 10
BenchPressExerciseName.ISOMETRIC_BARBELL_BENCH_PRESS = 11
BenchPressExerciseName.KETTLEBELL_CHEST_PRESS = 12
BenchPressExerciseName.NEUTRAL_GRIP_DUMBBELL_BENCH_PRESS = 13
BenchPressExerciseName.NEUTRAL_GRIP_DUMBBELL_INCLINE_BENCH_PRESS = 14
BenchPressExerciseName.ONE_ARM_FLOOR_PRESS = 15
BenchPressExerciseName.WEIGHTED_ONE_ARM_FLOOR_PRESS = 16
BenchPressExerciseName.PARTIAL_LOCKOUT = 17
BenchPressExerciseName.REVERSE_GRIP_BARBELL_BENCH_PRESS = 18
BenchPressExerciseName.REVERSE_GRIP_INCLINE_BENCH_PRESS = 19
BenchPressExerciseName.SINGLE_ARM_CABLE_CHEST_PRESS = 20
BenchPressExerciseName.SINGLE_ARM_DUMBBELL_BENCH_PRESS = 21
BenchPressExerciseName.SMITH_MACHINE_BENCH_PRESS = 22
BenchPressExerciseName.SWISS_BALL_DUMBBELL_CHEST_PRESS = 23
BenchPressExerciseName.TRIPLE_STOP_BARBELL_BENCH_PRESS = 24
BenchPressExerciseName.WIDE_GRIP_BARBELL_BENCH_PRESS = 25
BenchPressExerciseName.ALTERNATING_DUMBBELL_CHEST_PRESS = 26


@dataclass
class CalfRaiseExerciseName:
    value: int


CalfRaiseExerciseName._3_WAY_CALF_RAISE = 0
CalfRaiseExerciseName._3_WAY_WEIGHTED_CALF_RAISE = 1
CalfRaiseExerciseName._3_WAY_SINGLE_LEG_CALF_RAISE = 2
CalfRaiseExerciseName._3_WAY_WEIGHTED_SINGLE_LEG_CALF_RAISE = 3
CalfRaiseExerciseName.DONKEY_CALF_RAISE = 4
CalfRaiseExerciseName.WEIGHTED_DONKEY_CALF_RAISE = 5
CalfRaiseExerciseName.SEATED_CALF_RAISE = 6
CalfRaiseExerciseName.WEIGHTED_SEATED_CALF_RAISE = 7
CalfRaiseExerciseName.SEATED_DUMBBELL_TOE_RAISE = 8
CalfRaiseExerciseName.SINGLE_LEG_BENT_KNEE_CALF_RAISE = 9
CalfRaiseExerciseName.WEIGHTED_SINGLE_LEG_BENT_KNEE_CALF_RAISE = 10
CalfRaiseExerciseName.SINGLE_LEG_DECLINE_PUSH_UP = 11
CalfRaiseExerciseName.SINGLE_LEG_DONKEY_CALF_RAISE = 12
CalfRaiseExerciseName.WEIGHTED_SINGLE_LEG_DONKEY_CALF_RAISE = 13
CalfRaiseExerciseName.SINGLE_LEG_HIP_RAISE_WITH_KNEE_HOLD = 14
CalfRaiseExerciseName.SINGLE_LEG_STANDING_CALF_RAISE = 15
CalfRaiseExerciseName.SINGLE_LEG_STANDING_DUMBBELL_CALF_RAISE = 16
CalfRaiseExerciseName.STANDING_BARBELL_CALF_RAISE = 17
CalfRaiseExerciseName.STANDING_CALF_RAISE = 18
CalfRaiseExerciseName.WEIGHTED_STANDING_CALF_RAISE = 19
CalfRaiseExerciseName.STANDING_DUMBBELL_CALF_RAISE = 20


@dataclass
class CardioExerciseName:
    value: int


CardioExerciseName.BOB_AND_WEAVE_CIRCLE = 0
CardioExerciseName.WEIGHTED_BOB_AND_WEAVE_CIRCLE = 1
CardioExerciseName.CARDIO_CORE_CRAWL = 2
CardioExerciseName.WEIGHTED_CARDIO_CORE_CRAWL = 3
CardioExerciseName.DOUBLE_UNDER = 4
CardioExerciseName.WEIGHTED_DOUBLE_UNDER = 5
CardioExerciseName.JUMP_ROPE = 6
CardioExerciseName.WEIGHTED_JUMP_ROPE = 7
CardioExerciseName.JUMP_ROPE_CROSSOVER = 8
CardioExerciseName.WEIGHTED_JUMP_ROPE_CROSSOVER = 9
CardioExerciseName.JUMP_ROPE_JOG = 10
CardioExerciseName.WEIGHTED_JUMP_ROPE_JOG = 11
CardioExerciseName.JUMPING_JACKS = 12
CardioExerciseName.WEIGHTED_JUMPING_JACKS = 13
CardioExerciseName.SKI_MOGULS = 14
CardioExerciseName.WEIGHTED_SKI_MOGULS = 15
CardioExerciseName.SPLIT_JACKS = 16
CardioExerciseName.WEIGHTED_SPLIT_JACKS = 17
CardioExerciseName.SQUAT_JACKS = 18
CardioExerciseName.WEIGHTED_SQUAT_JACKS = 19
CardioExerciseName.TRIPLE_UNDER = 20
CardioExerciseName.WEIGHTED_TRIPLE_UNDER = 21


@dataclass
class CarryExerciseName:
    value: int


CarryExerciseName.BAR_HOLDS = 0
CarryExerciseName.FARMERS_WALK = 1
CarryExerciseName.FARMERS_WALK_ON_TOES = 2
CarryExerciseName.HEX_DUMBBELL_HOLD = 3
CarryExerciseName.OVERHEAD_CARRY = 4


@dataclass
class ChopExerciseName:
    value: int


ChopExerciseName.CABLE_PULL_THROUGH = 0
ChopExerciseName.CABLE_ROTATIONAL_LIFT = 1
ChopExerciseName.CABLE_WOODCHOP = 2
ChopExerciseName.CROSS_CHOP_TO_KNEE = 3
ChopExerciseName.WEIGHTED_CROSS_CHOP_TO_KNEE = 4
ChopExerciseName.DUMBBELL_CHOP = 5
ChopExerciseName.HALF_KNEELING_ROTATION = 6
ChopExerciseName.WEIGHTED_HALF_KNEELING_ROTATION = 7
ChopExerciseName.HALF_KNEELING_ROTATIONAL_CHOP = 8
ChopExerciseName.HALF_KNEELING_ROTATIONAL_REVERSE_CHOP = 9
ChopExerciseName.HALF_KNEELING_STABILITY_CHOP = 10
ChopExerciseName.HALF_KNEELING_STABILITY_REVERSE_CHOP = 11
ChopExerciseName.KNEELING_ROTATIONAL_CHOP = 12
ChopExerciseName.KNEELING_ROTATIONAL_REVERSE_CHOP = 13
ChopExerciseName.KNEELING_STABILITY_CHOP = 14
ChopExerciseName.KNEELING_WOODCHOPPER = 15
ChopExerciseName.MEDICINE_BALL_WOOD_CHOPS = 16
ChopExerciseName.POWER_SQUAT_CHOPS = 17
ChopExerciseName.WEIGHTED_POWER_SQUAT_CHOPS = 18
ChopExerciseName.STANDING_ROTATIONAL_CHOP = 19
ChopExerciseName.STANDING_SPLIT_ROTATIONAL_CHOP = 20
ChopExerciseName.STANDING_SPLIT_ROTATIONAL_REVERSE_CHOP = 21
ChopExerciseName.STANDING_STABILITY_REVERSE_CHOP = 22


@dataclass
class CoreExerciseName:
    value: int


CoreExerciseName.ABS_JABS = 0
CoreExerciseName.WEIGHTED_ABS_JABS = 1
CoreExerciseName.ALTERNATING_PLATE_REACH = 2
CoreExerciseName.BARBELL_ROLLOUT = 3
CoreExerciseName.WEIGHTED_BARBELL_ROLLOUT = 4
CoreExerciseName.BODY_BAR_OBLIQUE_TWIST = 5
CoreExerciseName.CABLE_CORE_PRESS = 6
CoreExerciseName.CABLE_SIDE_BEND = 7
CoreExerciseName.SIDE_BEND = 8
CoreExerciseName.WEIGHTED_SIDE_BEND = 9
CoreExerciseName.CRESCENT_CIRCLE = 10
CoreExerciseName.WEIGHTED_CRESCENT_CIRCLE = 11
CoreExerciseName.CYCLING_RUSSIAN_TWIST = 12
CoreExerciseName.WEIGHTED_CYCLING_RUSSIAN_TWIST = 13
CoreExerciseName.ELEVATED_FEET_RUSSIAN_TWIST = 14
CoreExerciseName.WEIGHTED_ELEVATED_FEET_RUSSIAN_TWIST = 15
CoreExerciseName.HALF_TURKISH_GET_UP = 16
CoreExerciseName.KETTLEBELL_WINDMILL = 17
CoreExerciseName.KNEELING_AB_WHEEL = 18
CoreExerciseName.WEIGHTED_KNEELING_AB_WHEEL = 19
CoreExerciseName.MODIFIED_FRONT_LEVER = 20
CoreExerciseName.OPEN_KNEE_TUCKS = 21
CoreExerciseName.WEIGHTED_OPEN_KNEE_TUCKS = 22
CoreExerciseName.SIDE_ABS_LEG_LIFT = 23
CoreExerciseName.WEIGHTED_SIDE_ABS_LEG_LIFT = 24
CoreExerciseName.SWISS_BALL_JACKKNIFE = 25
CoreExerciseName.WEIGHTED_SWISS_BALL_JACKKNIFE = 26
CoreExerciseName.SWISS_BALL_PIKE = 27
CoreExerciseName.WEIGHTED_SWISS_BALL_PIKE = 28
CoreExerciseName.SWISS_BALL_ROLLOUT = 29
CoreExerciseName.WEIGHTED_SWISS_BALL_ROLLOUT = 30
CoreExerciseName.TRIANGLE_HIP_PRESS = 31
CoreExerciseName.WEIGHTED_TRIANGLE_HIP_PRESS = 32
CoreExerciseName.TRX_SUSPENDED_JACKKNIFE = 33
CoreExerciseName.WEIGHTED_TRX_SUSPENDED_JACKKNIFE = 34
CoreExerciseName.U_BOAT = 35
CoreExerciseName.WEIGHTED_U_BOAT = 36
CoreExerciseName.WINDMILL_SWITCHES = 37
CoreExerciseName.WEIGHTED_WINDMILL_SWITCHES = 38
CoreExerciseName.ALTERNATING_SLIDE_OUT = 39
CoreExerciseName.WEIGHTED_ALTERNATING_SLIDE_OUT = 40
CoreExerciseName.GHD_BACK_EXTENSIONS = 41
CoreExerciseName.WEIGHTED_GHD_BACK_EXTENSIONS = 42
CoreExerciseName.OVERHEAD_WALK = 43
CoreExerciseName.INCHWORM = 44
CoreExerciseName.WEIGHTED_MODIFIED_FRONT_LEVER = 45
CoreExerciseName.RUSSIAN_TWIST = 46
CoreExerciseName.ABDOMINAL_LEG_ROTATIONS = 47  # Deprecated do not use
CoreExerciseName.ARM_AND_LEG_EXTENSION_ON_KNEES = 48
CoreExerciseName.BICYCLE = 49
CoreExerciseName.BICEP_CURL_WITH_LEG_EXTENSION = 50
CoreExerciseName.CAT_COW = 51
CoreExerciseName.CORKSCREW = 52
CoreExerciseName.CRISS_CROSS = 53
CoreExerciseName.CRISS_CROSS_WITH_BALL = 54  # Deprecated do not use
CoreExerciseName.DOUBLE_LEG_STRETCH = 55
CoreExerciseName.KNEE_FOLDS = 56
CoreExerciseName.LOWER_LIFT = 57
CoreExerciseName.NECK_PULL = 58
CoreExerciseName.PELVIC_CLOCKS = 59
CoreExerciseName.ROLL_OVER = 60
CoreExerciseName.ROLL_UP = 61
CoreExerciseName.ROLLING = 62
CoreExerciseName.ROWING_1 = 63
CoreExerciseName.ROWING_2 = 64
CoreExerciseName.SCISSORS = 65
CoreExerciseName.SINGLE_LEG_CIRCLES = 66
CoreExerciseName.SINGLE_LEG_STRETCH = 67
CoreExerciseName.SNAKE_TWIST_1_AND_2 = 68  # Deprecated do not use
CoreExerciseName.SWAN = 69
CoreExerciseName.SWIMMING = 70
CoreExerciseName.TEASER = 71
CoreExerciseName.THE_HUNDRED = 72


@dataclass
class CrunchExerciseName:
    value: int


CrunchExerciseName.BICYCLE_CRUNCH = 0
CrunchExerciseName.CABLE_CRUNCH = 1
CrunchExerciseName.CIRCULAR_ARM_CRUNCH = 2
CrunchExerciseName.CROSSED_ARMS_CRUNCH = 3
CrunchExerciseName.WEIGHTED_CROSSED_ARMS_CRUNCH = 4
CrunchExerciseName.CROSS_LEG_REVERSE_CRUNCH = 5
CrunchExerciseName.WEIGHTED_CROSS_LEG_REVERSE_CRUNCH = 6
CrunchExerciseName.CRUNCH_CHOP = 7
CrunchExerciseName.WEIGHTED_CRUNCH_CHOP = 8
CrunchExerciseName.DOUBLE_CRUNCH = 9
CrunchExerciseName.WEIGHTED_DOUBLE_CRUNCH = 10
CrunchExerciseName.ELBOW_TO_KNEE_CRUNCH = 11
CrunchExerciseName.WEIGHTED_ELBOW_TO_KNEE_CRUNCH = 12
CrunchExerciseName.FLUTTER_KICKS = 13
CrunchExerciseName.WEIGHTED_FLUTTER_KICKS = 14
CrunchExerciseName.FOAM_ROLLER_REVERSE_CRUNCH_ON_BENCH = 15
CrunchExerciseName.WEIGHTED_FOAM_ROLLER_REVERSE_CRUNCH_ON_BENCH = 16
CrunchExerciseName.FOAM_ROLLER_REVERSE_CRUNCH_WITH_DUMBBELL = 17
CrunchExerciseName.FOAM_ROLLER_REVERSE_CRUNCH_WITH_MEDICINE_BALL = 18
CrunchExerciseName.FROG_PRESS = 19
CrunchExerciseName.HANGING_KNEE_RAISE_OBLIQUE_CRUNCH = 20
CrunchExerciseName.WEIGHTED_HANGING_KNEE_RAISE_OBLIQUE_CRUNCH = 21
CrunchExerciseName.HIP_CROSSOVER = 22
CrunchExerciseName.WEIGHTED_HIP_CROSSOVER = 23
CrunchExerciseName.HOLLOW_ROCK = 24
CrunchExerciseName.WEIGHTED_HOLLOW_ROCK = 25
CrunchExerciseName.INCLINE_REVERSE_CRUNCH = 26
CrunchExerciseName.WEIGHTED_INCLINE_REVERSE_CRUNCH = 27
CrunchExerciseName.KNEELING_CABLE_CRUNCH = 28
CrunchExerciseName.KNEELING_CROSS_CRUNCH = 29
CrunchExerciseName.WEIGHTED_KNEELING_CROSS_CRUNCH = 30
CrunchExerciseName.KNEELING_OBLIQUE_CABLE_CRUNCH = 31
CrunchExerciseName.KNEES_TO_ELBOW = 32
CrunchExerciseName.LEG_EXTENSIONS = 33
CrunchExerciseName.WEIGHTED_LEG_EXTENSIONS = 34
CrunchExerciseName.LEG_LEVERS = 35
CrunchExerciseName.MCGILL_CURL_UP = 36
CrunchExerciseName.WEIGHTED_MCGILL_CURL_UP = 37
CrunchExerciseName.MODIFIED_PILATES_ROLL_UP_WITH_BALL = 38
CrunchExerciseName.WEIGHTED_MODIFIED_PILATES_ROLL_UP_WITH_BALL = 39
CrunchExerciseName.PILATES_CRUNCH = 40
CrunchExerciseName.WEIGHTED_PILATES_CRUNCH = 41
CrunchExerciseName.PILATES_ROLL_UP_WITH_BALL = 42
CrunchExerciseName.WEIGHTED_PILATES_ROLL_UP_WITH_BALL = 43
CrunchExerciseName.RAISED_LEGS_CRUNCH = 44
CrunchExerciseName.WEIGHTED_RAISED_LEGS_CRUNCH = 45
CrunchExerciseName.REVERSE_CRUNCH = 46
CrunchExerciseName.WEIGHTED_REVERSE_CRUNCH = 47
CrunchExerciseName.REVERSE_CRUNCH_ON_A_BENCH = 48
CrunchExerciseName.WEIGHTED_REVERSE_CRUNCH_ON_A_BENCH = 49
CrunchExerciseName.REVERSE_CURL_AND_LIFT = 50
CrunchExerciseName.WEIGHTED_REVERSE_CURL_AND_LIFT = 51
CrunchExerciseName.ROTATIONAL_LIFT = 52
CrunchExerciseName.WEIGHTED_ROTATIONAL_LIFT = 53
CrunchExerciseName.SEATED_ALTERNATING_REVERSE_CRUNCH = 54
CrunchExerciseName.WEIGHTED_SEATED_ALTERNATING_REVERSE_CRUNCH = 55
CrunchExerciseName.SEATED_LEG_U = 56
CrunchExerciseName.WEIGHTED_SEATED_LEG_U = 57
CrunchExerciseName.SIDE_TO_SIDE_CRUNCH_AND_WEAVE = 58
CrunchExerciseName.WEIGHTED_SIDE_TO_SIDE_CRUNCH_AND_WEAVE = 59
CrunchExerciseName.SINGLE_LEG_REVERSE_CRUNCH = 60
CrunchExerciseName.WEIGHTED_SINGLE_LEG_REVERSE_CRUNCH = 61
CrunchExerciseName.SKATER_CRUNCH_CROSS = 62
CrunchExerciseName.WEIGHTED_SKATER_CRUNCH_CROSS = 63
CrunchExerciseName.STANDING_CABLE_CRUNCH = 64
CrunchExerciseName.STANDING_SIDE_CRUNCH = 65
CrunchExerciseName.STEP_CLIMB = 66
CrunchExerciseName.WEIGHTED_STEP_CLIMB = 67
CrunchExerciseName.SWISS_BALL_CRUNCH = 68
CrunchExerciseName.SWISS_BALL_REVERSE_CRUNCH = 69
CrunchExerciseName.WEIGHTED_SWISS_BALL_REVERSE_CRUNCH = 70
CrunchExerciseName.SWISS_BALL_RUSSIAN_TWIST = 71
CrunchExerciseName.WEIGHTED_SWISS_BALL_RUSSIAN_TWIST = 72
CrunchExerciseName.SWISS_BALL_SIDE_CRUNCH = 73
CrunchExerciseName.WEIGHTED_SWISS_BALL_SIDE_CRUNCH = 74
CrunchExerciseName.THORACIC_CRUNCHES_ON_FOAM_ROLLER = 75
CrunchExerciseName.WEIGHTED_THORACIC_CRUNCHES_ON_FOAM_ROLLER = 76
CrunchExerciseName.TRICEPS_CRUNCH = 77
CrunchExerciseName.WEIGHTED_BICYCLE_CRUNCH = 78
CrunchExerciseName.WEIGHTED_CRUNCH = 79
CrunchExerciseName.WEIGHTED_SWISS_BALL_CRUNCH = 80
CrunchExerciseName.TOES_TO_BAR = 81
CrunchExerciseName.WEIGHTED_TOES_TO_BAR = 82
CrunchExerciseName.CRUNCH = 83
CrunchExerciseName.STRAIGHT_LEG_CRUNCH_WITH_BALL = 84


@dataclass
class CurlExerciseName:
    value: int


CurlExerciseName.ALTERNATING_DUMBBELL_BICEPS_CURL = 0
CurlExerciseName.ALTERNATING_DUMBBELL_BICEPS_CURL_ON_SWISS_BALL = 1
CurlExerciseName.ALTERNATING_INCLINE_DUMBBELL_BICEPS_CURL = 2
CurlExerciseName.BARBELL_BICEPS_CURL = 3
CurlExerciseName.BARBELL_REVERSE_WRIST_CURL = 4
CurlExerciseName.BARBELL_WRIST_CURL = 5
CurlExerciseName.BEHIND_THE_BACK_BARBELL_REVERSE_WRIST_CURL = 6
CurlExerciseName.BEHIND_THE_BACK_ONE_ARM_CABLE_CURL = 7
CurlExerciseName.CABLE_BICEPS_CURL = 8
CurlExerciseName.CABLE_HAMMER_CURL = 9
CurlExerciseName.CHEATING_BARBELL_BICEPS_CURL = 10
CurlExerciseName.CLOSE_GRIP_EZ_BAR_BICEPS_CURL = 11
CurlExerciseName.CROSS_BODY_DUMBBELL_HAMMER_CURL = 12
CurlExerciseName.DEAD_HANG_BICEPS_CURL = 13
CurlExerciseName.DECLINE_HAMMER_CURL = 14
CurlExerciseName.DUMBBELL_BICEPS_CURL_WITH_STATIC_HOLD = 15
CurlExerciseName.DUMBBELL_HAMMER_CURL = 16
CurlExerciseName.DUMBBELL_REVERSE_WRIST_CURL = 17
CurlExerciseName.DUMBBELL_WRIST_CURL = 18
CurlExerciseName.EZ_BAR_PREACHER_CURL = 19
CurlExerciseName.FORWARD_BEND_BICEPS_CURL = 20
CurlExerciseName.HAMMER_CURL_TO_PRESS = 21
CurlExerciseName.INCLINE_DUMBBELL_BICEPS_CURL = 22
CurlExerciseName.INCLINE_OFFSET_THUMB_DUMBBELL_CURL = 23
CurlExerciseName.KETTLEBELL_BICEPS_CURL = 24
CurlExerciseName.LYING_CONCENTRATION_CABLE_CURL = 25
CurlExerciseName.ONE_ARM_PREACHER_CURL = 26
CurlExerciseName.PLATE_PINCH_CURL = 27
CurlExerciseName.PREACHER_CURL_WITH_CABLE = 28
CurlExerciseName.REVERSE_EZ_BAR_CURL = 29
CurlExerciseName.REVERSE_GRIP_WRIST_CURL = 30
CurlExerciseName.REVERSE_GRIP_BARBELL_BICEPS_CURL = 31
CurlExerciseName.SEATED_ALTERNATING_DUMBBELL_BICEPS_CURL = 32
CurlExerciseName.SEATED_DUMBBELL_BICEPS_CURL = 33
CurlExerciseName.SEATED_REVERSE_DUMBBELL_CURL = 34
CurlExerciseName.SPLIT_STANCE_OFFSET_PINKY_DUMBBELL_CURL = 35
CurlExerciseName.STANDING_ALTERNATING_DUMBBELL_CURLS = 36
CurlExerciseName.STANDING_DUMBBELL_BICEPS_CURL = 37
CurlExerciseName.STANDING_EZ_BAR_BICEPS_CURL = 38
CurlExerciseName.STATIC_CURL = 39
CurlExerciseName.SWISS_BALL_DUMBBELL_OVERHEAD_TRICEPS_EXTENSION = 40
CurlExerciseName.SWISS_BALL_EZ_BAR_PREACHER_CURL = 41
CurlExerciseName.TWISTING_STANDING_DUMBBELL_BICEPS_CURL = 42
CurlExerciseName.WIDE_GRIP_EZ_BAR_BICEPS_CURL = 43


@dataclass
class DeadliftExerciseName:
    value: int


DeadliftExerciseName.BARBELL_DEADLIFT = 0
DeadliftExerciseName.BARBELL_STRAIGHT_LEG_DEADLIFT = 1
DeadliftExerciseName.DUMBBELL_DEADLIFT = 2
DeadliftExerciseName.DUMBBELL_SINGLE_LEG_DEADLIFT_TO_ROW = 3
DeadliftExerciseName.DUMBBELL_STRAIGHT_LEG_DEADLIFT = 4
DeadliftExerciseName.KETTLEBELL_FLOOR_TO_SHELF = 5
DeadliftExerciseName.ONE_ARM_ONE_LEG_DEADLIFT = 6
DeadliftExerciseName.RACK_PULL = 7
DeadliftExerciseName.ROTATIONAL_DUMBBELL_STRAIGHT_LEG_DEADLIFT = 8
DeadliftExerciseName.SINGLE_ARM_DEADLIFT = 9
DeadliftExerciseName.SINGLE_LEG_BARBELL_DEADLIFT = 10
DeadliftExerciseName.SINGLE_LEG_BARBELL_STRAIGHT_LEG_DEADLIFT = 11
DeadliftExerciseName.SINGLE_LEG_DEADLIFT_WITH_BARBELL = 12
DeadliftExerciseName.SINGLE_LEG_RDL_CIRCUIT = 13
DeadliftExerciseName.SINGLE_LEG_ROMANIAN_DEADLIFT_WITH_DUMBBELL = 14
DeadliftExerciseName.SUMO_DEADLIFT = 15
DeadliftExerciseName.SUMO_DEADLIFT_HIGH_PULL = 16
DeadliftExerciseName.TRAP_BAR_DEADLIFT = 17
DeadliftExerciseName.WIDE_GRIP_BARBELL_DEADLIFT = 18


@dataclass
class FlyeExerciseName:
    value: int


FlyeExerciseName.CABLE_CROSSOVER = 0
FlyeExerciseName.DECLINE_DUMBBELL_FLYE = 1
FlyeExerciseName.DUMBBELL_FLYE = 2
FlyeExerciseName.INCLINE_DUMBBELL_FLYE = 3
FlyeExerciseName.KETTLEBELL_FLYE = 4
FlyeExerciseName.KNEELING_REAR_FLYE = 5
FlyeExerciseName.SINGLE_ARM_STANDING_CABLE_REVERSE_FLYE = 6
FlyeExerciseName.SWISS_BALL_DUMBBELL_FLYE = 7
FlyeExerciseName.ARM_ROTATIONS = 8
FlyeExerciseName.HUG_A_TREE = 9


@dataclass
class HipRaiseExerciseName:
    value: int


HipRaiseExerciseName.BARBELL_HIP_THRUST_ON_FLOOR = 0
HipRaiseExerciseName.BARBELL_HIP_THRUST_WITH_BENCH = 1
HipRaiseExerciseName.BENT_KNEE_SWISS_BALL_REVERSE_HIP_RAISE = 2
HipRaiseExerciseName.WEIGHTED_BENT_KNEE_SWISS_BALL_REVERSE_HIP_RAISE = 3
HipRaiseExerciseName.BRIDGE_WITH_LEG_EXTENSION = 4
HipRaiseExerciseName.WEIGHTED_BRIDGE_WITH_LEG_EXTENSION = 5
HipRaiseExerciseName.CLAM_BRIDGE = 6
HipRaiseExerciseName.FRONT_KICK_TABLETOP = 7
HipRaiseExerciseName.WEIGHTED_FRONT_KICK_TABLETOP = 8
HipRaiseExerciseName.HIP_EXTENSION_AND_CROSS = 9
HipRaiseExerciseName.WEIGHTED_HIP_EXTENSION_AND_CROSS = 10
HipRaiseExerciseName.HIP_RAISE = 11
HipRaiseExerciseName.WEIGHTED_HIP_RAISE = 12
HipRaiseExerciseName.HIP_RAISE_WITH_FEET_ON_SWISS_BALL = 13
HipRaiseExerciseName.WEIGHTED_HIP_RAISE_WITH_FEET_ON_SWISS_BALL = 14
HipRaiseExerciseName.HIP_RAISE_WITH_HEAD_ON_BOSU_BALL = 15
HipRaiseExerciseName.WEIGHTED_HIP_RAISE_WITH_HEAD_ON_BOSU_BALL = 16
HipRaiseExerciseName.HIP_RAISE_WITH_HEAD_ON_SWISS_BALL = 17
HipRaiseExerciseName.WEIGHTED_HIP_RAISE_WITH_HEAD_ON_SWISS_BALL = 18
HipRaiseExerciseName.HIP_RAISE_WITH_KNEE_SQUEEZE = 19
HipRaiseExerciseName.WEIGHTED_HIP_RAISE_WITH_KNEE_SQUEEZE = 20
HipRaiseExerciseName.INCLINE_REAR_LEG_EXTENSION = 21
HipRaiseExerciseName.WEIGHTED_INCLINE_REAR_LEG_EXTENSION = 22
HipRaiseExerciseName.KETTLEBELL_SWING = 23
HipRaiseExerciseName.MARCHING_HIP_RAISE = 24
HipRaiseExerciseName.WEIGHTED_MARCHING_HIP_RAISE = 25
HipRaiseExerciseName.MARCHING_HIP_RAISE_WITH_FEET_ON_A_SWISS_BALL = 26
HipRaiseExerciseName.WEIGHTED_MARCHING_HIP_RAISE_WITH_FEET_ON_A_SWISS_BALL = 27
HipRaiseExerciseName.REVERSE_HIP_RAISE = 28
HipRaiseExerciseName.WEIGHTED_REVERSE_HIP_RAISE = 29
HipRaiseExerciseName.SINGLE_LEG_HIP_RAISE = 30
HipRaiseExerciseName.WEIGHTED_SINGLE_LEG_HIP_RAISE = 31
HipRaiseExerciseName.SINGLE_LEG_HIP_RAISE_WITH_FOOT_ON_BENCH = 32
HipRaiseExerciseName.WEIGHTED_SINGLE_LEG_HIP_RAISE_WITH_FOOT_ON_BENCH = 33
HipRaiseExerciseName.SINGLE_LEG_HIP_RAISE_WITH_FOOT_ON_BOSU_BALL = 34
HipRaiseExerciseName.WEIGHTED_SINGLE_LEG_HIP_RAISE_WITH_FOOT_ON_BOSU_BALL = 35
HipRaiseExerciseName.SINGLE_LEG_HIP_RAISE_WITH_FOOT_ON_FOAM_ROLLER = 36
HipRaiseExerciseName.WEIGHTED_SINGLE_LEG_HIP_RAISE_WITH_FOOT_ON_FOAM_ROLLER = 37
HipRaiseExerciseName.SINGLE_LEG_HIP_RAISE_WITH_FOOT_ON_MEDICINE_BALL = 38
HipRaiseExerciseName.WEIGHTED_SINGLE_LEG_HIP_RAISE_WITH_FOOT_ON_MEDICINE_BALL = 39
HipRaiseExerciseName.SINGLE_LEG_HIP_RAISE_WITH_HEAD_ON_BOSU_BALL = 40
HipRaiseExerciseName.WEIGHTED_SINGLE_LEG_HIP_RAISE_WITH_HEAD_ON_BOSU_BALL = 41
HipRaiseExerciseName.WEIGHTED_CLAM_BRIDGE = 42
HipRaiseExerciseName.SINGLE_LEG_SWISS_BALL_HIP_RAISE_AND_LEG_CURL = 43
HipRaiseExerciseName.CLAMS = 44
HipRaiseExerciseName.INNER_THIGH_CIRCLES = 45  # Deprecated do not use
HipRaiseExerciseName.INNER_THIGH_SIDE_LIFT = 46  # Deprecated do not use
HipRaiseExerciseName.LEG_CIRCLES = 47
HipRaiseExerciseName.LEG_LIFT = 48
HipRaiseExerciseName.LEG_LIFT_IN_EXTERNAL_ROTATION = 49


@dataclass
class HipStabilityExerciseName:
    value: int


HipStabilityExerciseName.BAND_SIDE_LYING_LEG_RAISE = 0
HipStabilityExerciseName.DEAD_BUG = 1
HipStabilityExerciseName.WEIGHTED_DEAD_BUG = 2
HipStabilityExerciseName.EXTERNAL_HIP_RAISE = 3
HipStabilityExerciseName.WEIGHTED_EXTERNAL_HIP_RAISE = 4
HipStabilityExerciseName.FIRE_HYDRANT_KICKS = 5
HipStabilityExerciseName.WEIGHTED_FIRE_HYDRANT_KICKS = 6
HipStabilityExerciseName.HIP_CIRCLES = 7
HipStabilityExerciseName.WEIGHTED_HIP_CIRCLES = 8
HipStabilityExerciseName.INNER_THIGH_LIFT = 9
HipStabilityExerciseName.WEIGHTED_INNER_THIGH_LIFT = 10
HipStabilityExerciseName.LATERAL_WALKS_WITH_BAND_AT_ANKLES = 11
HipStabilityExerciseName.PRETZEL_SIDE_KICK = 12
HipStabilityExerciseName.WEIGHTED_PRETZEL_SIDE_KICK = 13
HipStabilityExerciseName.PRONE_HIP_INTERNAL_ROTATION = 14
HipStabilityExerciseName.WEIGHTED_PRONE_HIP_INTERNAL_ROTATION = 15
HipStabilityExerciseName.QUADRUPED = 16
HipStabilityExerciseName.QUADRUPED_HIP_EXTENSION = 17
HipStabilityExerciseName.WEIGHTED_QUADRUPED_HIP_EXTENSION = 18
HipStabilityExerciseName.QUADRUPED_WITH_LEG_LIFT = 19
HipStabilityExerciseName.WEIGHTED_QUADRUPED_WITH_LEG_LIFT = 20
HipStabilityExerciseName.SIDE_LYING_LEG_RAISE = 21
HipStabilityExerciseName.WEIGHTED_SIDE_LYING_LEG_RAISE = 22
HipStabilityExerciseName.SLIDING_HIP_ADDUCTION = 23
HipStabilityExerciseName.WEIGHTED_SLIDING_HIP_ADDUCTION = 24
HipStabilityExerciseName.STANDING_ADDUCTION = 25
HipStabilityExerciseName.WEIGHTED_STANDING_ADDUCTION = 26
HipStabilityExerciseName.STANDING_CABLE_HIP_ABDUCTION = 27
HipStabilityExerciseName.STANDING_HIP_ABDUCTION = 28
HipStabilityExerciseName.WEIGHTED_STANDING_HIP_ABDUCTION = 29
HipStabilityExerciseName.STANDING_REAR_LEG_RAISE = 30
HipStabilityExerciseName.WEIGHTED_STANDING_REAR_LEG_RAISE = 31
HipStabilityExerciseName.SUPINE_HIP_INTERNAL_ROTATION = 32
HipStabilityExerciseName.WEIGHTED_SUPINE_HIP_INTERNAL_ROTATION = 33


@dataclass
class HipSwingExerciseName:
    value: int


HipSwingExerciseName.SINGLE_ARM_KETTLEBELL_SWING = 0
HipSwingExerciseName.SINGLE_ARM_DUMBBELL_SWING = 1
HipSwingExerciseName.STEP_OUT_SWING = 2


@dataclass
class HyperextensionExerciseName:
    value: int


HyperextensionExerciseName.BACK_EXTENSION_WITH_OPPOSITE_ARM_AND_LEG_REACH = 0
HyperextensionExerciseName.WEIGHTED_BACK_EXTENSION_WITH_OPPOSITE_ARM_AND_LEG_REACH = 1
HyperextensionExerciseName.BASE_ROTATIONS = 2
HyperextensionExerciseName.WEIGHTED_BASE_ROTATIONS = 3
HyperextensionExerciseName.BENT_KNEE_REVERSE_HYPEREXTENSION = 4
HyperextensionExerciseName.WEIGHTED_BENT_KNEE_REVERSE_HYPEREXTENSION = 5
HyperextensionExerciseName.HOLLOW_HOLD_AND_ROLL = 6
HyperextensionExerciseName.WEIGHTED_HOLLOW_HOLD_AND_ROLL = 7
HyperextensionExerciseName.KICKS = 8
HyperextensionExerciseName.WEIGHTED_KICKS = 9
HyperextensionExerciseName.KNEE_RAISES = 10
HyperextensionExerciseName.WEIGHTED_KNEE_RAISES = 11
HyperextensionExerciseName.KNEELING_SUPERMAN = 12
HyperextensionExerciseName.WEIGHTED_KNEELING_SUPERMAN = 13
HyperextensionExerciseName.LAT_PULL_DOWN_WITH_ROW = 14
HyperextensionExerciseName.MEDICINE_BALL_DEADLIFT_TO_REACH = 15
HyperextensionExerciseName.ONE_ARM_ONE_LEG_ROW = 16
HyperextensionExerciseName.ONE_ARM_ROW_WITH_BAND = 17
HyperextensionExerciseName.OVERHEAD_LUNGE_WITH_MEDICINE_BALL = 18
HyperextensionExerciseName.PLANK_KNEE_TUCKS = 19
HyperextensionExerciseName.WEIGHTED_PLANK_KNEE_TUCKS = 20
HyperextensionExerciseName.SIDE_STEP = 21
HyperextensionExerciseName.WEIGHTED_SIDE_STEP = 22
HyperextensionExerciseName.SINGLE_LEG_BACK_EXTENSION = 23
HyperextensionExerciseName.WEIGHTED_SINGLE_LEG_BACK_EXTENSION = 24
HyperextensionExerciseName.SPINE_EXTENSION = 25
HyperextensionExerciseName.WEIGHTED_SPINE_EXTENSION = 26
HyperextensionExerciseName.STATIC_BACK_EXTENSION = 27
HyperextensionExerciseName.WEIGHTED_STATIC_BACK_EXTENSION = 28
HyperextensionExerciseName.SUPERMAN_FROM_FLOOR = 29
HyperextensionExerciseName.WEIGHTED_SUPERMAN_FROM_FLOOR = 30
HyperextensionExerciseName.SWISS_BALL_BACK_EXTENSION = 31
HyperextensionExerciseName.WEIGHTED_SWISS_BALL_BACK_EXTENSION = 32
HyperextensionExerciseName.SWISS_BALL_HYPEREXTENSION = 33
HyperextensionExerciseName.WEIGHTED_SWISS_BALL_HYPEREXTENSION = 34
HyperextensionExerciseName.SWISS_BALL_OPPOSITE_ARM_AND_LEG_LIFT = 35
HyperextensionExerciseName.WEIGHTED_SWISS_BALL_OPPOSITE_ARM_AND_LEG_LIFT = 36
HyperextensionExerciseName.SUPERMAN_ON_SWISS_BALL = 37
HyperextensionExerciseName.COBRA = 38
HyperextensionExerciseName.SUPINE_FLOOR_BARRE = 39  # Deprecated do not use


@dataclass
class LateralRaiseExerciseName:
    value: int


LateralRaiseExerciseName._45_DEGREE_CABLE_EXTERNAL_ROTATION = 0
LateralRaiseExerciseName.ALTERNATING_LATERAL_RAISE_WITH_STATIC_HOLD = 1
LateralRaiseExerciseName.BAR_MUSCLE_UP = 2
LateralRaiseExerciseName.BENT_OVER_LATERAL_RAISE = 3
LateralRaiseExerciseName.CABLE_DIAGONAL_RAISE = 4
LateralRaiseExerciseName.CABLE_FRONT_RAISE = 5
LateralRaiseExerciseName.CALORIE_ROW = 6
LateralRaiseExerciseName.COMBO_SHOULDER_RAISE = 7
LateralRaiseExerciseName.DUMBBELL_DIAGONAL_RAISE = 8
LateralRaiseExerciseName.DUMBBELL_V_RAISE = 9
LateralRaiseExerciseName.FRONT_RAISE = 10
LateralRaiseExerciseName.LEANING_DUMBBELL_LATERAL_RAISE = 11
LateralRaiseExerciseName.LYING_DUMBBELL_RAISE = 12
LateralRaiseExerciseName.MUSCLE_UP = 13
LateralRaiseExerciseName.ONE_ARM_CABLE_LATERAL_RAISE = 14
LateralRaiseExerciseName.OVERHAND_GRIP_REAR_LATERAL_RAISE = 15
LateralRaiseExerciseName.PLATE_RAISES = 16
LateralRaiseExerciseName.RING_DIP = 17
LateralRaiseExerciseName.WEIGHTED_RING_DIP = 18
LateralRaiseExerciseName.RING_MUSCLE_UP = 19
LateralRaiseExerciseName.WEIGHTED_RING_MUSCLE_UP = 20
LateralRaiseExerciseName.ROPE_CLIMB = 21
LateralRaiseExerciseName.WEIGHTED_ROPE_CLIMB = 22
LateralRaiseExerciseName.SCAPTION = 23
LateralRaiseExerciseName.SEATED_LATERAL_RAISE = 24
LateralRaiseExerciseName.SEATED_REAR_LATERAL_RAISE = 25
LateralRaiseExerciseName.SIDE_LYING_LATERAL_RAISE = 26
LateralRaiseExerciseName.STANDING_LIFT = 27
LateralRaiseExerciseName.SUSPENDED_ROW = 28
LateralRaiseExerciseName.UNDERHAND_GRIP_REAR_LATERAL_RAISE = 29
LateralRaiseExerciseName.WALL_SLIDE = 30
LateralRaiseExerciseName.WEIGHTED_WALL_SLIDE = 31
LateralRaiseExerciseName.ARM_CIRCLES = 32
LateralRaiseExerciseName.SHAVING_THE_HEAD = 33


@dataclass
class LegCurlExerciseName:
    value: int


LegCurlExerciseName.LEG_CURL = 0
LegCurlExerciseName.WEIGHTED_LEG_CURL = 1
LegCurlExerciseName.GOOD_MORNING = 2
LegCurlExerciseName.SEATED_BARBELL_GOOD_MORNING = 3
LegCurlExerciseName.SINGLE_LEG_BARBELL_GOOD_MORNING = 4
LegCurlExerciseName.SINGLE_LEG_SLIDING_LEG_CURL = 5
LegCurlExerciseName.SLIDING_LEG_CURL = 6
LegCurlExerciseName.SPLIT_BARBELL_GOOD_MORNING = 7
LegCurlExerciseName.SPLIT_STANCE_EXTENSION = 8
LegCurlExerciseName.STAGGERED_STANCE_GOOD_MORNING = 9
LegCurlExerciseName.SWISS_BALL_HIP_RAISE_AND_LEG_CURL = 10
LegCurlExerciseName.ZERCHER_GOOD_MORNING = 11


@dataclass
class LegRaiseExerciseName:
    value: int


LegRaiseExerciseName.HANGING_KNEE_RAISE = 0
LegRaiseExerciseName.HANGING_LEG_RAISE = 1
LegRaiseExerciseName.WEIGHTED_HANGING_LEG_RAISE = 2
LegRaiseExerciseName.HANGING_SINGLE_LEG_RAISE = 3
LegRaiseExerciseName.WEIGHTED_HANGING_SINGLE_LEG_RAISE = 4
LegRaiseExerciseName.KETTLEBELL_LEG_RAISES = 5
LegRaiseExerciseName.LEG_LOWERING_DRILL = 6
LegRaiseExerciseName.WEIGHTED_LEG_LOWERING_DRILL = 7
LegRaiseExerciseName.LYING_STRAIGHT_LEG_RAISE = 8
LegRaiseExerciseName.WEIGHTED_LYING_STRAIGHT_LEG_RAISE = 9
LegRaiseExerciseName.MEDICINE_BALL_LEG_DROPS = 10
LegRaiseExerciseName.QUADRUPED_LEG_RAISE = 11
LegRaiseExerciseName.WEIGHTED_QUADRUPED_LEG_RAISE = 12
LegRaiseExerciseName.REVERSE_LEG_RAISE = 13
LegRaiseExerciseName.WEIGHTED_REVERSE_LEG_RAISE = 14
LegRaiseExerciseName.REVERSE_LEG_RAISE_ON_SWISS_BALL = 15
LegRaiseExerciseName.WEIGHTED_REVERSE_LEG_RAISE_ON_SWISS_BALL = 16
LegRaiseExerciseName.SINGLE_LEG_LOWERING_DRILL = 17
LegRaiseExerciseName.WEIGHTED_SINGLE_LEG_LOWERING_DRILL = 18
LegRaiseExerciseName.WEIGHTED_HANGING_KNEE_RAISE = 19
LegRaiseExerciseName.LATERAL_STEPOVER = 20
LegRaiseExerciseName.WEIGHTED_LATERAL_STEPOVER = 21


@dataclass
class LungeExerciseName:
    value: int


LungeExerciseName.OVERHEAD_LUNGE = 0
LungeExerciseName.LUNGE_MATRIX = 1
LungeExerciseName.WEIGHTED_LUNGE_MATRIX = 2
LungeExerciseName.ALTERNATING_BARBELL_FORWARD_LUNGE = 3
LungeExerciseName.ALTERNATING_DUMBBELL_LUNGE_WITH_REACH = 4
LungeExerciseName.BACK_FOOT_ELEVATED_DUMBBELL_SPLIT_SQUAT = 5
LungeExerciseName.BARBELL_BOX_LUNGE = 6
LungeExerciseName.BARBELL_BULGARIAN_SPLIT_SQUAT = 7
LungeExerciseName.BARBELL_CROSSOVER_LUNGE = 8
LungeExerciseName.BARBELL_FRONT_SPLIT_SQUAT = 9
LungeExerciseName.BARBELL_LUNGE = 10
LungeExerciseName.BARBELL_REVERSE_LUNGE = 11
LungeExerciseName.BARBELL_SIDE_LUNGE = 12
LungeExerciseName.BARBELL_SPLIT_SQUAT = 13
LungeExerciseName.CORE_CONTROL_REAR_LUNGE = 14
LungeExerciseName.DIAGONAL_LUNGE = 15
LungeExerciseName.DROP_LUNGE = 16
LungeExerciseName.DUMBBELL_BOX_LUNGE = 17
LungeExerciseName.DUMBBELL_BULGARIAN_SPLIT_SQUAT = 18
LungeExerciseName.DUMBBELL_CROSSOVER_LUNGE = 19
LungeExerciseName.DUMBBELL_DIAGONAL_LUNGE = 20
LungeExerciseName.DUMBBELL_LUNGE = 21
LungeExerciseName.DUMBBELL_LUNGE_AND_ROTATION = 22
LungeExerciseName.DUMBBELL_OVERHEAD_BULGARIAN_SPLIT_SQUAT = 23
LungeExerciseName.DUMBBELL_REVERSE_LUNGE_TO_HIGH_KNEE_AND_PRESS = 24
LungeExerciseName.DUMBBELL_SIDE_LUNGE = 25
LungeExerciseName.ELEVATED_FRONT_FOOT_BARBELL_SPLIT_SQUAT = 26
LungeExerciseName.FRONT_FOOT_ELEVATED_DUMBBELL_SPLIT_SQUAT = 27
LungeExerciseName.GUNSLINGER_LUNGE = 28
LungeExerciseName.LAWNMOWER_LUNGE = 29
LungeExerciseName.LOW_LUNGE_WITH_ISOMETRIC_ADDUCTION = 30
LungeExerciseName.LOW_SIDE_TO_SIDE_LUNGE = 31
LungeExerciseName.LUNGE = 32
LungeExerciseName.WEIGHTED_LUNGE = 33
LungeExerciseName.LUNGE_WITH_ARM_REACH = 34
LungeExerciseName.LUNGE_WITH_DIAGONAL_REACH = 35
LungeExerciseName.LUNGE_WITH_SIDE_BEND = 36
LungeExerciseName.OFFSET_DUMBBELL_LUNGE = 37
LungeExerciseName.OFFSET_DUMBBELL_REVERSE_LUNGE = 38
LungeExerciseName.OVERHEAD_BULGARIAN_SPLIT_SQUAT = 39
LungeExerciseName.OVERHEAD_DUMBBELL_REVERSE_LUNGE = 40
LungeExerciseName.OVERHEAD_DUMBBELL_SPLIT_SQUAT = 41
LungeExerciseName.OVERHEAD_LUNGE_WITH_ROTATION = 42
LungeExerciseName.REVERSE_BARBELL_BOX_LUNGE = 43
LungeExerciseName.REVERSE_BOX_LUNGE = 44
LungeExerciseName.REVERSE_DUMBBELL_BOX_LUNGE = 45
LungeExerciseName.REVERSE_DUMBBELL_CROSSOVER_LUNGE = 46
LungeExerciseName.REVERSE_DUMBBELL_DIAGONAL_LUNGE = 47
LungeExerciseName.REVERSE_LUNGE_WITH_REACH_BACK = 48
LungeExerciseName.WEIGHTED_REVERSE_LUNGE_WITH_REACH_BACK = 49
LungeExerciseName.REVERSE_LUNGE_WITH_TWIST_AND_OVERHEAD_REACH = 50
LungeExerciseName.WEIGHTED_REVERSE_LUNGE_WITH_TWIST_AND_OVERHEAD_REACH = 51
LungeExerciseName.REVERSE_SLIDING_BOX_LUNGE = 52
LungeExerciseName.WEIGHTED_REVERSE_SLIDING_BOX_LUNGE = 53
LungeExerciseName.REVERSE_SLIDING_LUNGE = 54
LungeExerciseName.WEIGHTED_REVERSE_SLIDING_LUNGE = 55
LungeExerciseName.RUNNERS_LUNGE_TO_BALANCE = 56
LungeExerciseName.WEIGHTED_RUNNERS_LUNGE_TO_BALANCE = 57
LungeExerciseName.SHIFTING_SIDE_LUNGE = 58
LungeExerciseName.SIDE_AND_CROSSOVER_LUNGE = 59
LungeExerciseName.WEIGHTED_SIDE_AND_CROSSOVER_LUNGE = 60
LungeExerciseName.SIDE_LUNGE = 61
LungeExerciseName.WEIGHTED_SIDE_LUNGE = 62
LungeExerciseName.SIDE_LUNGE_AND_PRESS = 63
LungeExerciseName.SIDE_LUNGE_JUMP_OFF = 64
LungeExerciseName.SIDE_LUNGE_SWEEP = 65
LungeExerciseName.WEIGHTED_SIDE_LUNGE_SWEEP = 66
LungeExerciseName.SIDE_LUNGE_TO_CROSSOVER_TAP = 67
LungeExerciseName.WEIGHTED_SIDE_LUNGE_TO_CROSSOVER_TAP = 68
LungeExerciseName.SIDE_TO_SIDE_LUNGE_CHOPS = 69
LungeExerciseName.WEIGHTED_SIDE_TO_SIDE_LUNGE_CHOPS = 70
LungeExerciseName.SIFF_JUMP_LUNGE = 71
LungeExerciseName.WEIGHTED_SIFF_JUMP_LUNGE = 72
LungeExerciseName.SINGLE_ARM_REVERSE_LUNGE_AND_PRESS = 73
LungeExerciseName.SLIDING_LATERAL_LUNGE = 74
LungeExerciseName.WEIGHTED_SLIDING_LATERAL_LUNGE = 75
LungeExerciseName.WALKING_BARBELL_LUNGE = 76
LungeExerciseName.WALKING_DUMBBELL_LUNGE = 77
LungeExerciseName.WALKING_LUNGE = 78
LungeExerciseName.WEIGHTED_WALKING_LUNGE = 79
LungeExerciseName.WIDE_GRIP_OVERHEAD_BARBELL_SPLIT_SQUAT = 80


@dataclass
class OlympicLiftExerciseName:
    value: int


OlympicLiftExerciseName.BARBELL_HANG_POWER_CLEAN = 0
OlympicLiftExerciseName.BARBELL_HANG_SQUAT_CLEAN = 1
OlympicLiftExerciseName.BARBELL_POWER_CLEAN = 2
OlympicLiftExerciseName.BARBELL_POWER_SNATCH = 3
OlympicLiftExerciseName.BARBELL_SQUAT_CLEAN = 4
OlympicLiftExerciseName.CLEAN_AND_JERK = 5
OlympicLiftExerciseName.BARBELL_HANG_POWER_SNATCH = 6
OlympicLiftExerciseName.BARBELL_HANG_PULL = 7
OlympicLiftExerciseName.BARBELL_HIGH_PULL = 8
OlympicLiftExerciseName.BARBELL_SNATCH = 9
OlympicLiftExerciseName.BARBELL_SPLIT_JERK = 10
OlympicLiftExerciseName.CLEAN = 11
OlympicLiftExerciseName.DUMBBELL_CLEAN = 12
OlympicLiftExerciseName.DUMBBELL_HANG_PULL = 13
OlympicLiftExerciseName.ONE_HAND_DUMBBELL_SPLIT_SNATCH = 14
OlympicLiftExerciseName.PUSH_JERK = 15
OlympicLiftExerciseName.SINGLE_ARM_DUMBBELL_SNATCH = 16
OlympicLiftExerciseName.SINGLE_ARM_HANG_SNATCH = 17
OlympicLiftExerciseName.SINGLE_ARM_KETTLEBELL_SNATCH = 18
OlympicLiftExerciseName.SPLIT_JERK = 19
OlympicLiftExerciseName.SQUAT_CLEAN_AND_JERK = 20


@dataclass
class PlankExerciseName:
    value: int


PlankExerciseName._45_DEGREE_PLANK = 0
PlankExerciseName.WEIGHTED_45_DEGREE_PLANK = 1
PlankExerciseName._90_DEGREE_STATIC_HOLD = 2
PlankExerciseName.WEIGHTED_90_DEGREE_STATIC_HOLD = 3
PlankExerciseName.BEAR_CRAWL = 4
PlankExerciseName.WEIGHTED_BEAR_CRAWL = 5
PlankExerciseName.CROSS_BODY_MOUNTAIN_CLIMBER = 6
PlankExerciseName.WEIGHTED_CROSS_BODY_MOUNTAIN_CLIMBER = 7
PlankExerciseName.ELBOW_PLANK_PIKE_JACKS = 8
PlankExerciseName.WEIGHTED_ELBOW_PLANK_PIKE_JACKS = 9
PlankExerciseName.ELEVATED_FEET_PLANK = 10
PlankExerciseName.WEIGHTED_ELEVATED_FEET_PLANK = 11
PlankExerciseName.ELEVATOR_ABS = 12
PlankExerciseName.WEIGHTED_ELEVATOR_ABS = 13
PlankExerciseName.EXTENDED_PLANK = 14
PlankExerciseName.WEIGHTED_EXTENDED_PLANK = 15
PlankExerciseName.FULL_PLANK_PASSE_TWIST = 16
PlankExerciseName.WEIGHTED_FULL_PLANK_PASSE_TWIST = 17
PlankExerciseName.INCHING_ELBOW_PLANK = 18
PlankExerciseName.WEIGHTED_INCHING_ELBOW_PLANK = 19
PlankExerciseName.INCHWORM_TO_SIDE_PLANK = 20
PlankExerciseName.WEIGHTED_INCHWORM_TO_SIDE_PLANK = 21
PlankExerciseName.KNEELING_PLANK = 22
PlankExerciseName.WEIGHTED_KNEELING_PLANK = 23
PlankExerciseName.KNEELING_SIDE_PLANK_WITH_LEG_LIFT = 24
PlankExerciseName.WEIGHTED_KNEELING_SIDE_PLANK_WITH_LEG_LIFT = 25
PlankExerciseName.LATERAL_ROLL = 26
PlankExerciseName.WEIGHTED_LATERAL_ROLL = 27
PlankExerciseName.LYING_REVERSE_PLANK = 28
PlankExerciseName.WEIGHTED_LYING_REVERSE_PLANK = 29
PlankExerciseName.MEDICINE_BALL_MOUNTAIN_CLIMBER = 30
PlankExerciseName.WEIGHTED_MEDICINE_BALL_MOUNTAIN_CLIMBER = 31
PlankExerciseName.MODIFIED_MOUNTAIN_CLIMBER_AND_EXTENSION = 32
PlankExerciseName.WEIGHTED_MODIFIED_MOUNTAIN_CLIMBER_AND_EXTENSION = 33
PlankExerciseName.MOUNTAIN_CLIMBER = 34
PlankExerciseName.WEIGHTED_MOUNTAIN_CLIMBER = 35
PlankExerciseName.MOUNTAIN_CLIMBER_ON_SLIDING_DISCS = 36
PlankExerciseName.WEIGHTED_MOUNTAIN_CLIMBER_ON_SLIDING_DISCS = 37
PlankExerciseName.MOUNTAIN_CLIMBER_WITH_FEET_ON_BOSU_BALL = 38
PlankExerciseName.WEIGHTED_MOUNTAIN_CLIMBER_WITH_FEET_ON_BOSU_BALL = 39
PlankExerciseName.MOUNTAIN_CLIMBER_WITH_HANDS_ON_BENCH = 40
PlankExerciseName.MOUNTAIN_CLIMBER_WITH_HANDS_ON_SWISS_BALL = 41
PlankExerciseName.WEIGHTED_MOUNTAIN_CLIMBER_WITH_HANDS_ON_SWISS_BALL = 42
PlankExerciseName.PLANK = 43
PlankExerciseName.PLANK_JACKS_WITH_FEET_ON_SLIDING_DISCS = 44
PlankExerciseName.WEIGHTED_PLANK_JACKS_WITH_FEET_ON_SLIDING_DISCS = 45
PlankExerciseName.PLANK_KNEE_TWIST = 46
PlankExerciseName.WEIGHTED_PLANK_KNEE_TWIST = 47
PlankExerciseName.PLANK_PIKE_JUMPS = 48
PlankExerciseName.WEIGHTED_PLANK_PIKE_JUMPS = 49
PlankExerciseName.PLANK_PIKES = 50
PlankExerciseName.WEIGHTED_PLANK_PIKES = 51
PlankExerciseName.PLANK_TO_STAND_UP = 52
PlankExerciseName.WEIGHTED_PLANK_TO_STAND_UP = 53
PlankExerciseName.PLANK_WITH_ARM_RAISE = 54
PlankExerciseName.WEIGHTED_PLANK_WITH_ARM_RAISE = 55
PlankExerciseName.PLANK_WITH_KNEE_TO_ELBOW = 56
PlankExerciseName.WEIGHTED_PLANK_WITH_KNEE_TO_ELBOW = 57
PlankExerciseName.PLANK_WITH_OBLIQUE_CRUNCH = 58
PlankExerciseName.WEIGHTED_PLANK_WITH_OBLIQUE_CRUNCH = 59
PlankExerciseName.PLYOMETRIC_SIDE_PLANK = 60
PlankExerciseName.WEIGHTED_PLYOMETRIC_SIDE_PLANK = 61
PlankExerciseName.ROLLING_SIDE_PLANK = 62
PlankExerciseName.WEIGHTED_ROLLING_SIDE_PLANK = 63
PlankExerciseName.SIDE_KICK_PLANK = 64
PlankExerciseName.WEIGHTED_SIDE_KICK_PLANK = 65
PlankExerciseName.SIDE_PLANK = 66
PlankExerciseName.WEIGHTED_SIDE_PLANK = 67
PlankExerciseName.SIDE_PLANK_AND_ROW = 68
PlankExerciseName.WEIGHTED_SIDE_PLANK_AND_ROW = 69
PlankExerciseName.SIDE_PLANK_LIFT = 70
PlankExerciseName.WEIGHTED_SIDE_PLANK_LIFT = 71
PlankExerciseName.SIDE_PLANK_WITH_ELBOW_ON_BOSU_BALL = 72
PlankExerciseName.WEIGHTED_SIDE_PLANK_WITH_ELBOW_ON_BOSU_BALL = 73
PlankExerciseName.SIDE_PLANK_WITH_FEET_ON_BENCH = 74
PlankExerciseName.WEIGHTED_SIDE_PLANK_WITH_FEET_ON_BENCH = 75
PlankExerciseName.SIDE_PLANK_WITH_KNEE_CIRCLE = 76
PlankExerciseName.WEIGHTED_SIDE_PLANK_WITH_KNEE_CIRCLE = 77
PlankExerciseName.SIDE_PLANK_WITH_KNEE_TUCK = 78
PlankExerciseName.WEIGHTED_SIDE_PLANK_WITH_KNEE_TUCK = 79
PlankExerciseName.SIDE_PLANK_WITH_LEG_LIFT = 80
PlankExerciseName.WEIGHTED_SIDE_PLANK_WITH_LEG_LIFT = 81
PlankExerciseName.SIDE_PLANK_WITH_REACH_UNDER = 82
PlankExerciseName.WEIGHTED_SIDE_PLANK_WITH_REACH_UNDER = 83
PlankExerciseName.SINGLE_LEG_ELEVATED_FEET_PLANK = 84
PlankExerciseName.WEIGHTED_SINGLE_LEG_ELEVATED_FEET_PLANK = 85
PlankExerciseName.SINGLE_LEG_FLEX_AND_EXTEND = 86
PlankExerciseName.WEIGHTED_SINGLE_LEG_FLEX_AND_EXTEND = 87
PlankExerciseName.SINGLE_LEG_SIDE_PLANK = 88
PlankExerciseName.WEIGHTED_SINGLE_LEG_SIDE_PLANK = 89
PlankExerciseName.SPIDERMAN_PLANK = 90
PlankExerciseName.WEIGHTED_SPIDERMAN_PLANK = 91
PlankExerciseName.STRAIGHT_ARM_PLANK = 92
PlankExerciseName.WEIGHTED_STRAIGHT_ARM_PLANK = 93
PlankExerciseName.STRAIGHT_ARM_PLANK_WITH_SHOULDER_TOUCH = 94
PlankExerciseName.WEIGHTED_STRAIGHT_ARM_PLANK_WITH_SHOULDER_TOUCH = 95
PlankExerciseName.SWISS_BALL_PLANK = 96
PlankExerciseName.WEIGHTED_SWISS_BALL_PLANK = 97
PlankExerciseName.SWISS_BALL_PLANK_LEG_LIFT = 98
PlankExerciseName.WEIGHTED_SWISS_BALL_PLANK_LEG_LIFT = 99
PlankExerciseName.SWISS_BALL_PLANK_LEG_LIFT_AND_HOLD = 100
PlankExerciseName.SWISS_BALL_PLANK_WITH_FEET_ON_BENCH = 101
PlankExerciseName.WEIGHTED_SWISS_BALL_PLANK_WITH_FEET_ON_BENCH = 102
PlankExerciseName.SWISS_BALL_PRONE_JACKKNIFE = 103
PlankExerciseName.WEIGHTED_SWISS_BALL_PRONE_JACKKNIFE = 104
PlankExerciseName.SWISS_BALL_SIDE_PLANK = 105
PlankExerciseName.WEIGHTED_SWISS_BALL_SIDE_PLANK = 106
PlankExerciseName.THREE_WAY_PLANK = 107
PlankExerciseName.WEIGHTED_THREE_WAY_PLANK = 108
PlankExerciseName.TOWEL_PLANK_AND_KNEE_IN = 109
PlankExerciseName.WEIGHTED_TOWEL_PLANK_AND_KNEE_IN = 110
PlankExerciseName.T_STABILIZATION = 111
PlankExerciseName.WEIGHTED_T_STABILIZATION = 112
PlankExerciseName.TURKISH_GET_UP_TO_SIDE_PLANK = 113
PlankExerciseName.WEIGHTED_TURKISH_GET_UP_TO_SIDE_PLANK = 114
PlankExerciseName.TWO_POINT_PLANK = 115
PlankExerciseName.WEIGHTED_TWO_POINT_PLANK = 116
PlankExerciseName.WEIGHTED_PLANK = 117
PlankExerciseName.WIDE_STANCE_PLANK_WITH_DIAGONAL_ARM_LIFT = 118
PlankExerciseName.WEIGHTED_WIDE_STANCE_PLANK_WITH_DIAGONAL_ARM_LIFT = 119
PlankExerciseName.WIDE_STANCE_PLANK_WITH_DIAGONAL_LEG_LIFT = 120
PlankExerciseName.WEIGHTED_WIDE_STANCE_PLANK_WITH_DIAGONAL_LEG_LIFT = 121
PlankExerciseName.WIDE_STANCE_PLANK_WITH_LEG_LIFT = 122
PlankExerciseName.WEIGHTED_WIDE_STANCE_PLANK_WITH_LEG_LIFT = 123
PlankExerciseName.WIDE_STANCE_PLANK_WITH_OPPOSITE_ARM_AND_LEG_LIFT = 124
PlankExerciseName.WEIGHTED_MOUNTAIN_CLIMBER_WITH_HANDS_ON_BENCH = 125
PlankExerciseName.WEIGHTED_SWISS_BALL_PLANK_LEG_LIFT_AND_HOLD = 126
PlankExerciseName.WEIGHTED_WIDE_STANCE_PLANK_WITH_OPPOSITE_ARM_AND_LEG_LIFT = 127
PlankExerciseName.PLANK_WITH_FEET_ON_SWISS_BALL = 128
PlankExerciseName.SIDE_PLANK_TO_PLANK_WITH_REACH_UNDER = 129
PlankExerciseName.BRIDGE_WITH_GLUTE_LOWER_LIFT = 130
PlankExerciseName.BRIDGE_ONE_LEG_BRIDGE = 131
PlankExerciseName.PLANK_WITH_ARM_VARIATIONS = 132
PlankExerciseName.PLANK_WITH_LEG_LIFT = 133
PlankExerciseName.REVERSE_PLANK_WITH_LEG_PULL = 134


@dataclass
class PlyoExerciseName:
    value: int


PlyoExerciseName.ALTERNATING_JUMP_LUNGE = 0
PlyoExerciseName.WEIGHTED_ALTERNATING_JUMP_LUNGE = 1
PlyoExerciseName.BARBELL_JUMP_SQUAT = 2
PlyoExerciseName.BODY_WEIGHT_JUMP_SQUAT = 3
PlyoExerciseName.WEIGHTED_JUMP_SQUAT = 4
PlyoExerciseName.CROSS_KNEE_STRIKE = 5
PlyoExerciseName.WEIGHTED_CROSS_KNEE_STRIKE = 6
PlyoExerciseName.DEPTH_JUMP = 7
PlyoExerciseName.WEIGHTED_DEPTH_JUMP = 8
PlyoExerciseName.DUMBBELL_JUMP_SQUAT = 9
PlyoExerciseName.DUMBBELL_SPLIT_JUMP = 10
PlyoExerciseName.FRONT_KNEE_STRIKE = 11
PlyoExerciseName.WEIGHTED_FRONT_KNEE_STRIKE = 12
PlyoExerciseName.HIGH_BOX_JUMP = 13
PlyoExerciseName.WEIGHTED_HIGH_BOX_JUMP = 14
PlyoExerciseName.ISOMETRIC_EXPLOSIVE_BODY_WEIGHT_JUMP_SQUAT = 15
PlyoExerciseName.WEIGHTED_ISOMETRIC_EXPLOSIVE_JUMP_SQUAT = 16
PlyoExerciseName.LATERAL_LEAP_AND_HOP = 17
PlyoExerciseName.WEIGHTED_LATERAL_LEAP_AND_HOP = 18
PlyoExerciseName.LATERAL_PLYO_SQUATS = 19
PlyoExerciseName.WEIGHTED_LATERAL_PLYO_SQUATS = 20
PlyoExerciseName.LATERAL_SLIDE = 21
PlyoExerciseName.WEIGHTED_LATERAL_SLIDE = 22
PlyoExerciseName.MEDICINE_BALL_OVERHEAD_THROWS = 23
PlyoExerciseName.MEDICINE_BALL_SIDE_THROW = 24
PlyoExerciseName.MEDICINE_BALL_SLAM = 25
PlyoExerciseName.SIDE_TO_SIDE_MEDICINE_BALL_THROWS = 26
PlyoExerciseName.SIDE_TO_SIDE_SHUFFLE_JUMP = 27
PlyoExerciseName.WEIGHTED_SIDE_TO_SIDE_SHUFFLE_JUMP = 28
PlyoExerciseName.SQUAT_JUMP_ONTO_BOX = 29
PlyoExerciseName.WEIGHTED_SQUAT_JUMP_ONTO_BOX = 30
PlyoExerciseName.SQUAT_JUMPS_IN_AND_OUT = 31
PlyoExerciseName.WEIGHTED_SQUAT_JUMPS_IN_AND_OUT = 32


@dataclass
class PullUpExerciseName:
    value: int


PullUpExerciseName.BANDED_PULL_UPS = 0
PullUpExerciseName._30_DEGREE_LAT_PULLDOWN = 1
PullUpExerciseName.BAND_ASSISTED_CHIN_UP = 2
PullUpExerciseName.CLOSE_GRIP_CHIN_UP = 3
PullUpExerciseName.WEIGHTED_CLOSE_GRIP_CHIN_UP = 4
PullUpExerciseName.CLOSE_GRIP_LAT_PULLDOWN = 5
PullUpExerciseName.CROSSOVER_CHIN_UP = 6
PullUpExerciseName.WEIGHTED_CROSSOVER_CHIN_UP = 7
PullUpExerciseName.EZ_BAR_PULLOVER = 8
PullUpExerciseName.HANGING_HURDLE = 9
PullUpExerciseName.WEIGHTED_HANGING_HURDLE = 10
PullUpExerciseName.KNEELING_LAT_PULLDOWN = 11
PullUpExerciseName.KNEELING_UNDERHAND_GRIP_LAT_PULLDOWN = 12
PullUpExerciseName.LAT_PULLDOWN = 13
PullUpExerciseName.MIXED_GRIP_CHIN_UP = 14
PullUpExerciseName.WEIGHTED_MIXED_GRIP_CHIN_UP = 15
PullUpExerciseName.MIXED_GRIP_PULL_UP = 16
PullUpExerciseName.WEIGHTED_MIXED_GRIP_PULL_UP = 17
PullUpExerciseName.REVERSE_GRIP_PULLDOWN = 18
PullUpExerciseName.STANDING_CABLE_PULLOVER = 19
PullUpExerciseName.STRAIGHT_ARM_PULLDOWN = 20
PullUpExerciseName.SWISS_BALL_EZ_BAR_PULLOVER = 21
PullUpExerciseName.TOWEL_PULL_UP = 22
PullUpExerciseName.WEIGHTED_TOWEL_PULL_UP = 23
PullUpExerciseName.WEIGHTED_PULL_UP = 24
PullUpExerciseName.WIDE_GRIP_LAT_PULLDOWN = 25
PullUpExerciseName.WIDE_GRIP_PULL_UP = 26
PullUpExerciseName.WEIGHTED_WIDE_GRIP_PULL_UP = 27
PullUpExerciseName.BURPEE_PULL_UP = 28
PullUpExerciseName.WEIGHTED_BURPEE_PULL_UP = 29
PullUpExerciseName.JUMPING_PULL_UPS = 30
PullUpExerciseName.WEIGHTED_JUMPING_PULL_UPS = 31
PullUpExerciseName.KIPPING_PULL_UP = 32
PullUpExerciseName.WEIGHTED_KIPPING_PULL_UP = 33
PullUpExerciseName.L_PULL_UP = 34
PullUpExerciseName.WEIGHTED_L_PULL_UP = 35
PullUpExerciseName.SUSPENDED_CHIN_UP = 36
PullUpExerciseName.WEIGHTED_SUSPENDED_CHIN_UP = 37
PullUpExerciseName.PULL_UP = 38


@dataclass
class PushUpExerciseName:
    value: int


PushUpExerciseName.CHEST_PRESS_WITH_BAND = 0
PushUpExerciseName.ALTERNATING_STAGGERED_PUSH_UP = 1
PushUpExerciseName.WEIGHTED_ALTERNATING_STAGGERED_PUSH_UP = 2
PushUpExerciseName.ALTERNATING_HANDS_MEDICINE_BALL_PUSH_UP = 3
PushUpExerciseName.WEIGHTED_ALTERNATING_HANDS_MEDICINE_BALL_PUSH_UP = 4
PushUpExerciseName.BOSU_BALL_PUSH_UP = 5
PushUpExerciseName.WEIGHTED_BOSU_BALL_PUSH_UP = 6
PushUpExerciseName.CLAPPING_PUSH_UP = 7
PushUpExerciseName.WEIGHTED_CLAPPING_PUSH_UP = 8
PushUpExerciseName.CLOSE_GRIP_MEDICINE_BALL_PUSH_UP = 9
PushUpExerciseName.WEIGHTED_CLOSE_GRIP_MEDICINE_BALL_PUSH_UP = 10
PushUpExerciseName.CLOSE_HANDS_PUSH_UP = 11
PushUpExerciseName.WEIGHTED_CLOSE_HANDS_PUSH_UP = 12
PushUpExerciseName.DECLINE_PUSH_UP = 13
PushUpExerciseName.WEIGHTED_DECLINE_PUSH_UP = 14
PushUpExerciseName.DIAMOND_PUSH_UP = 15
PushUpExerciseName.WEIGHTED_DIAMOND_PUSH_UP = 16
PushUpExerciseName.EXPLOSIVE_CROSSOVER_PUSH_UP = 17
PushUpExerciseName.WEIGHTED_EXPLOSIVE_CROSSOVER_PUSH_UP = 18
PushUpExerciseName.EXPLOSIVE_PUSH_UP = 19
PushUpExerciseName.WEIGHTED_EXPLOSIVE_PUSH_UP = 20
PushUpExerciseName.FEET_ELEVATED_SIDE_TO_SIDE_PUSH_UP = 21
PushUpExerciseName.WEIGHTED_FEET_ELEVATED_SIDE_TO_SIDE_PUSH_UP = 22
PushUpExerciseName.HAND_RELEASE_PUSH_UP = 23
PushUpExerciseName.WEIGHTED_HAND_RELEASE_PUSH_UP = 24
PushUpExerciseName.HANDSTAND_PUSH_UP = 25
PushUpExerciseName.WEIGHTED_HANDSTAND_PUSH_UP = 26
PushUpExerciseName.INCLINE_PUSH_UP = 27
PushUpExerciseName.WEIGHTED_INCLINE_PUSH_UP = 28
PushUpExerciseName.ISOMETRIC_EXPLOSIVE_PUSH_UP = 29
PushUpExerciseName.WEIGHTED_ISOMETRIC_EXPLOSIVE_PUSH_UP = 30
PushUpExerciseName.JUDO_PUSH_UP = 31
PushUpExerciseName.WEIGHTED_JUDO_PUSH_UP = 32
PushUpExerciseName.KNEELING_PUSH_UP = 33
PushUpExerciseName.WEIGHTED_KNEELING_PUSH_UP = 34
PushUpExerciseName.MEDICINE_BALL_CHEST_PASS = 35
PushUpExerciseName.MEDICINE_BALL_PUSH_UP = 36
PushUpExerciseName.WEIGHTED_MEDICINE_BALL_PUSH_UP = 37
PushUpExerciseName.ONE_ARM_PUSH_UP = 38
PushUpExerciseName.WEIGHTED_ONE_ARM_PUSH_UP = 39
PushUpExerciseName.WEIGHTED_PUSH_UP = 40
PushUpExerciseName.PUSH_UP_AND_ROW = 41
PushUpExerciseName.WEIGHTED_PUSH_UP_AND_ROW = 42
PushUpExerciseName.PUSH_UP_PLUS = 43
PushUpExerciseName.WEIGHTED_PUSH_UP_PLUS = 44
PushUpExerciseName.PUSH_UP_WITH_FEET_ON_SWISS_BALL = 45
PushUpExerciseName.WEIGHTED_PUSH_UP_WITH_FEET_ON_SWISS_BALL = 46
PushUpExerciseName.PUSH_UP_WITH_ONE_HAND_ON_MEDICINE_BALL = 47
PushUpExerciseName.WEIGHTED_PUSH_UP_WITH_ONE_HAND_ON_MEDICINE_BALL = 48
PushUpExerciseName.SHOULDER_PUSH_UP = 49
PushUpExerciseName.WEIGHTED_SHOULDER_PUSH_UP = 50
PushUpExerciseName.SINGLE_ARM_MEDICINE_BALL_PUSH_UP = 51
PushUpExerciseName.WEIGHTED_SINGLE_ARM_MEDICINE_BALL_PUSH_UP = 52
PushUpExerciseName.SPIDERMAN_PUSH_UP = 53
PushUpExerciseName.WEIGHTED_SPIDERMAN_PUSH_UP = 54
PushUpExerciseName.STACKED_FEET_PUSH_UP = 55
PushUpExerciseName.WEIGHTED_STACKED_FEET_PUSH_UP = 56
PushUpExerciseName.STAGGERED_HANDS_PUSH_UP = 57
PushUpExerciseName.WEIGHTED_STAGGERED_HANDS_PUSH_UP = 58
PushUpExerciseName.SUSPENDED_PUSH_UP = 59
PushUpExerciseName.WEIGHTED_SUSPENDED_PUSH_UP = 60
PushUpExerciseName.SWISS_BALL_PUSH_UP = 61
PushUpExerciseName.WEIGHTED_SWISS_BALL_PUSH_UP = 62
PushUpExerciseName.SWISS_BALL_PUSH_UP_PLUS = 63
PushUpExerciseName.WEIGHTED_SWISS_BALL_PUSH_UP_PLUS = 64
PushUpExerciseName.T_PUSH_UP = 65
PushUpExerciseName.WEIGHTED_T_PUSH_UP = 66
PushUpExerciseName.TRIPLE_STOP_PUSH_UP = 67
PushUpExerciseName.WEIGHTED_TRIPLE_STOP_PUSH_UP = 68
PushUpExerciseName.WIDE_HANDS_PUSH_UP = 69
PushUpExerciseName.WEIGHTED_WIDE_HANDS_PUSH_UP = 70
PushUpExerciseName.PARALLETTE_HANDSTAND_PUSH_UP = 71
PushUpExerciseName.WEIGHTED_PARALLETTE_HANDSTAND_PUSH_UP = 72
PushUpExerciseName.RING_HANDSTAND_PUSH_UP = 73
PushUpExerciseName.WEIGHTED_RING_HANDSTAND_PUSH_UP = 74
PushUpExerciseName.RING_PUSH_UP = 75
PushUpExerciseName.WEIGHTED_RING_PUSH_UP = 76
PushUpExerciseName.PUSH_UP = 77
PushUpExerciseName.PILATES_PUSHUP = 78


@dataclass
class RowExerciseName:
    value: int


RowExerciseName.BARBELL_STRAIGHT_LEG_DEADLIFT_TO_ROW = 0
RowExerciseName.CABLE_ROW_STANDING = 1
RowExerciseName.DUMBBELL_ROW = 2
RowExerciseName.ELEVATED_FEET_INVERTED_ROW = 3
RowExerciseName.WEIGHTED_ELEVATED_FEET_INVERTED_ROW = 4
RowExerciseName.FACE_PULL = 5
RowExerciseName.FACE_PULL_WITH_EXTERNAL_ROTATION = 6
RowExerciseName.INVERTED_ROW_WITH_FEET_ON_SWISS_BALL = 7
RowExerciseName.WEIGHTED_INVERTED_ROW_WITH_FEET_ON_SWISS_BALL = 8
RowExerciseName.KETTLEBELL_ROW = 9
RowExerciseName.MODIFIED_INVERTED_ROW = 10
RowExerciseName.WEIGHTED_MODIFIED_INVERTED_ROW = 11
RowExerciseName.NEUTRAL_GRIP_ALTERNATING_DUMBBELL_ROW = 12
RowExerciseName.ONE_ARM_BENT_OVER_ROW = 13
RowExerciseName.ONE_LEGGED_DUMBBELL_ROW = 14
RowExerciseName.RENEGADE_ROW = 15
RowExerciseName.REVERSE_GRIP_BARBELL_ROW = 16
RowExerciseName.ROPE_HANDLE_CABLE_ROW = 17
RowExerciseName.SEATED_CABLE_ROW = 18
RowExerciseName.SEATED_DUMBBELL_ROW = 19
RowExerciseName.SINGLE_ARM_CABLE_ROW = 20
RowExerciseName.SINGLE_ARM_CABLE_ROW_AND_ROTATION = 21
RowExerciseName.SINGLE_ARM_INVERTED_ROW = 22
RowExerciseName.WEIGHTED_SINGLE_ARM_INVERTED_ROW = 23
RowExerciseName.SINGLE_ARM_NEUTRAL_GRIP_DUMBBELL_ROW = 24
RowExerciseName.SINGLE_ARM_NEUTRAL_GRIP_DUMBBELL_ROW_AND_ROTATION = 25
RowExerciseName.SUSPENDED_INVERTED_ROW = 26
RowExerciseName.WEIGHTED_SUSPENDED_INVERTED_ROW = 27
RowExerciseName.T_BAR_ROW = 28
RowExerciseName.TOWEL_GRIP_INVERTED_ROW = 29
RowExerciseName.WEIGHTED_TOWEL_GRIP_INVERTED_ROW = 30
RowExerciseName.UNDERHAND_GRIP_CABLE_ROW = 31
RowExerciseName.V_GRIP_CABLE_ROW = 32
RowExerciseName.WIDE_GRIP_SEATED_CABLE_ROW = 33


@dataclass
class ShoulderPressExerciseName:
    value: int


ShoulderPressExerciseName.ALTERNATING_DUMBBELL_SHOULDER_PRESS = 0
ShoulderPressExerciseName.ARNOLD_PRESS = 1
ShoulderPressExerciseName.BARBELL_FRONT_SQUAT_TO_PUSH_PRESS = 2
ShoulderPressExerciseName.BARBELL_PUSH_PRESS = 3
ShoulderPressExerciseName.BARBELL_SHOULDER_PRESS = 4
ShoulderPressExerciseName.DEAD_CURL_PRESS = 5
ShoulderPressExerciseName.DUMBBELL_ALTERNATING_SHOULDER_PRESS_AND_TWIST = 6
ShoulderPressExerciseName.DUMBBELL_HAMMER_CURL_TO_LUNGE_TO_PRESS = 7
ShoulderPressExerciseName.DUMBBELL_PUSH_PRESS = 8
ShoulderPressExerciseName.FLOOR_INVERTED_SHOULDER_PRESS = 9
ShoulderPressExerciseName.WEIGHTED_FLOOR_INVERTED_SHOULDER_PRESS = 10
ShoulderPressExerciseName.INVERTED_SHOULDER_PRESS = 11
ShoulderPressExerciseName.WEIGHTED_INVERTED_SHOULDER_PRESS = 12
ShoulderPressExerciseName.ONE_ARM_PUSH_PRESS = 13
ShoulderPressExerciseName.OVERHEAD_BARBELL_PRESS = 14
ShoulderPressExerciseName.OVERHEAD_DUMBBELL_PRESS = 15
ShoulderPressExerciseName.SEATED_BARBELL_SHOULDER_PRESS = 16
ShoulderPressExerciseName.SEATED_DUMBBELL_SHOULDER_PRESS = 17
ShoulderPressExerciseName.SINGLE_ARM_DUMBBELL_SHOULDER_PRESS = 18
ShoulderPressExerciseName.SINGLE_ARM_STEP_UP_AND_PRESS = 19
ShoulderPressExerciseName.SMITH_MACHINE_OVERHEAD_PRESS = 20
ShoulderPressExerciseName.SPLIT_STANCE_HAMMER_CURL_TO_PRESS = 21
ShoulderPressExerciseName.SWISS_BALL_DUMBBELL_SHOULDER_PRESS = 22
ShoulderPressExerciseName.WEIGHT_PLATE_FRONT_RAISE = 23


@dataclass
class ShoulderStabilityExerciseName:
    value: int


ShoulderStabilityExerciseName._90_DEGREE_CABLE_EXTERNAL_ROTATION = 0
ShoulderStabilityExerciseName.BAND_EXTERNAL_ROTATION = 1
ShoulderStabilityExerciseName.BAND_INTERNAL_ROTATION = 2
ShoulderStabilityExerciseName.BENT_ARM_LATERAL_RAISE_AND_EXTERNAL_ROTATION = 3
ShoulderStabilityExerciseName.CABLE_EXTERNAL_ROTATION = 4
ShoulderStabilityExerciseName.DUMBBELL_FACE_PULL_WITH_EXTERNAL_ROTATION = 5
ShoulderStabilityExerciseName.FLOOR_I_RAISE = 6
ShoulderStabilityExerciseName.WEIGHTED_FLOOR_I_RAISE = 7
ShoulderStabilityExerciseName.FLOOR_T_RAISE = 8
ShoulderStabilityExerciseName.WEIGHTED_FLOOR_T_RAISE = 9
ShoulderStabilityExerciseName.FLOOR_Y_RAISE = 10
ShoulderStabilityExerciseName.WEIGHTED_FLOOR_Y_RAISE = 11
ShoulderStabilityExerciseName.INCLINE_I_RAISE = 12
ShoulderStabilityExerciseName.WEIGHTED_INCLINE_I_RAISE = 13
ShoulderStabilityExerciseName.INCLINE_L_RAISE = 14
ShoulderStabilityExerciseName.WEIGHTED_INCLINE_L_RAISE = 15
ShoulderStabilityExerciseName.INCLINE_T_RAISE = 16
ShoulderStabilityExerciseName.WEIGHTED_INCLINE_T_RAISE = 17
ShoulderStabilityExerciseName.INCLINE_W_RAISE = 18
ShoulderStabilityExerciseName.WEIGHTED_INCLINE_W_RAISE = 19
ShoulderStabilityExerciseName.INCLINE_Y_RAISE = 20
ShoulderStabilityExerciseName.WEIGHTED_INCLINE_Y_RAISE = 21
ShoulderStabilityExerciseName.LYING_EXTERNAL_ROTATION = 22
ShoulderStabilityExerciseName.SEATED_DUMBBELL_EXTERNAL_ROTATION = 23
ShoulderStabilityExerciseName.STANDING_L_RAISE = 24
ShoulderStabilityExerciseName.SWISS_BALL_I_RAISE = 25
ShoulderStabilityExerciseName.WEIGHTED_SWISS_BALL_I_RAISE = 26
ShoulderStabilityExerciseName.SWISS_BALL_T_RAISE = 27
ShoulderStabilityExerciseName.WEIGHTED_SWISS_BALL_T_RAISE = 28
ShoulderStabilityExerciseName.SWISS_BALL_W_RAISE = 29
ShoulderStabilityExerciseName.WEIGHTED_SWISS_BALL_W_RAISE = 30
ShoulderStabilityExerciseName.SWISS_BALL_Y_RAISE = 31
ShoulderStabilityExerciseName.WEIGHTED_SWISS_BALL_Y_RAISE = 32


@dataclass
class ShrugExerciseName:
    value: int


ShrugExerciseName.BARBELL_JUMP_SHRUG = 0
ShrugExerciseName.BARBELL_SHRUG = 1
ShrugExerciseName.BARBELL_UPRIGHT_ROW = 2
ShrugExerciseName.BEHIND_THE_BACK_SMITH_MACHINE_SHRUG = 3
ShrugExerciseName.DUMBBELL_JUMP_SHRUG = 4
ShrugExerciseName.DUMBBELL_SHRUG = 5
ShrugExerciseName.DUMBBELL_UPRIGHT_ROW = 6
ShrugExerciseName.INCLINE_DUMBBELL_SHRUG = 7
ShrugExerciseName.OVERHEAD_BARBELL_SHRUG = 8
ShrugExerciseName.OVERHEAD_DUMBBELL_SHRUG = 9
ShrugExerciseName.SCAPTION_AND_SHRUG = 10
ShrugExerciseName.SCAPULAR_RETRACTION = 11
ShrugExerciseName.SERRATUS_CHAIR_SHRUG = 12
ShrugExerciseName.WEIGHTED_SERRATUS_CHAIR_SHRUG = 13
ShrugExerciseName.SERRATUS_SHRUG = 14
ShrugExerciseName.WEIGHTED_SERRATUS_SHRUG = 15
ShrugExerciseName.WIDE_GRIP_JUMP_SHRUG = 16


@dataclass
class SitUpExerciseName:
    value: int


SitUpExerciseName.ALTERNATING_SIT_UP = 0
SitUpExerciseName.WEIGHTED_ALTERNATING_SIT_UP = 1
SitUpExerciseName.BENT_KNEE_V_UP = 2
SitUpExerciseName.WEIGHTED_BENT_KNEE_V_UP = 3
SitUpExerciseName.BUTTERFLY_SIT_UP = 4
SitUpExerciseName.WEIGHTED_BUTTERFLY_SITUP = 5
SitUpExerciseName.CROSS_PUNCH_ROLL_UP = 6
SitUpExerciseName.WEIGHTED_CROSS_PUNCH_ROLL_UP = 7
SitUpExerciseName.CROSSED_ARMS_SIT_UP = 8
SitUpExerciseName.WEIGHTED_CROSSED_ARMS_SIT_UP = 9
SitUpExerciseName.GET_UP_SIT_UP = 10
SitUpExerciseName.WEIGHTED_GET_UP_SIT_UP = 11
SitUpExerciseName.HOVERING_SIT_UP = 12
SitUpExerciseName.WEIGHTED_HOVERING_SIT_UP = 13
SitUpExerciseName.KETTLEBELL_SIT_UP = 14
SitUpExerciseName.MEDICINE_BALL_ALTERNATING_V_UP = 15
SitUpExerciseName.MEDICINE_BALL_SIT_UP = 16
SitUpExerciseName.MEDICINE_BALL_V_UP = 17
SitUpExerciseName.MODIFIED_SIT_UP = 18
SitUpExerciseName.NEGATIVE_SIT_UP = 19
SitUpExerciseName.ONE_ARM_FULL_SIT_UP = 20
SitUpExerciseName.RECLINING_CIRCLE = 21
SitUpExerciseName.WEIGHTED_RECLINING_CIRCLE = 22
SitUpExerciseName.REVERSE_CURL_UP = 23
SitUpExerciseName.WEIGHTED_REVERSE_CURL_UP = 24
SitUpExerciseName.SINGLE_LEG_SWISS_BALL_JACKKNIFE = 25
SitUpExerciseName.WEIGHTED_SINGLE_LEG_SWISS_BALL_JACKKNIFE = 26
SitUpExerciseName.THE_TEASER = 27
SitUpExerciseName.THE_TEASER_WEIGHTED = 28
SitUpExerciseName.THREE_PART_ROLL_DOWN = 29
SitUpExerciseName.WEIGHTED_THREE_PART_ROLL_DOWN = 30
SitUpExerciseName.V_UP = 31
SitUpExerciseName.WEIGHTED_V_UP = 32
SitUpExerciseName.WEIGHTED_RUSSIAN_TWIST_ON_SWISS_BALL = 33
SitUpExerciseName.WEIGHTED_SIT_UP = 34
SitUpExerciseName.X_ABS = 35
SitUpExerciseName.WEIGHTED_X_ABS = 36
SitUpExerciseName.SIT_UP = 37


@dataclass
class SquatExerciseName:
    value: int


SquatExerciseName.LEG_PRESS = 0
SquatExerciseName.BACK_SQUAT_WITH_BODY_BAR = 1
SquatExerciseName.BACK_SQUATS = 2
SquatExerciseName.WEIGHTED_BACK_SQUATS = 3
SquatExerciseName.BALANCING_SQUAT = 4
SquatExerciseName.WEIGHTED_BALANCING_SQUAT = 5
SquatExerciseName.BARBELL_BACK_SQUAT = 6
SquatExerciseName.BARBELL_BOX_SQUAT = 7
SquatExerciseName.BARBELL_FRONT_SQUAT = 8
SquatExerciseName.BARBELL_HACK_SQUAT = 9
SquatExerciseName.BARBELL_HANG_SQUAT_SNATCH = 10
SquatExerciseName.BARBELL_LATERAL_STEP_UP = 11
SquatExerciseName.BARBELL_QUARTER_SQUAT = 12
SquatExerciseName.BARBELL_SIFF_SQUAT = 13
SquatExerciseName.BARBELL_SQUAT_SNATCH = 14
SquatExerciseName.BARBELL_SQUAT_WITH_HEELS_RAISED = 15
SquatExerciseName.BARBELL_STEPOVER = 16
SquatExerciseName.BARBELL_STEP_UP = 17
SquatExerciseName.BENCH_SQUAT_WITH_ROTATIONAL_CHOP = 18
SquatExerciseName.WEIGHTED_BENCH_SQUAT_WITH_ROTATIONAL_CHOP = 19
SquatExerciseName.BODY_WEIGHT_WALL_SQUAT = 20
SquatExerciseName.WEIGHTED_WALL_SQUAT = 21
SquatExerciseName.BOX_STEP_SQUAT = 22
SquatExerciseName.WEIGHTED_BOX_STEP_SQUAT = 23
SquatExerciseName.BRACED_SQUAT = 24
SquatExerciseName.CROSSED_ARM_BARBELL_FRONT_SQUAT = 25
SquatExerciseName.CROSSOVER_DUMBBELL_STEP_UP = 26
SquatExerciseName.DUMBBELL_FRONT_SQUAT = 27
SquatExerciseName.DUMBBELL_SPLIT_SQUAT = 28
SquatExerciseName.DUMBBELL_SQUAT = 29
SquatExerciseName.DUMBBELL_SQUAT_CLEAN = 30
SquatExerciseName.DUMBBELL_STEPOVER = 31
SquatExerciseName.DUMBBELL_STEP_UP = 32
SquatExerciseName.ELEVATED_SINGLE_LEG_SQUAT = 33
SquatExerciseName.WEIGHTED_ELEVATED_SINGLE_LEG_SQUAT = 34
SquatExerciseName.FIGURE_FOUR_SQUATS = 35
SquatExerciseName.WEIGHTED_FIGURE_FOUR_SQUATS = 36
SquatExerciseName.GOBLET_SQUAT = 37
SquatExerciseName.KETTLEBELL_SQUAT = 38
SquatExerciseName.KETTLEBELL_SWING_OVERHEAD = 39
SquatExerciseName.KETTLEBELL_SWING_WITH_FLIP_TO_SQUAT = 40
SquatExerciseName.LATERAL_DUMBBELL_STEP_UP = 41
SquatExerciseName.ONE_LEGGED_SQUAT = 42
SquatExerciseName.OVERHEAD_DUMBBELL_SQUAT = 43
SquatExerciseName.OVERHEAD_SQUAT = 44
SquatExerciseName.PARTIAL_SINGLE_LEG_SQUAT = 45
SquatExerciseName.WEIGHTED_PARTIAL_SINGLE_LEG_SQUAT = 46
SquatExerciseName.PISTOL_SQUAT = 47
SquatExerciseName.WEIGHTED_PISTOL_SQUAT = 48
SquatExerciseName.PLIE_SLIDES = 49
SquatExerciseName.WEIGHTED_PLIE_SLIDES = 50
SquatExerciseName.PLIE_SQUAT = 51
SquatExerciseName.WEIGHTED_PLIE_SQUAT = 52
SquatExerciseName.PRISONER_SQUAT = 53
SquatExerciseName.WEIGHTED_PRISONER_SQUAT = 54
SquatExerciseName.SINGLE_LEG_BENCH_GET_UP = 55
SquatExerciseName.WEIGHTED_SINGLE_LEG_BENCH_GET_UP = 56
SquatExerciseName.SINGLE_LEG_BENCH_SQUAT = 57
SquatExerciseName.WEIGHTED_SINGLE_LEG_BENCH_SQUAT = 58
SquatExerciseName.SINGLE_LEG_SQUAT_ON_SWISS_BALL = 59
SquatExerciseName.WEIGHTED_SINGLE_LEG_SQUAT_ON_SWISS_BALL = 60
SquatExerciseName.SQUAT = 61
SquatExerciseName.WEIGHTED_SQUAT = 62
SquatExerciseName.SQUATS_WITH_BAND = 63
SquatExerciseName.STAGGERED_SQUAT = 64
SquatExerciseName.WEIGHTED_STAGGERED_SQUAT = 65
SquatExerciseName.STEP_UP = 66
SquatExerciseName.WEIGHTED_STEP_UP = 67
SquatExerciseName.SUITCASE_SQUATS = 68
SquatExerciseName.SUMO_SQUAT = 69
SquatExerciseName.SUMO_SQUAT_SLIDE_IN = 70
SquatExerciseName.WEIGHTED_SUMO_SQUAT_SLIDE_IN = 71
SquatExerciseName.SUMO_SQUAT_TO_HIGH_PULL = 72
SquatExerciseName.SUMO_SQUAT_TO_STAND = 73
SquatExerciseName.WEIGHTED_SUMO_SQUAT_TO_STAND = 74
SquatExerciseName.SUMO_SQUAT_WITH_ROTATION = 75
SquatExerciseName.WEIGHTED_SUMO_SQUAT_WITH_ROTATION = 76
SquatExerciseName.SWISS_BALL_BODY_WEIGHT_WALL_SQUAT = 77
SquatExerciseName.WEIGHTED_SWISS_BALL_WALL_SQUAT = 78
SquatExerciseName.THRUSTERS = 79
SquatExerciseName.UNEVEN_SQUAT = 80
SquatExerciseName.WEIGHTED_UNEVEN_SQUAT = 81
SquatExerciseName.WAIST_SLIMMING_SQUAT = 82
SquatExerciseName.WALL_BALL = 83
SquatExerciseName.WIDE_STANCE_BARBELL_SQUAT = 84
SquatExerciseName.WIDE_STANCE_GOBLET_SQUAT = 85
SquatExerciseName.ZERCHER_SQUAT = 86
SquatExerciseName.KBS_OVERHEAD = 87  # Deprecated do not use
SquatExerciseName.SQUAT_AND_SIDE_KICK = 88
SquatExerciseName.SQUAT_JUMPS_IN_N_OUT = 89
SquatExerciseName.PILATES_PLIE_SQUATS_PARALLEL_TURNED_OUT_FLAT_AND_HEELS = 90
SquatExerciseName.RELEVE_STRAIGHT_LEG_AND_KNEE_BENT_WITH_ONE_LEG_VARIATION = 91


@dataclass
class TotalBodyExerciseName:
    value: int


TotalBodyExerciseName.BURPEE = 0
TotalBodyExerciseName.WEIGHTED_BURPEE = 1
TotalBodyExerciseName.BURPEE_BOX_JUMP = 2
TotalBodyExerciseName.WEIGHTED_BURPEE_BOX_JUMP = 3
TotalBodyExerciseName.HIGH_PULL_BURPEE = 4
TotalBodyExerciseName.MAN_MAKERS = 5
TotalBodyExerciseName.ONE_ARM_BURPEE = 6
TotalBodyExerciseName.SQUAT_THRUSTS = 7
TotalBodyExerciseName.WEIGHTED_SQUAT_THRUSTS = 8
TotalBodyExerciseName.SQUAT_PLANK_PUSH_UP = 9
TotalBodyExerciseName.WEIGHTED_SQUAT_PLANK_PUSH_UP = 10
TotalBodyExerciseName.STANDING_T_ROTATION_BALANCE = 11
TotalBodyExerciseName.WEIGHTED_STANDING_T_ROTATION_BALANCE = 12


@dataclass
class TricepsExtensionExerciseName:
    value: int


TricepsExtensionExerciseName.BENCH_DIP = 0
TricepsExtensionExerciseName.WEIGHTED_BENCH_DIP = 1
TricepsExtensionExerciseName.BODY_WEIGHT_DIP = 2
TricepsExtensionExerciseName.CABLE_KICKBACK = 3
TricepsExtensionExerciseName.CABLE_LYING_TRICEPS_EXTENSION = 4
TricepsExtensionExerciseName.CABLE_OVERHEAD_TRICEPS_EXTENSION = 5
TricepsExtensionExerciseName.DUMBBELL_KICKBACK = 6
TricepsExtensionExerciseName.DUMBBELL_LYING_TRICEPS_EXTENSION = 7
TricepsExtensionExerciseName.EZ_BAR_OVERHEAD_TRICEPS_EXTENSION = 8
TricepsExtensionExerciseName.INCLINE_DIP = 9
TricepsExtensionExerciseName.WEIGHTED_INCLINE_DIP = 10
TricepsExtensionExerciseName.INCLINE_EZ_BAR_LYING_TRICEPS_EXTENSION = 11
TricepsExtensionExerciseName.LYING_DUMBBELL_PULLOVER_TO_EXTENSION = 12
TricepsExtensionExerciseName.LYING_EZ_BAR_TRICEPS_EXTENSION = 13
TricepsExtensionExerciseName.LYING_TRICEPS_EXTENSION_TO_CLOSE_GRIP_BENCH_PRESS = 14
TricepsExtensionExerciseName.OVERHEAD_DUMBBELL_TRICEPS_EXTENSION = 15
TricepsExtensionExerciseName.RECLINING_TRICEPS_PRESS = 16
TricepsExtensionExerciseName.REVERSE_GRIP_PRESSDOWN = 17
TricepsExtensionExerciseName.REVERSE_GRIP_TRICEPS_PRESSDOWN = 18
TricepsExtensionExerciseName.ROPE_PRESSDOWN = 19
TricepsExtensionExerciseName.SEATED_BARBELL_OVERHEAD_TRICEPS_EXTENSION = 20
TricepsExtensionExerciseName.SEATED_DUMBBELL_OVERHEAD_TRICEPS_EXTENSION = 21
TricepsExtensionExerciseName.SEATED_EZ_BAR_OVERHEAD_TRICEPS_EXTENSION = 22
TricepsExtensionExerciseName.SEATED_SINGLE_ARM_OVERHEAD_DUMBBELL_EXTENSION = 23
TricepsExtensionExerciseName.SINGLE_ARM_DUMBBELL_OVERHEAD_TRICEPS_EXTENSION = 24
TricepsExtensionExerciseName.SINGLE_DUMBBELL_SEATED_OVERHEAD_TRICEPS_EXTENSION = 25
TricepsExtensionExerciseName.SINGLE_LEG_BENCH_DIP_AND_KICK = 26
TricepsExtensionExerciseName.WEIGHTED_SINGLE_LEG_BENCH_DIP_AND_KICK = 27
TricepsExtensionExerciseName.SINGLE_LEG_DIP = 28
TricepsExtensionExerciseName.WEIGHTED_SINGLE_LEG_DIP = 29
TricepsExtensionExerciseName.STATIC_LYING_TRICEPS_EXTENSION = 30
TricepsExtensionExerciseName.SUSPENDED_DIP = 31
TricepsExtensionExerciseName.WEIGHTED_SUSPENDED_DIP = 32
TricepsExtensionExerciseName.SWISS_BALL_DUMBBELL_LYING_TRICEPS_EXTENSION = 33
TricepsExtensionExerciseName.SWISS_BALL_EZ_BAR_LYING_TRICEPS_EXTENSION = 34
TricepsExtensionExerciseName.SWISS_BALL_EZ_BAR_OVERHEAD_TRICEPS_EXTENSION = 35
TricepsExtensionExerciseName.TABLETOP_DIP = 36
TricepsExtensionExerciseName.WEIGHTED_TABLETOP_DIP = 37
TricepsExtensionExerciseName.TRICEPS_EXTENSION_ON_FLOOR = 38
TricepsExtensionExerciseName.TRICEPS_PRESSDOWN = 39
TricepsExtensionExerciseName.WEIGHTED_DIP = 40


@dataclass
class WarmUpExerciseName:
    value: int


WarmUpExerciseName.QUADRUPED_ROCKING = 0
WarmUpExerciseName.NECK_TILTS = 1
WarmUpExerciseName.ANKLE_CIRCLES = 2
WarmUpExerciseName.ANKLE_DORSIFLEXION_WITH_BAND = 3
WarmUpExerciseName.ANKLE_INTERNAL_ROTATION = 4
WarmUpExerciseName.ARM_CIRCLES = 5
WarmUpExerciseName.BENT_OVER_REACH_TO_SKY = 6
WarmUpExerciseName.CAT_CAMEL = 7
WarmUpExerciseName.ELBOW_TO_FOOT_LUNGE = 8
WarmUpExerciseName.FORWARD_AND_BACKWARD_LEG_SWINGS = 9
WarmUpExerciseName.GROINERS = 10
WarmUpExerciseName.INVERTED_HAMSTRING_STRETCH = 11
WarmUpExerciseName.LATERAL_DUCK_UNDER = 12
WarmUpExerciseName.NECK_ROTATIONS = 13
WarmUpExerciseName.OPPOSITE_ARM_AND_LEG_BALANCE = 14
WarmUpExerciseName.REACH_ROLL_AND_LIFT = 15
WarmUpExerciseName.SCORPION = 16  # Deprecated do not use
WarmUpExerciseName.SHOULDER_CIRCLES = 17
WarmUpExerciseName.SIDE_TO_SIDE_LEG_SWINGS = 18
WarmUpExerciseName.SLEEPER_STRETCH = 19
WarmUpExerciseName.SLIDE_OUT = 20
WarmUpExerciseName.SWISS_BALL_HIP_CROSSOVER = 21
WarmUpExerciseName.SWISS_BALL_REACH_ROLL_AND_LIFT = 22
WarmUpExerciseName.SWISS_BALL_WINDSHIELD_WIPERS = 23
WarmUpExerciseName.THORACIC_ROTATION = 24
WarmUpExerciseName.WALKING_HIGH_KICKS = 25
WarmUpExerciseName.WALKING_HIGH_KNEES = 26
WarmUpExerciseName.WALKING_KNEE_HUGS = 27
WarmUpExerciseName.WALKING_LEG_CRADLES = 28
WarmUpExerciseName.WALKOUT = 29
WarmUpExerciseName.WALKOUT_FROM_PUSH_UP_POSITION = 30


@dataclass
class RunExerciseName:
    value: int


RunExerciseName.RUN = 0
RunExerciseName.WALK = 1
RunExerciseName.JOG = 2
RunExerciseName.SPRINT = 3


class WaterType(Enum):
    FRESH = 0
    SALT = 1
    EN13319 = 2
    CUSTOM = 3


class TissueModelType(Enum):
    ZHL_16C = 0  # Buhlmann's decompression algorithm, version C


class DiveGasStatus(Enum):
    DISABLED = 0
    ENABLED = 1
    BACKUP_ONLY = 2


class DiveAlert(Enum):
    NDL_REACHED = 0
    GAS_SWITCH_PROMPTED = 1
    NEAR_SURFACE = 2
    APPROACHING_NDL = 3
    PO2_WARN = 4
    PO2_CRIT_HIGH = 5
    PO2_CRIT_LOW = 6
    TIME_ALERT = 7
    DEPTH_ALERT = 8
    DECO_CEILING_BROKEN = 9
    DECO_COMPLETE = 10
    SAFETY_STOP_BROKEN = 11
    SAFETY_STOP_COMPLETE = 12
    CNS_WARNING = 13
    CNS_CRITICAL = 14
    OTU_WARNING = 15
    OTU_CRITICAL = 16
    ASCENT_CRITICAL = 17
    ALERT_DISMISSED_BY_KEY = 18
    ALERT_DISMISSED_BY_TIMEOUT = 19
    BATTERY_LOW = 20
    BATTERY_CRITICAL = 21
    SAFETY_STOP_STARTED = 22
    APPROACHING_FIRST_DECO_STOP = 23
    SETPOINT_SWITCH_AUTO_LOW = 24
    SETPOINT_SWITCH_AUTO_HIGH = 25
    SETPOINT_SWITCH_MANUAL_LOW = 26
    SETPOINT_SWITCH_MANUAL_HIGH = 27
    AUTO_SETPOINT_SWITCH_IGNORED = 28
    SWITCHED_TO_OPEN_CIRCUIT = 29
    SWITCHED_TO_CLOSED_CIRCUIT = 30
    TANK_BATTERY_LOW = 32
    PO2_CCR_DIL_LOW = 33  # ccr diluent has low po2
    DECO_STOP_CLEARED = 34  # a deco stop has been cleared
    APNEA_NEUTRAL_BUOYANCY = 35  # Target Depth Apnea Alarm triggered
    APNEA_TARGET_DEPTH = 36  # Neutral Buoyance Apnea Alarm triggered
    APNEA_SURFACE = 37  # Surface Apnea Alarm triggered
    APNEA_HIGH_SPEED = 38  # High Speed Apnea Alarm triggered
    APNEA_LOW_SPEED = 39  # Low Speed Apnea Alarm triggered


class DiveAlarmType(Enum):
    DEPTH = 0  # Alarm when a certain depth is crossed
    TIME = 1  # Alarm when a certain time has transpired
    SPEED = 2  # Alarm when a certain ascent or descent rate is exceeded


class DiveBacklightMode(Enum):
    AT_DEPTH = 0
    ALWAYS_ON = 1


class SleepLevel(Enum):
    UNMEASURABLE = 0
    AWAKE = 1
    LIGHT = 2
    DEEP = 3
    REM = 4


class Spo2MeasurementType(Enum):
    OFF_WRIST = 0
    SPOT_CHECK = 1
    CONTINUOUS_CHECK = 2
    PERIODIC = 3


class CcrSetpointSwitchMode(Enum):
    MANUAL = 0  # User switches setpoints manually
    AUTOMATIC = 1  # Switch automatically based on depth


class DiveGasMode(Enum):
    OPEN_CIRCUIT = 0
    CLOSED_CIRCUIT_DILUENT = 1


class ProjectileType(Enum):
    ARROW = 0  # Arrow projectile type
    RIFLE_CARTRIDGE = 1  # Rifle cartridge projectile type
    PISTOL_CARTRIDGE = 2  # Pistol cartridge projectile type
    SHOTSHELL = 3  # Shotshell projectile type
    AIR_RIFLE_PELLET = 4  # Air rifle pellet projectile type
    OTHER = 5  # Other projectile type


@dataclass
class FaveroProduct:
    value: int


FaveroProduct.ASSIOMA_UNO = 10
FaveroProduct.ASSIOMA_DUO = 12


class SplitType(Enum):
    ASCENT_SPLIT = 1
    DESCENT_SPLIT = 2
    INTERVAL_ACTIVE = 3
    INTERVAL_REST = 4
    INTERVAL_WARMUP = 5
    INTERVAL_COOLDOWN = 6
    INTERVAL_RECOVERY = 7
    INTERVAL_OTHER = 8
    CLIMB_ACTIVE = 9
    CLIMB_REST = 10
    SURF_ACTIVE = 11
    RUN_ACTIVE = 12
    RUN_REST = 13
    WORKOUT_ROUND = 14
    RWD_RUN = 17  # run/walk detection running
    RWD_WALK = 18  # run/walk detection walking
    WINDSURF_ACTIVE = 21
    RWD_STAND = 22  # run/walk detection standing
    TRANSITION = 23  # Marks the time going from ascent_split to descent_split/used in backcountry ski
    SKI_LIFT_SPLIT = 28
    SKI_RUN_SPLIT = 29


class ClimbProEvent(Enum):
    APPROACH = 0
    START = 1
    COMPLETE = 2


class GasConsumptionRateType(Enum):
    PRESSURE_SAC = 0  # Pressure-based Surface Air Consumption
    VOLUME_SAC = 1  # Volumetric Surface Air Consumption
    RMV = 2  # Respiratory Minute Volume


class TapSensitivity(Enum):
    HIGH = 0
    MEDIUM = 1
    LOW = 2


class RadarThreatLevelType(Enum):
    THREAT_UNKNOWN = 0
    THREAT_NONE = 1
    THREAT_APPROACHING = 2
    THREAT_APPROACHING_FAST = 3


class MaxMetSpeedSource(Enum):
    ONBOARD_GPS = 0
    CONNECTED_GPS = 1
    CADENCE = 2


class MaxMetHeartRateSource(Enum):
    WHR = 0  # Wrist Heart Rate Monitor
    HRM = 1  # Chest Strap Heart Rate Monitor


class HrvStatus(Enum):
    NONE = 0
    POOR = 1
    LOW = 2
    UNBALANCED = 3
    BALANCED = 4


class NoFlyTimeMode(Enum):
    STANDARD = 0  # Standard Diver Alert Network no-fly guidance
    FLAT_24_HOURS = 1  # Flat 24 hour no-fly guidance
