# Streve
# Copyright (C) 2024 The Streve Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: Copyright (C) 2024 The Streve Authors

import logging
import re
import zipfile
from pathlib import Path
from typing import Optional

import openpyxl
from openpyxl.worksheet.worksheet import Worksheet

from streve.garmin_fit.base_type import BASE_TYPES_BY_NAME
from streve.garmin_fit.profile.schema import (
    FieldName,
    MessageComponentDefinition,
    MessageDefinition,
    MessageFieldDefinition,
    MessageName,
    MessageRefFieldDefinition,
    MessageSubfieldDefinition,
    ProfileDefinition,
    TypeDefinition,
    TypeName,
    TypeValueDefinition,
    ValueName,
)

__all__ = ["parse_profile"]


_SDK_FILENAME_PATTERN = re.compile(r"^FitSDKRelease_(\d+\.\d+\.\d+).zip$")


def parse_profile(sdk_path: Path) -> ProfileDefinition:
    m = _SDK_FILENAME_PATTERN.match(sdk_path.name)
    if m is None:
        raise Exception(
            "unable to determine version of Fit SDK: "
            f"expected filename matching '{_SDK_FILENAME_PATTERN.pattern}', found '{sdk_path.name}'"
        )
    version = m.group(1)
    logging.info("SDK filename(%s) version(%s)", sdk_path.name, version)
    with zipfile.ZipFile(sdk_path, "r").open("Profile.xlsx") as profile_xls:
        workbook = openpyxl.load_workbook(profile_xls)
    types = list(_parse_types(workbook["Types"]))
    types_by_name = {type.name: type for type in types}
    message_number_by_raw_name = {value.name.raw: value.value for value in types_by_name[TypeName("mesg_num")].values}
    messages = list(_parse_messages(workbook["Messages"], message_number_by_raw_name))
    return ProfileDefinition(types=types, messages=messages)


def _combine_rows(rows, is_key):
    """Group rows based on a condition.

    For example:
    [key1, value1, value2, value3, key2, value4, value5]
    -->
    [(key1, [value1, value2, value3]), (key2, [value4, value5])]
    """
    key_row = None
    value_rows = []
    for row in rows:
        if is_key(row):
            if key_row is not None or value_rows:
                yield (key_row, value_rows)
                value_rows = []
            key_row = row
        else:
            value_rows.append(row)
    if key_row is not None or value_rows:
        yield (key_row, value_rows)


def _parse_types(sheet: Worksheet):

    def extract_value(value_row):
        (_, _, value_name, value, comment) = value_row
        value_name = ValueName(value_name)
        if isinstance(value, str):
            value = int(value, 16)
        assert isinstance(value, int)
        return TypeValueDefinition(name=value_name, value=value, comment=comment)

    def extract_type(type_row, value_rows):
        (type_name, base_type, _, _, comment) = type_row
        type_name = TypeName(type_name)
        base_type = BASE_TYPES_BY_NAME[base_type]
        values = [extract_value(value_row) for value_row in value_rows]
        return TypeDefinition(name=type_name, base_type=base_type, values=values, comment=comment)

    rows = sheet.iter_rows(min_row=2, values_only=True)  # Skip the first row (header)
    for type_row, value_rows in _combine_rows(rows, lambda row: row[0] is not None):
        yield extract_type(type_row, value_rows)


def _split_cell(cell: None | int | str, n: int):
    """Split a comma-delimited cell (holding either 0, 1, or n values) into a list of n values."""
    if cell is None:
        return [None] * n
    elif isinstance(cell, int):
        return [cell] * n
    elif isinstance(cell, str):
        values = [int(value) if value.isdigit() else value for value in cell.split(",")]
        if len(values) == 1:
            return values * n
        else:
            assert len(values) == n
            return values
    else:
        assert False


def _parse_messages(sheet: Worksheet, message_number_by_raw_name: dict[str, int]):

    def is_group_row(row):
        # Only column 3 is populated
        return all((cell is not None) == (i == 3) for i, cell in enumerate(row))

    def extract_components(row) -> list[MessageComponentDefinition]:
        if row[5] is None:
            return []
        names = [FieldName(name) for name in row[5].split(",")]
        n = len(names)
        return [
            MessageComponentDefinition(
                name=name, scale=scale, offset=offset, units=units, bits=bits, accumulate=accumulate
            )
            for name, scale, offset, units, bits, accumulate in zip(
                names,
                _split_cell(row[6], n),
                _split_cell(row[7], n),
                _split_cell(row[8], n),
                _split_cell(row[9], n),
                _split_cell(row[10], n),
            )
        ]

    def extract_subfield(subfield_row):
        components = extract_components(subfield_row)
        ref_field_names = [FieldName(name) for name in subfield_row[11].split(",")]
        ref_field_values = [ValueName(name) for name in subfield_row[12].split(",")]
        assert len(ref_field_names) == len(ref_field_values)
        ref_fields = [
            MessageRefFieldDefinition(name, value) for (name, value) in zip(ref_field_names, ref_field_values)
        ]
        return MessageSubfieldDefinition(
            name=FieldName(subfield_row[2]),
            type=TypeName(subfield_row[3]),
            scale=None if components else subfield_row[6],
            offset=None if components else subfield_row[7],
            units=None if components else subfield_row[8],
            ref_fields=ref_fields,
            components=components,
            comment=subfield_row[13],
        )

    def extract_field(field_row, subfield_rows):
        subfields = [extract_subfield(subfield_row) for subfield_row in subfield_rows]
        components = extract_components(field_row)
        return MessageFieldDefinition(
            name=FieldName(field_row[2]),
            number=field_row[1],
            type=TypeName(field_row[3]),
            array=(field_row[4] is not None) and type != "string",
            scale=None if components else field_row[6],
            offset=None if components else field_row[7],
            units=None if components else field_row[8],
            components=components,
            subfields=subfields,
            comment=field_row[13],
        )

    def extract_message(group_name: Optional[str], message_row, field_rows):
        name, comment = message_row[0], message_row[13]
        name = MessageName(name)
        number = message_number_by_raw_name[name.raw]
        fields = [
            extract_field(field_row, subfield_rows)
            for field_row, subfield_rows in _combine_rows(field_rows, lambda row: row[1] is not None)
        ]
        return MessageDefinition(name=name, number=number, group_name=group_name, fields=fields, comment=comment)

    rows = sheet.iter_rows(min_row=2, values_only=True)  # Skip the first row (header)
    for group_name_row, group_rows in _combine_rows(rows, is_group_row):
        group_name = group_name_row[3] if group_name_row else None
        for message_row, field_rows in _combine_rows(group_rows, lambda row: row[0] is not None):
            yield extract_message(group_name, message_row, field_rows)
