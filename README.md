# Streve

## Development

### Requirements

* Python 3.12
* Poetry

### Set-up

Run `poetry install` to set up a virtual environment and install the required dependencies.

### Running

The web server can be run using `poetry run streve/app.py`. During development running `./bin/watch-tailwind` will keep
the generated CSS up to date.

### Hooks

Set up git hooks by running `poetry run pre-commit install`.