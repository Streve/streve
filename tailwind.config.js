/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./streve/templates/**/*.html",
        "./streve/static/src/**/*.js",
    ],
    theme: {
        extend: {},
    },
    plugins: [],
}

